package com.yss.main.generic.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.yss.main.entity.UrlEntity;
import com.yss.main.util.Constants;

@Service("ISmsSender")
@PropertySource("classpath:application.properties")
public class SmsSenderImpl implements ISmsSender {

	@Autowired
	private IGenericService<UrlEntity> urlEntityService;

	private static final Logger log = LoggerFactory.getLogger(SmsSenderImpl.class);

	// @Value("${sms-url}")
	// private String smsUrl;

	String MOBILE = "";
	String SMS_TEXT = "";
	Long SMS_ID = 0L;
	String SENT_DATE1 = "";
	String SENT_DATE2 = "";
	String URL_NAME = "";

	@Override
	public int send(String mobile, String password) {

		List<UrlEntity> objurlEntity = urlEntityService.fetch(new UrlEntity());
		for (UrlEntity urlEntity : objurlEntity) {
			URL_NAME = urlEntity.getUrlName();
			System.out.println("URL NAME is" + URL_NAME);
		}

		if (URL_NAME != null && !URL_NAME.equals("")) {
			SMS_TEXT = "Your Passoword is " + password;
			MOBILE = mobile;
			try {

				log.info("***************SMS URL Ready For Send SMS*****************" + URL_NAME);
				URL_NAME = URL_NAME.replace("XXXX", MOBILE).replace("YYYY", SMS_TEXT);
				log.info("#####URL After Replace By Mobile And Message#######" + URL_NAME);
				String response= new RestTemplate().getForObject(URL_NAME, String.class);

				if (response.isEmpty() || response == null) {
					log.info("Response : " + Constants.FAIL);
					URL_NAME = URL_NAME.replace(MOBILE, "XXXX").replace(SMS_TEXT, "YYYY");
					log.info("********Refresh URL*********" + URL_NAME);
					return 0;
				} else {

					log.info("Response : " + Constants.SUCCESS);

					URL_NAME = URL_NAME.replace(MOBILE, "XXXX").replace(SMS_TEXT, "YYYY");
					log.info("********Refresh URL*********" + URL_NAME);
					return 1;
				}

			} catch (Exception e) {
				System.out.println(" #### Oops! Error in Connection #### " + e.getMessage());
			}
		} else {
			log.info(" #### Url not found #### ");
		}

		return 1;
	}

}