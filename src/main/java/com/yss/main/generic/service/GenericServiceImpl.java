package com.yss.main.generic.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yss.main.generic.dao.IGenericDao;

@Service
public class GenericServiceImpl<T> implements IGenericService<T> {
	@Autowired
	private IGenericDao<T> iGenericDao;

	@Override
	public void save(T entity) {
		iGenericDao.save(entity);
	}

	@Override
	public void update(T entity) {
		iGenericDao.update(entity);
	}

	@Override
	public void delete(T entity) {
		iGenericDao.delete(entity);
	}

	@Override
	public List<T> fetch(T entity) {
		return iGenericDao.fetch(entity);
	}

	@Override
	public List<T> fetch(T entity, String condition) {
		return iGenericDao.fetch(entity, condition);
	}

	@Override
	public T find(T entity, Serializable id) {
		return iGenericDao.find(entity, id);
	}

	@Override
	public T find(T entity, String condition) {
		return iGenericDao.find(entity, condition);
	}

	@Override
	public boolean exists(T entity, Serializable id) {
		return iGenericDao.exists(entity, id);
	}

	@Override
	public boolean exists(T entity, String condition) {
		return iGenericDao.exists(entity, condition);
	}
 
	@Override
	public List<T> nativeQuery(T entity,String query) { 
		return iGenericDao.nativeQuery(entity,query);
	}

	@Override
	public List<T> findAllByCondition(T entity, String condition) {
		
		return iGenericDao.findAllByCondition(entity, condition);

		 
	}

	@Override
	public List<T> fetchFields(String fields, T entity) {
		// TODO Auto-generated method stub
		return iGenericDao.fetchFields(fields, entity);
	}

	@Override
	public List<T> fetchFields(String fields, T entity, String condition) {
		// TODO Auto-generated method stub
		return iGenericDao.fetchFields(fields, entity, condition);
	}

	@Override
	public T findMaxId(Long fields,T entity, String condition) {
		return iGenericDao.findMaxId(fields,entity, condition);
	}

	
	@Override
	public List<T> fetch(T entity, int pageNumber, int size) {
		// TODO Auto-generated method stub
		return iGenericDao.fetch(entity, pageNumber, size);
	}

	@Override
	public List<T> fetch(T entity, int pageNumber, int size, String condition) {
		// TODO Auto-generated method stub
		return iGenericDao.fetch(entity, pageNumber, size, condition);
	}
}
