package com.yss.main.generic.service;

import com.yss.main.util.IOperations;

public interface IGenericService<T> extends IOperations<T> {

}
