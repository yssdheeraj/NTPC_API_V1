package com.yss.main.generic.dao;

import com.yss.main.util.IOperations;

public interface IGenericDao<T> extends IOperations<T> {

}
