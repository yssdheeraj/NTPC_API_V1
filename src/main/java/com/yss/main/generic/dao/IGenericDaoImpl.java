package com.yss.main.generic.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.yss.main.util.Constants;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class IGenericDaoImpl<T> implements IGenericDao<T> {

	@PersistenceContext
	private EntityManager entityManager; 
	
	
	
	/*@Autowired
	   SessionFactory sessionFactory;; 
*/
	

		@Override
	public void save(T entity) {
		// TODO Auto-generated method stub
			entityManager.persist(entity);
	}

	@Override
	public void update(T entity) {
		// TODO Auto-generated method stub
		
		entityManager.merge(entity);
		
	}

	@Override
	public void delete(T entity) {
		// TODO Auto-generated method stub
		entityManager.remove(entity);
	}

	@Override
	public List<T> fetch(T entity) {
		// TODO Auto-generated method stub
		return entityManager.createQuery("from " + entity.getClass().getName()).getResultList();
		
	}

	@Override
	public List<T> fetch(T entity, String condition) {
		return entityManager.createQuery("from " + entity.getClass().getName() + " " + condition).getResultList();
		
	}

	@Override
	public T find(T entity, Serializable id) {
		// TODO Auto-generated method stub
		return (T) entityManager.find(entity.getClass(), id);
		
	}

	@Override
	public T find(T entity, String condition) {
		try {
			System.out.println("-------------Find By Condition-------------");
			return (T) entityManager.createQuery("from " + entity.getClass().getName() + " " + condition)
					.getResultList().get(0);
		} catch (IndexOutOfBoundsException e) {
			System.out.println("-------------ERROR-------------" + e.getMessage());
			return null;
		}
	}
	
	@Override
	public List<T> nativeQuery(T entity, String query) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean exists(T entity, Serializable id) {		
		return find(entity, id) != null ? true : false;
	}

	@Override
	public boolean exists(T entity, String condition) {		
		return find(entity, condition) != null ? true : false;
	}

	@Override
	public List<T> findAllByCondition(T entity, String condition) {
		
		if (condition != null && condition != "") {
			try {
				System.out.println("----Fetch By All in If---- ");
				return entityManager.createQuery(
						"from " + entity.getClass().getName()+" obj "+ condition).getResultList();
			} catch (Exception e) {
				System.out.println("----Error In Fetch By All ---- " +e.getMessage());
				return null;
			}
		} else {
			System.out.println("----Fetch By All in else ---- ");
			return entityManager.createQuery(
					"from " + entity.getClass().getName()).getResultList();
		}
	}

	@Override
	public List<T> fetchFields(String fields, T entity) {
		// TODO Auto-generated method stub
		 return entityManager.createQuery("select "+fields+" from " + entity.getClass().getName()).getResultList();
	}

	@Override
	public List<T> fetchFields(String fields, T entity, String condition) {
		// TODO Auto-generated method stub
		System.out.println("fields "+fields);
		System.out.println("condition "+condition);		
		return entityManager.createQuery("select "+fields+" from " + entity.getClass().getName()+ " " + condition).getResultList();
	}

	@Override
	public T findMaxId(Long fields,T entity, String condition) {
		
		return (T) entityManager.createQuery("select Max(" +fields+ ") from " + entity.getClass().getName()+ " " + condition).getResultList().get(0);

	}

//pagination without condition	
	@Override
	public List<T> fetch(T entity,int pageNumber, int size) {
		// TODO Auto-generated method stub
		int maxValue=pageNumber*size;
		int minValue=(maxValue-size);
		System.out.println(pageNumber+"dao "+size);
		System.out.println(minValue+"dao "+maxValue);
		List maxData =entityManager.createQuery("select count (*) from " + entity.getClass().getName()).getResultList();
		System.out.println(minValue+"   "+maxValue+"  "+maxData);
		long data=(long) maxData.get(0);
		Constants.TOTAL_NUMBER_OF_DATA=(int)data;
		Constants.TOTAL_NUMBER_OF_PAGE= getTotalPage((int)data,size);
		
		if(maxValue>data){
			long j=maxValue-data;
			maxValue=(int) data;
					
			System.out.println(minValue+"   "+maxValue+"  "+maxData);
		}else {
			
		}
		return entityManager.createQuery("from " + entity.getClass().getName()).getResultList().subList(minValue, maxValue);
	}

	//pagination with condition	
	
	@Override
	public List<T> fetch(T entity,int pageNumber, int size,String condition) {
		// TODO Auto-generated method stub
		int maxValue=pageNumber*size;
		int minValue=maxValue-size;
		System.out.println(pageNumber+"dao "+size);
		System.out.println(minValue+"dao "+maxValue+"    "+condition);
		
		List maxData =entityManager.createQuery("select count (*) from " + entity.getClass().getName()+" " + condition).getResultList();
		
		System.out.println(minValue+"   "+maxValue+"  "+maxData);
		long data=(long) maxData.get(0);
		Constants.TOTAL_NUMBER_OF_DATA=(int)data;
		if(maxValue>data){
			long j=maxValue-data;
			maxValue=(int) data;
			System.out.println(minValue+"   "+maxValue+"  "+maxData);
		}
		
		Constants.TOTAL_NUMBER_OF_PAGE= getTotalPage((int)data,size);
		return entityManager.createQuery("from " + entity.getClass().getName() + " " + condition).getResultList().subList(minValue, maxValue);
	}
	
	public int getTotalPage(int totaldata,int pageSize) {
		int i=totaldata/pageSize;
		if(totaldata%pageSize !=0) {
			i++;
		}
		return i;
	}
	
}
