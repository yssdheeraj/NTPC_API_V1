package com.yss.main.util;

import java.io.Serializable;
import java.util.List;

public interface IOperations<T> {
	
	void save(T entity);

	void update(T entity);

	void delete(T entity);

	List<T> fetch(T entity);

	List<T> fetch(T entity, String condition);

	T find(T entity, Serializable id);

	T find(T entity, String condition);

	boolean exists(T entity, Serializable id);

	boolean exists(T entity, String condition); 

	List<T> nativeQuery(T entity, String query);
	 
	List<T> findAllByCondition(T entity , String condition);
	
	List<T> fetchFields(String fields,T entity);
	
	List<T> fetchFields(String fields,T entity,String condition);
	
	public T findMaxId(Long fields,T entity, String condition);
	
	List<T> fetch(T entity,int pageNumber,int size);
	List<T> fetch(T entity,int pageNumber,int size,String condition);

}
