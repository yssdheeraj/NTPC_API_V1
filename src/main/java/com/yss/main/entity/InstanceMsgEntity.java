package com.yss.main.entity;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class InstanceMsgEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
//	@Column(name="altid")
//	private long altid;
//	
//	@Column(name="armyid")
//	private String armyid;
//	
//	@Column(name = "name" )
//	private String name;
//
//	@Column(name = "rank" )
//	private String rank;
//
//	@Column(name = "group_name" )
//	private String group_name;
//
//	@Column(name = "status")
//	private String status;

	@Column(name="phoneDirectoryName")
	private ArrayList<String> phoneDirectoryName;
	
	
	private ArrayList<String> uploadContactList;
	
	@Column(name="contactGroupName")
	private ArrayList<String> contactGroupName;
	
	@Column(name="departmentName")
	private ArrayList<String> departmentName;
	
	@Column(name="project")
	private ArrayList<String> projectName;
	
	@Column(name="location")
	private ArrayList<String> locationName;	
	
	@Column(name="grade")
	private ArrayList<String> grade;	
	
	@Column(name="substgrade")
	private ArrayList<String> substGradeName;	
	
	@Column(name="smsName")
	private String smsName;
	
	
	@Column(name="smsText")
	private String smsText;
	
//	private String smsVoice;
	
	@Column(name="mobile")
	private String mobile;
	
	@Column(name="scheduleType")
	private String scheduleType;
	
	@Column(name="scheduleDate")
	private String scheduleDate;
		
	@Column(name="dateAndTime")
	private String dateAndTime;
	
	@Column(name="flag")
	private boolean flag;
	
	
	@Column(name="priority")
	private String priority;

	@OneToOne
	@JoinColumn(name="sender_by")
	private UserEntity loginId;

	
	
	public InstanceMsgEntity() {
		super();
		// TODO Auto-generated constructor stub
	}



	public InstanceMsgEntity(long id, ArrayList<String> phoneDirectoryName, ArrayList<String> uploadContactList,
			ArrayList<String> contactGroupName, ArrayList<String> departmentName, ArrayList<String> projectName,
			ArrayList<String> locationName, ArrayList<String> grade, ArrayList<String> substGradeName, String smsName,
			String smsText, String mobile, String scheduleType, String scheduleDate, String dateAndTime, boolean flag,
			String priority, UserEntity loginId) {
		super();
		this.id = id;
		this.phoneDirectoryName = phoneDirectoryName;
		this.uploadContactList = uploadContactList;
		this.contactGroupName = contactGroupName;
		this.departmentName = departmentName;
		this.projectName = projectName;
		this.locationName = locationName;
		this.grade = grade;
		this.substGradeName = substGradeName;
		this.smsName = smsName;
		this.smsText = smsText;
		this.mobile = mobile;
		this.scheduleType = scheduleType;
		this.scheduleDate = scheduleDate;
		this.dateAndTime = dateAndTime;
		this.flag = flag;
		this.priority = priority;
		this.loginId = loginId;
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public ArrayList<String> getPhoneDirectoryName() {
		return phoneDirectoryName;
	}



	public void setPhoneDirectoryName(ArrayList<String> phoneDirectoryName) {
		this.phoneDirectoryName = phoneDirectoryName;
	}



	public ArrayList<String> getUploadContactList() {
		return uploadContactList;
	}



	public void setUploadContactList(ArrayList<String> uploadContactList) {
		this.uploadContactList = uploadContactList;
	}



	public ArrayList<String> getContactGroupName() {
		return contactGroupName;
	}



	public void setContactGroupName(ArrayList<String> contactGroupName) {
		this.contactGroupName = contactGroupName;
	}



	public ArrayList<String> getDepartmentName() {
		return departmentName;
	}



	public void setDepartmentName(ArrayList<String> departmentName) {
		this.departmentName = departmentName;
	}



	public ArrayList<String> getProjectName() {
		return projectName;
	}



	public void setProjectName(ArrayList<String> projectName) {
		this.projectName = projectName;
	}



	public ArrayList<String> getLocationName() {
		return locationName;
	}



	public void setLocationName(ArrayList<String> locationName) {
		this.locationName = locationName;
	}



	public ArrayList<String> getGrade() {
		return grade;
	}



	public void setGrade(ArrayList<String> grade) {
		this.grade = grade;
	}



	public ArrayList<String> getSubstGradeName() {
		return substGradeName;
	}



	public void setSubstGradeName(ArrayList<String> substGradeName) {
		this.substGradeName = substGradeName;
	}



	public String getSmsName() {
		return smsName;
	}



	public void setSmsName(String smsName) {
		this.smsName = smsName;
	}



	public String getSmsText() {
		return smsText;
	}



	public void setSmsText(String smsText) {
		this.smsText = smsText;
	}



	public String getMobile() {
		return mobile;
	}



	public void setMobile(String mobile) {
		this.mobile = mobile;
	}



	public String getScheduleType() {
		return scheduleType;
	}



	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}



	public String getScheduleDate() {
		return scheduleDate;
	}



	public void setScheduleDate(String scheduleDate) {
		this.scheduleDate = scheduleDate;
	}



	public String getDateAndTime() {
		return dateAndTime;
	}



	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}



	public boolean isFlag() {
		return flag;
	}



	public void setFlag(boolean flag) {
		this.flag = flag;
	}



	public String getPriority() {
		return priority;
	}



	public void setPriority(String priority) {
		this.priority = priority;
	}



	public UserEntity getLoginId() {
		return loginId;
	}



	public void setLoginId(UserEntity loginId) {
		this.loginId = loginId;
	}



	@Override
	public String toString() {
		return "InstanceMsgEntity [id=" + id + ", phoneDirectoryName=" + phoneDirectoryName + ", uploadContactList="
				+ uploadContactList + ", contactGroupName=" + contactGroupName + ", departmentName=" + departmentName
				+ ", projectName=" + projectName + ", locationName=" + locationName + ", grade=" + grade
				+ ", substGradeName=" + substGradeName + ", smsName=" + smsName + ", smsText=" + smsText + ", mobile="
				+ mobile + ", scheduleType=" + scheduleType + ", scheduleDate=" + scheduleDate + ", dateAndTime="
				+ dateAndTime + ", flag=" + flag + ", priority=" + priority + ", loginId=" + loginId + "]";
	}



	


	
}

	