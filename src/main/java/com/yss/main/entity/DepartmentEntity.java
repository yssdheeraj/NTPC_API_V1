package com.yss.main.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
//@Table(name="tbl_department")
@Table
public class DepartmentEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7011210471592431368L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="departmentId")
	private long departmentId;
	
	@Column(name="departmentName")
	private String departmentName;
	
	@Column(name="departmentDescription")
	private String departmentDesc;
	
	@OneToOne
	@JoinColumn(name = "loginId")
	private UserEntity loginId;

//	@Column(name="loginId")
//	private String loginId;
	
	
	@Column(name="createdDate")
	private String createdDate;
	
	 @Column(name="flag", columnDefinition = "boolean default false")
	 private boolean flag;
	
	public DepartmentEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getDepartmentDesc() {
		return departmentDesc;
	}

	public void setDepartmentDesc(String departmentDesc) {
		this.departmentDesc = departmentDesc;
	}

	public UserEntity getLoginId() {
		return loginId;
	}

	public void setLoginId(UserEntity loginId) {
		this.loginId = loginId;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "DepartmentEntity [departmentId=" + departmentId + ", departmentName=" + departmentName
				+ ", departmentDesc=" + departmentDesc + ", loginId=" + loginId + ", createdDate=" + createdDate
				+ ", flag=" + flag + "]";
	}

	public DepartmentEntity(long departmentId, String departmentName, String departmentDesc, UserEntity loginId,
			String createdDate, boolean flag) {
		super();
		this.departmentId = departmentId;
		this.departmentName = departmentName;
		this.departmentDesc = departmentDesc;
		this.loginId = loginId;
		this.createdDate = createdDate;
		this.flag = flag;
	}


}
