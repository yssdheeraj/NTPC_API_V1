package com.yss.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="group_name")
public class ContactGroup {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="group_id")
	private long contact_groupId;
	
	@Column(name="group_Name")
	private String contactGroupName;
	
	@Column(name="abbreviation")
	private String abbreviation;

	@OneToOne
	@JoinColumn(name = "loginId")
	private UserEntity loginId;
	
	
	@Column(name="create_Date")
	private String createDate;
	
	 @Column(name="flag", columnDefinition = "boolean default false")
	 private boolean flag;
	
	public ContactGroup() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ContactGroup(long contact_groupId, String contactGroupName, String abbreviation, UserEntity loginId,
			String createDate, boolean flag) {
		super();
		this.contact_groupId = contact_groupId;
		this.contactGroupName = contactGroupName;
		this.abbreviation = abbreviation;
		this.loginId = loginId;
		this.createDate = createDate;
		this.flag = flag;
	}

	public long getContact_groupId() {
		return contact_groupId;
	}

	public void setContact_groupId(long contact_groupId) {
		this.contact_groupId = contact_groupId;
	}

	public String getContactGroupName() {
		return contactGroupName;
	}

	public void setContactGroupName(String contactGroupName) {
		this.contactGroupName = contactGroupName;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public UserEntity getLoginId() {
		return loginId;
	}

	public void setLoginId(UserEntity loginId) {
		this.loginId = loginId;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "ContactGroup [contact_groupId=" + contact_groupId + ", contactGroupName=" + contactGroupName
				+ ", abbreviation=" + abbreviation + ", loginId=" + loginId + ", createDate=" + createDate + ", flag="
				+ flag + "]";
	}


}
