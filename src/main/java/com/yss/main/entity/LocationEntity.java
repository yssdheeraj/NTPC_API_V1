package com.yss.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="location")
public class LocationEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long locationId;
	
	@Column(name="locationname")
	private String locationName;
	
	@Column(name="create_Date")
	private String createDate;
	
	 @Column(name="flag", columnDefinition = "boolean default false")
	private boolean flag;
		

	public LocationEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	
	public LocationEntity(long locationId, String locationName, String createDate, boolean flag) {
		super();
		this.locationId = locationId;
		this.locationName = locationName;
		this.createDate = createDate;
		this.flag = flag;
	}


	public long getLocationId() {
		return locationId;
	}


	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}


	public String getLocationName() {
		return locationName;
	}


	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}


	public String getCreateDate() {
		return createDate;
	}


	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}


	public boolean isFlag() {
		return flag;
	}


	public void setFlag(boolean flag) {
		this.flag = flag;
	}


	@Override
	public String toString() {
		return "LocationEntity [locationId=" + locationId + ", locationName=" + locationName + ", createDate="
				+ createDate + ", flag=" + flag + "]";
	}


	
	
}
