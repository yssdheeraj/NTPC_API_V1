package com.yss.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_news")
public class AddNewsEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="newsId")
	private Long newsId;
	
	@Column(name="newsTitle")
	private String newsTitle;
	
	@Column(name="date_time")
	private String date_time;
	
	public AddNewsEntity() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Long getNewsId() {
		return newsId;
	}



	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}



	public String getNewsTitle() {
		return newsTitle;
	}



	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}



	public String getDate_time() {
		return date_time;
	}



	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}



	@Override
	public String toString() {
		return "AddNewsEntity [newsId=" + newsId + ", newsTitle=" + newsTitle + ", date_time=" + date_time + "]";
	}



	public AddNewsEntity(Long newsId, String newsTitle, String date_time) {
		super();
		this.newsId = newsId;
		this.newsTitle = newsTitle;
		this.date_time = date_time;
	}



		
}
