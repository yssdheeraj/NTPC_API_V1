package com.yss.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_master_trunk")
public class TrunkEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="Fld_RowID")
	private long Fld_RowID;
	
	@Column(name="Fld_TrunkNo")
	private long Fld_TrunkNo;
	
	@Column(name="mobile_no")
	private String mobile_no;
	
	public TrunkEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public TrunkEntity(long fld_RowID, long fld_TrunkNo, String mobile_no) {
		super();
		Fld_RowID = fld_RowID;
		Fld_TrunkNo = fld_TrunkNo;
		this.mobile_no = mobile_no;
	}

	public long getFld_RowID() {
		return Fld_RowID;
	}

	public void setFld_RowID(long fld_RowID) {
		Fld_RowID = fld_RowID;
	}

	public long getFld_TrunkNo() {
		return Fld_TrunkNo;
	}

	public void setFld_TrunkNo(long fld_TrunkNo) {
		Fld_TrunkNo = fld_TrunkNo;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	
	@Override
	public String toString() {
		return "TrunkEntity [Fld_RowID=" + Fld_RowID + ", Fld_TrunkNo=" + Fld_TrunkNo + ", mobile_no=" + mobile_no
				+ "]";
	}
	
}
