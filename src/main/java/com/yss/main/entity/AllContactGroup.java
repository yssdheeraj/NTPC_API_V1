package com.yss.main.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
public class AllContactGroup {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long allContactGroup_Id;
	
	@OneToOne
	@JoinColumn(name = "contact_groupId")
	private ContactGroup contactGroupName;
	
	@OneToMany(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
	private List<PhoneDirectory> phoneDirectoryId;
	
	
	@OneToOne
	@JoinColumn(name = "loginId")
	private UserEntity loginId;
	
	@Column(name="create_Date")
	private String createDate;
	
	
	public AllContactGroup() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	public AllContactGroup(long allContactGroup_Id, ContactGroup contactGroupName,
			List<PhoneDirectory> phoneDirectoryId, UserEntity loginId, String createDate) {
		super();
		this.allContactGroup_Id = allContactGroup_Id;
		this.contactGroupName = contactGroupName;
		this.phoneDirectoryId = phoneDirectoryId;
		this.loginId = loginId;
		this.createDate = createDate;
	}




	public long getAllContactGroup_Id() {
		return allContactGroup_Id;
	}
	
	public void setAllContactGroup_Id(long allContactGroup_Id) {
		this.allContactGroup_Id = allContactGroup_Id;
	}
	public ContactGroup getContactGroupName() {
		return contactGroupName;
	}
	public void setContactGroupName(ContactGroup contactGroupName) {
		this.contactGroupName = contactGroupName;
	}
	public List<PhoneDirectory> getPhoneDirectoryId() {
		return phoneDirectoryId;
	}
	public void setPhoneDirectoryId(List<PhoneDirectory> phoneDirectoryId) {
		this.phoneDirectoryId = phoneDirectoryId;
	}
	public UserEntity getLoginId() {
		return loginId;
	}
	public void setLoginId(UserEntity loginId) {
		this.loginId = loginId;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}




	@Override
	public String toString() {
		return "AllContactGroup [allContactGroup_Id=" + allContactGroup_Id + ", contactGroupName=" + contactGroupName
				+ ", phoneDirectoryId=" + phoneDirectoryId + ", loginId=" + loginId + ", createDate=" + createDate
				+ "]";
	}

	
	
}
