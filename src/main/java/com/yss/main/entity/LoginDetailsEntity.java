package com.yss.main.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_loginDetails")

public class LoginDetailsEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4458293065006623040L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "loginId")
	private String loginId;
	
	@Column(name = "passsword")
	private String password;
	
	@Column(name = "login_datetime") 
	private String login_datetime;
	
	@Column(name = "logout_datetime")  
	private String logout_datetime;

	@Column(name="location_name")
	private String locationname;
	

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLogin_datetime() {
		return login_datetime;
	}

	public void setLogin_datetime(String login_datetime) {
		this.login_datetime = login_datetime;
	}

	public String getLogout_datetime() {
		return logout_datetime;
	}

	public void setLogout_datetime(String logout_datetime) {
		this.logout_datetime = logout_datetime;
	}

	public String getLocationname() {
		return locationname;
	}

	public void setLocationname(String locationname) {
		this.locationname = locationname;
	}

	
}
