package com.yss.main.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class PaginationDataProvider {

	@Id
	int id;
	int pageNumber;
	int pageSize;
	int totalNumberOfPage;
	int totalNumberOfData;
	
	@OneToMany
	List<PhoneDirectory> numberOfData;
	
	@OneToMany
	List<ScheduleMessageEntity> schdeulDataList;
	
	@OneToMany
	List<ReportEntity> ReportDataList;
	
	@OneToMany
	List<ContactGroup> groupsList;
	
	
	
	
		
	public PaginationDataProvider() {
		super();
		// TODO Auto-generated constructor stub@OneToMany
	}





	public PaginationDataProvider(int id, int pageNumber, int pageSize, int totalNumberOfPage, int totalNumberOfData,
			List<PhoneDirectory> numberOfData, List<ScheduleMessageEntity> schdeulDataList,
			List<ReportEntity> reportDataList, List<ContactGroup> groupsList) {
		super();
		this.id = id;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.totalNumberOfPage = totalNumberOfPage;
		this.totalNumberOfData = totalNumberOfData;
		this.numberOfData = numberOfData;
		this.schdeulDataList = schdeulDataList;
		ReportDataList = reportDataList;
		this.groupsList = groupsList;
	}





	public int getId() {
		return id;
	}





	public void setId(int id) {
		this.id = id;
	}





	public int getPageNumber() {
		return pageNumber;
	}





	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}





	public int getPageSize() {
		return pageSize;
	}





	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}





	public int getTotalNumberOfPage() {
		return totalNumberOfPage;
	}





	public void setTotalNumberOfPage(int totalNumberOfPage) {
		this.totalNumberOfPage = totalNumberOfPage;
	}





	public int getTotalNumberOfData() {
		return totalNumberOfData;
	}





	public void setTotalNumberOfData(int totalNumberOfData) {
		this.totalNumberOfData = totalNumberOfData;
	}





	public List<PhoneDirectory> getNumberOfData() {
		return numberOfData;
	}





	public void setNumberOfData(List<PhoneDirectory> numberOfData) {
		this.numberOfData = numberOfData;
	}





	public List<ScheduleMessageEntity> getSchdeulDataList() {
		return schdeulDataList;
	}





	public void setSchdeulDataList(List<ScheduleMessageEntity> schdeulDataList) {
		this.schdeulDataList = schdeulDataList;
	}





	public List<ReportEntity> getReportDataList() {
		return ReportDataList;
	}





	public void setReportDataList(List<ReportEntity> reportDataList) {
		ReportDataList = reportDataList;
	}





	public List<ContactGroup> getGroupsList() {
		return groupsList;
	}





	public void setGroupsList(List<ContactGroup> groupsList) {
		this.groupsList = groupsList;
	}





	@Override
	public String toString() {
		return "PaginationDataProvider [id=" + id + ", pageNumber=" + pageNumber + ", pageSize=" + pageSize
				+ ", totalNumberOfPage=" + totalNumberOfPage + ", totalNumberOfData=" + totalNumberOfData
				+ ", numberOfData=" + numberOfData + ", schdeulDataList=" + schdeulDataList + ", ReportDataList="
				+ ReportDataList + ", groupsList=" + groupsList + ", getId()=" + getId() + ", getPageNumber()="
				+ getPageNumber() + ", getPageSize()=" + getPageSize() + ", getTotalNumberOfPage()="
				+ getTotalNumberOfPage() + ", getTotalNumberOfData()=" + getTotalNumberOfData() + ", getNumberOfData()="
				+ getNumberOfData() + ", getSchdeulDataList()=" + getSchdeulDataList() + ", getReportDataList()="
				+ getReportDataList() + ", getGroupsList()=" + getGroupsList() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}





	

		
}
