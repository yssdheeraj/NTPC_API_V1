package com.yss.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="temp_msg")
public class TempEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="slno")
	private long id;
	
	@Column(name="phoneDirectoryName")
	private String phoneDirectoryName;
	
	
	@Column(name="grp_name")
	private String contactGroupName;
	
	@Column(name="departmentName")
	private String departmentName;
	
	@Column(name="grade")
	private String grade;
	
	@Column(name="sms_template")
	private String smsName;
	
	@Column(name="locationName")
	private String locationName;
	
	@Column(name="project")
	private String project;
	
	//private String smsVoice;
	
	@Column(name="msg", nullable = true, columnDefinition = "varchar(255) default ''")
	private String smsText;
	

	
	@Column(name="mobile_no" , nullable = true, columnDefinition = "varchar(255) default 0")
	private String mobile;
	
	@Column(name="schd")
	private String scheduleType;
	
	@Column(name="msg_date" , nullable = true, columnDefinition = "varchar(255) default ''")
	private String scheduleDate;
	
	@Column(name="msg_time" , nullable = true, columnDefinition = "varchar(255) default ''")
	private String scheduleTime;
		
	@Column(name="DateTime")
	private String DateTime;
	
	@Column(name="user")
	private String loginId;
	
	@Column(name="flag"  , columnDefinition = "boolean default false", nullable = true)
	private boolean flag;
	
	@Column(name="priorty" , nullable = true, columnDefinition = "varchar(255) default 0")
	private String priority;

	
	
	
	public TempEntity() {
		super();
		// TODO Auto-generated constructor stub
	}




	public TempEntity(long id, String phoneDirectoryName, String contactGroupName, String departmentName, String grade,
			String smsName, String locationName, String project, String smsText, String mobile, String scheduleType,
			String scheduleDate, String scheduleTime, String dateTime, String loginId, boolean flag, String priority) {
		super();
		this.id = id;
		this.phoneDirectoryName = phoneDirectoryName;
		this.contactGroupName = contactGroupName;
		this.departmentName = departmentName;
		this.grade = grade;
		this.smsName = smsName;
		this.locationName = locationName;
		this.project = project;
		this.smsText = smsText;
		this.mobile = mobile;
		this.scheduleType = scheduleType;
		this.scheduleDate = scheduleDate;
		this.scheduleTime = scheduleTime;
		DateTime = dateTime;
		this.loginId = loginId;
		this.flag = flag;
		this.priority = priority;
	}




	public long getId() {
		return id;
	}




	public void setId(long id) {
		this.id = id;
	}




	public String getPhoneDirectoryName() {
		return phoneDirectoryName;
	}




	public void setPhoneDirectoryName(String phoneDirectoryName) {
		this.phoneDirectoryName = phoneDirectoryName;
	}




	public String getContactGroupName() {
		return contactGroupName;
	}




	public void setContactGroupName(String contactGroupName) {
		this.contactGroupName = contactGroupName;
	}




	public String getDepartmentName() {
		return departmentName;
	}




	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}




	public String getGrade() {
		return grade;
	}




	public void setGrade(String grade) {
		this.grade = grade;
	}




	public String getSmsName() {
		return smsName;
	}




	public void setSmsName(String smsName) {
		this.smsName = smsName;
	}




	public String getLocationName() {
		return locationName;
	}




	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}




	public String getProject() {
		return project;
	}




	public void setProject(String project) {
		this.project = project;
	}




	public String getSmsText() {
		return smsText;
	}




	public void setSmsText(String smsText) {
		this.smsText = smsText;
	}




	public String getMobile() {
		return mobile;
	}




	public void setMobile(String mobile) {
		this.mobile = mobile;
	}




	public String getScheduleType() {
		return scheduleType;
	}




	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}




	public String getScheduleDate() {
		return scheduleDate;
	}




	public void setScheduleDate(String scheduleDate) {
		this.scheduleDate = scheduleDate;
	}




	public String getScheduleTime() {
		return scheduleTime;
	}




	public void setScheduleTime(String scheduleTime) {
		this.scheduleTime = scheduleTime;
	}




	public String getDateTime() {
		return DateTime;
	}




	public void setDateTime(String dateTime) {
		DateTime = dateTime;
	}




	public String getLoginId() {
		return loginId;
	}




	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}




	public boolean isFlag() {
		return flag;
	}




	public void setFlag(boolean flag) {
		this.flag = flag;
	}




	public String getPriority() {
		return priority;
	}




	public void setPriority(String priority) {
		this.priority = priority;
	}




	@Override
	public String toString() {
		return "TempEntity [id=" + id + ", phoneDirectoryName=" + phoneDirectoryName + ", contactGroupName="
				+ contactGroupName + ", departmentName=" + departmentName + ", grade=" + grade + ", smsName=" + smsName
				+ ", locationName=" + locationName + ", project=" + project + ", smsText=" + smsText + ", mobile="
				+ mobile + ", scheduleType=" + scheduleType + ", scheduleDate=" + scheduleDate + ", scheduleTime="
				+ scheduleTime + ", DateTime=" + DateTime + ", loginId=" + loginId + ", flag=" + flag + ", priority="
				+ priority + "]";
	}





	
	}
