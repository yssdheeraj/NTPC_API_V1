package com.yss.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class GroupWithContact {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@OneToOne
	@JoinColumn(name = "contact_groupId")
	private ContactGroup contactGroup;
	
	@OneToOne
	@JoinColumn(name = "phone_directory_id")
	private PhoneDirectory phoneDirectory;
	
	@OneToOne
	@JoinColumn(name = "loginId")
	private UserEntity loginId;
	
	@Column(name="create_Date")
	private String createDate;

	
	
	
	public GroupWithContact() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	public GroupWithContact(long id, ContactGroup contactGroup, PhoneDirectory phoneDirectory, UserEntity loginId,
			String createDate) {
		super();
		this.id = id;
		this.contactGroup = contactGroup;
		this.phoneDirectory = phoneDirectory;
		this.loginId = loginId;
		this.createDate = createDate;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
	public ContactGroup getContactGroup() {
		return contactGroup;
	}

	public void setContactGroup(ContactGroup contactGroup) {
		this.contactGroup = contactGroup;
	}

	public PhoneDirectory getPhoneDirectory() {
		return phoneDirectory;
	}


	public void setPhoneDirectory(PhoneDirectory phoneDirectory) {
		this.phoneDirectory = phoneDirectory;
	}

	public UserEntity getLoginId() {
		return loginId;
	}

	public void setLoginId(UserEntity loginId) {
		this.loginId = loginId;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}


	@Override
	public String toString() {
		return "GroupWithContact [id=" + id + ", contactGroup=" + contactGroup + ", phoneDirectory=" + phoneDirectory
				+ ", loginId=" + loginId + ", createDate=" + createDate + "]";
	}


}
