package com.yss.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_url")
public class UrlEntity {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="urlId")
	private Long urlId;
	
	@Column(name="protocol")
	private String protocol;
	
	@Column(name="domainName")
	private String domainName;
	
	@Column(name="sender")
	private String parameter1;
	
	@Column(name="route")
	private String parameter2;
	
	@Column(name="mobiles")
	private String parameter3;
	
	@Column(name="authkey")
	private String parameter4;
	
	@Column(name="country")
	private String parameter5;
	
	@Column(name="message")
	private String parameter6;

	@Column(name="urlName")
	private String urlName;

	
	public UrlEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Long getUrlId() {
		return urlId;
	}


	public void setUrlId(Long urlId) {
		this.urlId = urlId;
	}


	public String getProtocol() {
		return protocol;
	}


	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}


	public String getDomainName() {
		return domainName;
	}


	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}


	public String getParameter1() {
		return parameter1;
	}


	public void setParameter1(String parameter1) {
		this.parameter1 = parameter1;
	}


	public String getParameter2() {
		return parameter2;
	}


	public void setParameter2(String parameter2) {
		this.parameter2 = parameter2;
	}


	public String getParameter3() {
		return parameter3;
	}


	public void setParameter3(String parameter3) {
		this.parameter3 = parameter3;
	}


	public String getParameter4() {
		return parameter4;
	}


	public void setParameter4(String parameter4) {
		this.parameter4 = parameter4;
	}


	public String getParameter5() {
		return parameter5;
	}


	public void setParameter5(String parameter5) {
		this.parameter5 = parameter5;
	}


	public String getParameter6() {
		return parameter6;
	}


	public void setParameter6(String parameter6) {
		this.parameter6 = parameter6;
	}


	public String getUrlName() {
		return urlName;
	}


	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}


	@Override
	public String toString() {
		return "UrlEntity [urlId=" + urlId + ", protocol=" + protocol + ", domainName=" + domainName + ", parameter1="
				+ parameter1 + ", parameter2=" + parameter2 + ", parameter3=" + parameter3 + ", parameter4="
				+ parameter4 + ", parameter5=" + parameter5 + ", parameter6=" + parameter6 + ", urlName=" + urlName
				+ "]";
	}


	public UrlEntity(Long urlId, String protocol, String domainName, String parameter1, String parameter2,
			String parameter3, String parameter4, String parameter5, String parameter6, String urlName) {
		super();
		this.urlId = urlId;
		this.protocol = protocol;
		this.domainName = domainName;
		this.parameter1 = parameter1;
		this.parameter2 = parameter2;
		this.parameter3 = parameter3;
		this.parameter4 = parameter4;
		this.parameter5 = parameter5;
		this.parameter6 = parameter6;
		this.urlName = urlName;
	}

	
	
	
}
