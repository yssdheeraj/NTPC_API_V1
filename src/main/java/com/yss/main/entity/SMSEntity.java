package com.yss.main.entity;

import java.io.File;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name="sms_draft")
public class SMSEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column
	private String smsName;
	
	
	@Column
	private String smsDesc;
	
	@Column
	private String fileName;
	
	@Column(name="voiceSms",nullable=true)
	private File audioSms;
	
	@OneToOne
	@JoinColumn(name = "loginId")
	private UserEntity loginId;
	
	@Column
	private String createDate;
	
	 @Column(name="flag", columnDefinition = "boolean default false")
	private boolean flag;
	
	
	
	
	public SMSEntity() {
		super();
		// TODO Auto-generated constructor stub
	}




	public SMSEntity(long id, String smsName, String smsDesc, String fileName, File audioSms, UserEntity loginId,
			String createDate, boolean flag) {
		super();
		this.id = id;
		this.smsName = smsName;
		this.smsDesc = smsDesc;
		this.fileName = fileName;
		this.audioSms = audioSms;
		this.loginId = loginId;
		this.createDate = createDate;
		this.flag = flag;
	}




	public long getId() {
		return id;
	}




	public void setId(long id) {
		this.id = id;
	}




	public String getSmsName() {
		return smsName;
	}




	public void setSmsName(String smsName) {
		this.smsName = smsName;
	}




	public String getSmsDesc() {
		return smsDesc;
	}




	public void setSmsDesc(String smsDesc) {
		this.smsDesc = smsDesc;
	}




	public String getFileName() {
		return fileName;
	}




	public void setFileName(String fileName) {
		this.fileName = fileName;
	}




	public File getAudioSms() {
		return audioSms;
	}




	public void setAudioSms(File audioSms) {
		this.audioSms = audioSms;
	}




	public UserEntity getLoginId() {
		return loginId;
	}




	public void setLoginId(UserEntity loginId) {
		this.loginId = loginId;
	}




	public String getCreateDate() {
		return createDate;
	}




	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}




	public boolean isFlag() {
		return flag;
	}




	public void setFlag(boolean flag) {
		this.flag = flag;
	}




	@Override
	public String toString() {
		return "SMSEntity [id=" + id + ", smsName=" + smsName + ", smsDesc=" + smsDesc + ", fileName=" + fileName
				+ ", audioSms=" + audioSms + ", loginId=" + loginId + ", createDate=" + createDate + ", flag=" + flag
				+ "]";
	}

		
}
