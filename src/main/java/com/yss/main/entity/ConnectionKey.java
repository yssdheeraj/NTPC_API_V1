package com.yss.main.entity;


public class ConnectionKey {

	
	private long id;
	private boolean result;
	public ConnectionKey() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ConnectionKey(long id, boolean result) {
		super();
		this.id = id;
		this.result = result;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	@Override
	public String toString() {
		return "ConnectionKey [id=" + id + ", result=" + result + "]";
	}
	
}
