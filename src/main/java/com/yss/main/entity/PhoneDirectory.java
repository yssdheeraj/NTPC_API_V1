package com.yss.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="customer_data")
public class PhoneDirectory {
	
	
	//PERNR, FIRSTNAME, LASTNAME, GRADE, DEPT, PROJECT, LOCATION, PHONE, EMAIL, SUBSTGRADE
//	@Id
//	@GeneratedValue(strategy=GenerationType.AUTO)
//	@Column(name="customerid")
//	private long contactId;
	
	@Id
	
	@Column(name="pernr")
	private String contactId;
	
	@Column(name="name")
	private String Name;
	
	@Column(name="mobile_No")
	private String mobileNo1;
	
	@Column(name="grade")
	private String grade;
	
	@Column(name="project")
	private String project;
	
	@Column(name="substgrade")
	private String substgrade;
		
//	@ManyToOne
//	@JoinColumn(name="department_Id")
//	private DepartmentEntity department;
//		
	private String departmentName;
	private String locationName;
	
	 @Column(name="personal", columnDefinition = "boolean default true")
	private boolean personal;
	
	@Column(name="emp_Id")
	private String empId;
	
	
	
	
	@Column(name="email_Id")
	private String emailId;
		
	
	
	@Column(name="date_of_birth")
	private String dateofbirth;
	
	
	@Column(name="create_Date")
	private String createDate;
	 
		
	@OneToOne
	@JoinColumn(name = "loginId")
	private UserEntity loginId;
	
	 @Column(name="flag", columnDefinition = "boolean default false")
		private boolean flag;
		
	
	public PhoneDirectory() {
		super();
		// TODO Auto-generated constructor stub
	}


	public PhoneDirectory(String contactId, String name, String mobileNo1, String grade, String project,
			String substgrade, String departmentName, String locationName, boolean personal, String empId,
			String emailId, String dateofbirth, String createDate, UserEntity loginId, boolean flag) {
		super();
		this.contactId = contactId;
		Name = name;
		this.mobileNo1 = mobileNo1;
		this.grade = grade;
		this.project = project;
		this.substgrade = substgrade;
		this.departmentName = departmentName;
		this.locationName = locationName;
		this.personal = personal;
		this.empId = empId;
		this.emailId = emailId;
		this.dateofbirth = dateofbirth;
		this.createDate = createDate;
		this.loginId = loginId;
		this.flag = flag;
	}


	public String getContactId() {
		return contactId;
	}


	public void setContactId(String contactId) {
		this.contactId = contactId;
	}


	public String getName() {
		return Name;
	}


	public void setName(String name) {
		Name = name;
	}


	public String getMobileNo1() {
		return mobileNo1;
	}


	public void setMobileNo1(String mobileNo1) {
		this.mobileNo1 = mobileNo1;
	}


	public String getGrade() {
		return grade;
	}


	public void setGrade(String grade) {
		this.grade = grade;
	}


	public String getProject() {
		return project;
	}


	public void setProject(String project) {
		this.project = project;
	}


	public String getSubstgrade() {
		return substgrade;
	}


	public void setSubstgrade(String substgrade) {
		this.substgrade = substgrade;
	}


	public String getDepartmentName() {
		return departmentName;
	}


	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}


	public String getLocationName() {
		return locationName;
	}


	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}


	public boolean isPersonal() {
		return personal;
	}


	public void setPersonal(boolean personal) {
		this.personal = personal;
	}


	public String getEmpId() {
		return empId;
	}


	public void setEmpId(String empId) {
		this.empId = empId;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getDateofbirth() {
		return dateofbirth;
	}


	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}


	public String getCreateDate() {
		return createDate;
	}


	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}


	public UserEntity getLoginId() {
		return loginId;
	}


	public void setLoginId(UserEntity loginId) {
		this.loginId = loginId;
	}


	public boolean isFlag() {
		return flag;
	}


	public void setFlag(boolean flag) {
		this.flag = flag;
	}


	@Override
	public String toString() {
		return "PhoneDirectory [contactId=" + contactId + ", Name=" + Name + ", mobileNo1=" + mobileNo1 + ", grade="
				+ grade + ", project=" + project + ", substgrade=" + substgrade + ", departmentName=" + departmentName
				+ ", locationName=" + locationName + ", personal=" + personal + ", empId=" + empId + ", emailId="
				+ emailId + ", dateofbirth=" + dateofbirth + ", createDate=" + createDate + ", loginId=" + loginId
				+ ", flag=" + flag + "]";
	}



}
