package com.yss.main.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity

public class ChangePasswordEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6758342212379945137L;

	@Id
	private String loginId;
	
	private String password;
	
	private String oldpassword;
	
	
	
	public ChangePasswordEntity() {
		super();
		// TODO Auto-generated constructor stub
	}



	public ChangePasswordEntity(String loginId, String password, String oldpassword) {
		super();
		this.loginId = loginId;
		this.password = password;
		this.oldpassword = oldpassword;
	}



	public String getLoginId() {
		return loginId;
	}



	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getOldpassword() {
		return oldpassword;
	}



	public void setOldpassword(String oldpassword) {
		this.oldpassword = oldpassword;
	}



	@Override
	public String toString() {
		return "ChangePasswordEntity [loginId=" + loginId + ", password=" + password + ", oldpassword=" + oldpassword
				+ "]";
	}

	
	
}
