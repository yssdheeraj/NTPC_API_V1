package com.yss.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_incomingsms")
public class IncomingEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="Fld_RowID")
	private long id;
	
	@Column(name="Fld_SMSID")
	private String simId;
	
	@Column(name="FLd_TrunkNumber")
	private String trunkNumber;
	
	@Column(name="Fld_Action")
	private String action;
	
	@Column(name="Fld_CLI")
	private String cli;
	
	@Column(name="Fld_CLIName")
	private String cliName;
	
	@Column(name="Fld_DateTime")
	private String dataTime;
	
	@Column(name="Fld_SMSMessage")
	private String smsMessage;
	@Column(name="Fld_InsertedDateTime")
	private String insertDate;
	public IncomingEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public IncomingEntity(long id, String simId, String trunkNumber, String action, String cli, String cliName,
			String dataTime, String smsMessage, String insertDate) {
		super();
		this.id = id;
		this.simId = simId;
		this.trunkNumber = trunkNumber;
		this.action = action;
		this.cli = cli;
		this.cliName = cliName;
		this.dataTime = dataTime;
		this.smsMessage = smsMessage;
		this.insertDate = insertDate;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSimId() {
		return simId;
	}
	public void setSimId(String simId) {
		this.simId = simId;
	}
	public String getTrunkNumber() {
		return trunkNumber;
	}
	public void setTrunkNumber(String trunkNumber) {
		this.trunkNumber = trunkNumber;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getCli() {
		return cli;
	}
	public void setCli(String cli) {
		this.cli = cli;
	}
	public String getCliName() {
		return cliName;
	}
	public void setCliName(String cliName) {
		this.cliName = cliName;
	}
	public String getDataTime() {
		return dataTime;
	}
	public void setDataTime(String dataTime) {
		this.dataTime = dataTime;
	}
	public String getSmsMessage() {
		return smsMessage;
	}
	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}
	public String getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(String insertDate) {
		this.insertDate = insertDate;
	}
	@Override
	public String toString() {
		return "IncomingEntity [id=" + id + ", simId=" + simId + ", trunkNumber=" + trunkNumber + ", action=" + action
				+ ", cli=" + cli + ", cliName=" + cliName + ", dataTime=" + dataTime + ", smsMessage=" + smsMessage
				+ ", insertDate=" + insertDate + "]";
	}
	

}
