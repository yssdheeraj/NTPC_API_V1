package com.yss.main.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_fileUploadDetails")
public class FileUploadEntity implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6870067405078737048L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
	@Column(name="loginId")
	private String loginId;
	
	@Column(name="uploadTextDocument")
	private String uploadTextDocument;
	
	
	 @Column(name="flag", columnDefinition = "boolean default false")
	private boolean flag;
	
	
	
//	String fileName;
//	String filePath;
//	boolean allowSms;
//	boolean allowVoice;
//	String createBY;
//	String createDate;
//	String location;
//	
	public FileUploadEntity() {
		super();
		// TODO Auto-generated constructor stub
	}



	public FileUploadEntity(long id, String loginId, String uploadTextDocument, boolean flag) {
		super();
		this.id = id;
		this.loginId = loginId;
		this.uploadTextDocument = uploadTextDocument;
		this.flag = flag;
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getLoginId() {
		return loginId;
	}



	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}



	public String getUploadTextDocument() {
		return uploadTextDocument;
	}



	public void setUploadTextDocument(String uploadTextDocument) {
		this.uploadTextDocument = uploadTextDocument;
	}



	public boolean isFlag() {
		return flag;
	}



	public void setFlag(boolean flag) {
		this.flag = flag;
	}



	@Override
	public String toString() {
		return "FileUploadEntity [id=" + id + ", loginId=" + loginId + ", uploadTextDocument=" + uploadTextDocument
				+ ", flag=" + flag + "]";
	}



}
