package com.yss.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_outsms")
public class InstanceMessageDetails {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Fld_SMSID")
	private long id;

	@Column(name = "Fld_TrunkNumber")
	private String Fld_TrunkNumber;

	

	@Column(name = "Fld_SMSText", nullable = true, columnDefinition = "varchar(255) default ''")
	private String smsText;

	// private String smsVoice;

	@Column(name = "Fld_MobileNo", nullable = true, columnDefinition = "varchar(255) default 0")
	private String mobile;

	// @Column(name="scheduleType")
	// private String scheduleType;

	// @Column(name="scheduled" , nullable = true, columnDefinition = "varchar(255)
	// default ''")
	// private String scheduleDate;

	@Column(name = "Fld_SentDateTime")
	private String messageDate;

	// @Column(name="sendBy")
	// private String sendBy;

	@Column(name = "Fld_isSent", columnDefinition = "boolean default false", nullable = true)
	private boolean flag;

	// @Column(name="priorty" , nullable = true, columnDefinition = "varchar(255)
	// default 0")
	// private String priority;

	@Column(name = "Fld_Status", columnDefinition = "boolean default false", nullable = true)
	private boolean Fld_Status;
	

	@Column(name = "Fld_RetryAttempt", columnDefinition = "boolean default false", nullable = true)
	private boolean Fld_RetryAttempt;

	public InstanceMessageDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFld_TrunkNumber() {
		return Fld_TrunkNumber;
	}

	public void setFld_TrunkNumber(String fld_TrunkNumber) {
		Fld_TrunkNumber = fld_TrunkNumber;
	}

	public String getSmsText() {
		return smsText;
	}

	public void setSmsText(String smsText) {
		this.smsText = smsText;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMessageDate() {
		return messageDate;
	}

	public void setMessageDate(String messageDate) {
		this.messageDate = messageDate;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public boolean isFld_Status() {
		return Fld_Status;
	}

	public void setFld_Status(boolean fld_Status) {
		Fld_Status = fld_Status;
	}

	public boolean isFld_RetryAttempt() {
		return Fld_RetryAttempt;
	}

	public void setFld_RetryAttempt(boolean fld_RetryAttempt) {
		Fld_RetryAttempt = fld_RetryAttempt;
	}

	public InstanceMessageDetails(long id, String fld_TrunkNumber, String smsText, String mobile, String messageDate,
			boolean flag, boolean fld_Status, boolean fld_RetryAttempt) {
		super();
		this.id = id;
		Fld_TrunkNumber = fld_TrunkNumber;
		this.smsText = smsText;
		this.mobile = mobile;
		this.messageDate = messageDate;
		this.flag = flag;
		Fld_Status = fld_Status;
		Fld_RetryAttempt = fld_RetryAttempt;
	}

	@Override
	public String toString() {
		return "InstanceMessageDetails [id=" + id + ", Fld_TrunkNumber=" + Fld_TrunkNumber + ", smsText=" + smsText
				+ ", mobile=" + mobile + ", messageDate=" + messageDate + ", flag=" + flag + ", Fld_Status="
				+ Fld_Status + ", Fld_RetryAttempt=" + Fld_RetryAttempt + "]";
	}


	
}
