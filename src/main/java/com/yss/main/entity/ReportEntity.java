package com.yss.main.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "tbl_outsmshistory")
public class ReportEntity  implements Serializable{

	
	private static final long serialVersionUID = -6589342057704836094L;

	@Id
	@Column(name="Fld_RowID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;



	@Column(name="Fld_SMSID")
	private String Fld_SMSID;
	



	@Column(name = "Fld_SMSText", nullable = true, columnDefinition = "varchar(255) default ''")
	private String smsText;

	@Column(name = "Fld_SentDateTime", nullable = true, columnDefinition = "varchar(255) default ''")
	private String scheduleDate;
	
	
	@Column(name = "Fld_TrunkNumber")
	private String Fld_TrunkNumber;




	@Column(name = "Fld_CLI", nullable = true, columnDefinition = "varchar(255) default 0")
	private String mobile;
	
	@Column(name = "Reponse", nullable = true, columnDefinition = "varchar(255) default ''")
	private String Reponse;


	@Column(name = "Fld_IsSent", columnDefinition = "boolean default false", nullable = true)
	private boolean flag;


	@Column(name = "Fld_RetryAttempt", columnDefinition = "boolean default false", nullable = true)
	private boolean Fld_RetryAttempt;

	@Column(name = "Fld_Status", columnDefinition = "boolean default false", nullable = true)
	private boolean Fld_Status;


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "insertdatetime" , nullable = false, columnDefinition = "timestamp default CURRENT_TIMESTAMP")
	private Date insertdatetime;
	
	
	@Column(name="loginId")
	private String loginId;
	
	public ReportEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFld_SMSID() {
		return Fld_SMSID;
	}

	public void setFld_SMSID(String fld_SMSID) {
		Fld_SMSID = fld_SMSID;
	}

	public String getSmsText() {
		return smsText;
	}

	public void setSmsText(String smsText) {
		this.smsText = smsText;
	}

	public String getScheduleDate() {
		return scheduleDate;
	}

	public void setScheduleDate(String scheduleDate) {
		this.scheduleDate = scheduleDate;
	}

	public String getFld_TrunkNumber() {
		return Fld_TrunkNumber;
	}

	public void setFld_TrunkNumber(String fld_TrunkNumber) {
		Fld_TrunkNumber = fld_TrunkNumber;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getReponse() {
		return Reponse;
	}

	public void setReponse(String reponse) {
		Reponse = reponse;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public boolean isFld_RetryAttempt() {
		return Fld_RetryAttempt;
	}

	public void setFld_RetryAttempt(boolean fld_RetryAttempt) {
		Fld_RetryAttempt = fld_RetryAttempt;
	}

	public boolean isFld_Status() {
		return Fld_Status;
	}

	public void setFld_Status(boolean fld_Status) {
		Fld_Status = fld_Status;
	}

	public Date getInsertdatetime() {
		return insertdatetime;
	}

	public void setInsertdatetime(Date insertdatetime) {
		this.insertdatetime = insertdatetime;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	@Override
	public String toString() {
		return "ReportEntity [id=" + id + ", Fld_SMSID=" + Fld_SMSID + ", smsText=" + smsText + ", scheduleDate="
				+ scheduleDate + ", Fld_TrunkNumber=" + Fld_TrunkNumber + ", mobile=" + mobile + ", Reponse=" + Reponse
				+ ", flag=" + flag + ", Fld_RetryAttempt=" + Fld_RetryAttempt + ", Fld_Status=" + Fld_Status
				+ ", insertdatetime=" + insertdatetime + ", loginId=" + loginId + "]";
	}

	public ReportEntity(long id, String fld_SMSID, String smsText, String scheduleDate, String fld_TrunkNumber,
			String mobile, String reponse, boolean flag, boolean fld_RetryAttempt, boolean fld_Status,
			Date insertdatetime, String loginId) {
		super();
		this.id = id;
		Fld_SMSID = fld_SMSID;
		this.smsText = smsText;
		this.scheduleDate = scheduleDate;
		Fld_TrunkNumber = fld_TrunkNumber;
		this.mobile = mobile;
		Reponse = reponse;
		this.flag = flag;
		Fld_RetryAttempt = fld_RetryAttempt;
		Fld_Status = fld_Status;
		this.insertdatetime = insertdatetime;
		this.loginId = loginId;
	}


	
	
}
