package com.yss.main.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


public class NewsListEntity {

	@Id
	private Long newsId;

	private String newsTitle;
	
//	@OneToOne
//	@JoinColumn(name="addBy")
//	private UserEntity loginId;

	
	public NewsListEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Long getNewsId() {
		return newsId;
	}


	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}


	public String getNewsTitle() {
		return newsTitle;
	}


	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}


	


	}
