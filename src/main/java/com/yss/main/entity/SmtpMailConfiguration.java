package com.yss.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="mail_config")
public class SmtpMailConfiguration {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	long id;
	
	@Column(name="smtp_server")
	private String smtpServer;
	
	@Column(name="smtp_port")
	private String smtpPort;
	
	@Column(name="sender_mailid")
	private String senderMailId;
	
	@Column(name="sender_password")
	private String senderPassword;
	
	@Column(name="subject")
	private String subject;
	
	@Column(name="locationId")
	private String locationId;
	
	@OneToOne
	@JoinColumn(name = "loginId")
	private UserEntity loginId;
	
	
	public SmtpMailConfiguration() {
		super();
		// TODO Auto-generated constructor stub
	}


	public SmtpMailConfiguration(long id, String smtpServer, String smtpPort, String senderMailId, String senderPassword,
			String subject, String locationId, UserEntity loginId) {
		super();
		this.id = id;
		this.smtpServer = smtpServer;
		this.smtpPort = smtpPort;
		this.senderMailId = senderMailId;
		this.senderPassword = senderPassword;
		this.subject = subject;
		this.locationId = locationId;
		this.loginId = loginId;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getSmtpServer() {
		return smtpServer;
	}


	public void setSmtpServer(String smtpServer) {
		this.smtpServer = smtpServer;
	}


	public String getSmtpPort() {
		return smtpPort;
	}


	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}


	public String getSenderMailId() {
		return senderMailId;
	}


	public void setSenderMailId(String senderMailId) {
		this.senderMailId = senderMailId;
	}


	public String getSenderPassword() {
		return senderPassword;
	}


	public void setSenderPassword(String senderPassword) {
		this.senderPassword = senderPassword;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getLocationId() {
		return locationId;
	}


	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}


	public UserEntity getLoginId() {
		return loginId;
	}


	public void setLoginId(UserEntity loginId) {
		this.loginId = loginId;
	}


	@Override
	public String toString() {
		return "SmtpMailConfiguration [id=" + id + ", smtpServer=" + smtpServer + ", smtpPort=" + smtpPort
				+ ", senderMailId=" + senderMailId + ", senderPassword=" + senderPassword + ", subject=" + subject
				+ ", locationId=" + locationId + ", loginId=" + loginId + "]";
	}



	
	
}
