package com.yss.main.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_loginfailure")
public class LoginFailureEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7575109130334191113L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "loginId")
	private String loginId;
	
	@Column(name = "passsword")
	private String password;
	
	@Column(name="loginfailure")
	private String loginfailure;
	
	@Column(name="dateTime")
	private String dateTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLoginfailure() {
		return loginfailure;
	}

	public void setLoginfailure(String loginfailure) {
		this.loginfailure = loginfailure;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	
	
}
