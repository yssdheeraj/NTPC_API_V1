package com.yss.main.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.ContactGroup;
import com.yss.main.entity.PaginationDataProvider;
import com.yss.main.entity.PhoneDirectory;
import com.yss.main.entity.ReportEntity;
import com.yss.main.entity.ScheduleMessageEntity;
import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;
import com.yss.main.sms.service.DataProvider;
import com.yss.main.util.Constants;

@RestController
@CrossOrigin
public class PaginationController {

	
	private static final Logger logger = LoggerFactory.getLogger(PaginationController.class);
	
	@Autowired 
	private IGenericService<UserEntity> userService;
	
	@Autowired
	private IGenericService<PhoneDirectory> phoneService;
	
	@Autowired 
	private IGenericService<ReportEntity> reportService;
	
	@Autowired 
	private IGenericService<ScheduleMessageEntity> scheduleService;
	
	@Autowired 
	private IGenericService<ContactGroup> groupService;
	
	
	
	@RequestMapping("/page")
	public ResponseEntity<PaginationDataProvider> getPhoneDirectory(@RequestParam(name="pageNumber") int pageNumber,
			@RequestParam(name="size") int size,
			@RequestParam(value = "loginId", required = false) String loginId,
			@RequestParam(value = "value", required = false) String value,
			@RequestParam(value = "field", required = false) String field){
		String query="";
		logger.info("working.......****************************......"+value+"......."+field +"......................");
		
	//	UserEntity user=userService.find(new UserEntity(),"where loginId ='"+loginId+"'");
	//	query=DataProvider.getQueryCondition(user);
		if((field.equals("") && value.equals("")) || (field==null && value==null))
		{
			query= "where flag=false";
		}else {
			query="where "+field+" like  '"+value+"%'";
		}
		
		
		logger.info("query "+query);
			
		System.out.println(pageNumber+"  "+size);
		PaginationDataProvider paginationDataProvider=new PaginationDataProvider();
		
		//String query="WHERE locationName='Noida'";
		List<PhoneDirectory> phoneList=phoneService.fetch(new PhoneDirectory(), pageNumber, size,query);
		System.out.println("List value "+phoneList);
		paginationDataProvider.setNumberOfData(phoneList);
		paginationDataProvider.setPageNumber(pageNumber);
		paginationDataProvider.setPageSize(size);
		paginationDataProvider.setTotalNumberOfPage(Constants.TOTAL_NUMBER_OF_PAGE);
		paginationDataProvider.setTotalNumberOfData(Constants.TOTAL_NUMBER_OF_DATA);
		
		
		return new ResponseEntity<PaginationDataProvider>(paginationDataProvider,HttpStatus.OK);
	}
	
	
	
	@RequestMapping("/report1")
	public ResponseEntity<PaginationDataProvider> getReportList(@RequestParam(name="pageNumber",required = false) int pageNumber,
			@RequestParam(name="size",required = false) int size,
			@RequestParam(name="loginId",required = true) String loginId,
			@RequestParam(value="dateFrom",required = false) String dateFrom,
			@RequestParam(value="dateTo",required = false) String dateTo
			){
			
		
		
		logger.info("\n ****************************************************\n "+pageNumber+"  "+size+"   "+loginId+"    "+dateFrom+"   "+dateTo+" "
				+ "\n ************************************");
		//String query="where scheduleDate>='"+dateFrom+"' AND scheduleDate <='"+dateTo+"'";
		
		
		System.out.println(dateFrom+"####  1  ####"+dateTo);
		System.out.println(pageNumber+"  "+size);
		String query="";
		PaginationDataProvider paginationDataProvider=new PaginationDataProvider();
		
		UserEntity user=userService.find(new UserEntity(), "where loginId="+loginId);
		query = DataProvider.getQueryConditionPagination(user, userService);
		logger.info(" **********************************************"+loginId);
			
//		
//		if(dateFrom !=null && !dateFrom.equals("undefined")) {
//			query="Where loginId='"+loginId+"' and scheduleDate>='"+dateFrom+"' AND scheduleDate <='"+dateTo+"'";
//		}else {
//			query="Where loginId='"+loginId+"'";
//		}
		
		
		List<ReportEntity> reportList=reportService.fetch(new ReportEntity(), pageNumber, size,query);
		System.out.println("Report value "+reportList);
		paginationDataProvider.setReportDataList(reportList);
		paginationDataProvider.setPageNumber(pageNumber);
		paginationDataProvider.setPageSize(size);
		paginationDataProvider.setTotalNumberOfPage(Constants.TOTAL_NUMBER_OF_PAGE);
		paginationDataProvider.setTotalNumberOfData(Constants.TOTAL_NUMBER_OF_DATA);
		
		return new ResponseEntity<PaginationDataProvider>(paginationDataProvider,HttpStatus.OK);
	}
	
	
	
	
	@RequestMapping("/report")
	public ResponseEntity<PaginationDataProvider> getReport(@RequestParam(name="pageNumber",required = false) int pageNumber,
			@RequestParam(name="size",required = false) int size,
			@RequestParam(name="loginId",required = true) String loginId,
			@RequestParam(value="dateFrom",required = false) String dateFrom,
			@RequestParam(value="dateTo",required = false) String dateTo
			){
			
		
		
		logger.info("\n ****************************************************\n "+pageNumber+"  "+size+"   "+loginId+"    "+dateFrom+"   "+dateTo+" "
				+ "\n ************************************");
		//String query="where scheduleDate>='"+dateFrom+"' AND scheduleDate <='"+dateTo+"'";
		
		
		System.out.println(dateFrom+"####  1  ####"+dateTo);
		System.out.println(pageNumber+"  "+size);
		String query="";
		PaginationDataProvider paginationDataProvider=new PaginationDataProvider();
		
		
		
		if(loginId.substring(0,3).equals("all")) {
			int end=loginId.length();
			loginId=loginId.substring(3,end);
			logger.info("my loginId is "+loginId);
			query="where loginId = '"+loginId+"'";
			UserEntity user=userService.find(new UserEntity(),query);
			query=DataProvider.getQueryConditionPagination(user, userService);
			logger.info(query+" **********************************************"+loginId);
		}else if(loginId !=null) {
			query="Where loginId='"+loginId+"'";
		}
		
		if(dateFrom !=null && !dateFrom.equals("undefined") && !dateFrom.equals("") && dateTo !=null && !dateTo.equals("undefined") && !dateTo.equals("")) {
			query=query+" and scheduleDate>='"+dateFrom+"' AND scheduleDate <='"+dateTo+"'";
		}else {
			query=query+"ORDER BY insertdatetime DESC";
		}
		
		logger.info(" last query "+query);
		
		
		List<ReportEntity> reportList=reportService.fetch(new ReportEntity(), pageNumber, size,query);
		System.out.println("Report value "+reportList);
		paginationDataProvider.setReportDataList(reportList);
		paginationDataProvider.setPageNumber(pageNumber);
		paginationDataProvider.setPageSize(size);
		paginationDataProvider.setTotalNumberOfPage(Constants.TOTAL_NUMBER_OF_PAGE);
		paginationDataProvider.setTotalNumberOfData(Constants.TOTAL_NUMBER_OF_DATA);
		
		return new ResponseEntity<PaginationDataProvider>(paginationDataProvider,HttpStatus.OK);
	}
	
	
	@RequestMapping("/schedule/report")
	public ResponseEntity<PaginationDataProvider> getScheduleReport(@RequestParam(name="pageNumber") int pageNumber,
			@RequestParam(name="size") int size,
			@RequestParam(name="loginId") String loginId)
	{
		String query="";	
		System.out.println(pageNumber+"  "+size+"   "+loginId);
		PaginationDataProvider paginationDataProvider=new PaginationDataProvider();
		if(loginId.substring(0,3).equals("all")) {
			int end=loginId.length();
			loginId=loginId.substring(3,end);
			logger.info("my loginId is "+loginId);
			query="where loginId = '"+loginId+"'";
			UserEntity user=userService.find(new UserEntity(),query);
			query=DataProvider.getQueryConditionPagination(user, userService);
			logger.info(query+" **********************************************"+loginId);
		}else if(loginId !=null) {
			query="Where loginId='"+loginId+"'";
		}
		
//		query="where loginId='"+loginId+"'";
//		UserEntity user=userService.find(new UserEntity(),query);
//		query=DataProvider.getQueryConditionPagination(user, userService);
	
		logger.info(query+" **********************************************"+loginId);
		
		List<ScheduleMessageEntity> scheduleList=scheduleService.fetch(new ScheduleMessageEntity(),pageNumber, size,query);
		System.out.println("schedule value "+scheduleList);
		paginationDataProvider.setSchdeulDataList(scheduleList);
		paginationDataProvider.setPageNumber(pageNumber);
		paginationDataProvider.setPageSize(size);
		paginationDataProvider.setTotalNumberOfPage(Constants.TOTAL_NUMBER_OF_PAGE);
		paginationDataProvider.setTotalNumberOfData(Constants.TOTAL_NUMBER_OF_DATA);
		
		return new ResponseEntity<PaginationDataProvider>(paginationDataProvider,HttpStatus.OK);
	}
	
	@RequestMapping("/contact/group")
	public ResponseEntity<PaginationDataProvider> getContactGroup(@RequestParam(name="pageNumber") int pageNumber,
			@RequestParam(name="size") int size,
			@RequestParam(name="loginId") String loginId,@RequestParam(name="groupName",required=false) String groupName)
	{
		String query="";	
		System.out.println(pageNumber+"  "+size+"   "+loginId+" "+groupName);
		PaginationDataProvider paginationDataProvider=new PaginationDataProvider();
		query="where loginId='"+loginId+"'";
		UserEntity user=userService.find(new UserEntity(),query);
		query=DataProvider.getQueryConditionPagination(user, userService);
		if(user.getUserType().equals("superadmin")) {
			query="where contactGroupName !='' ";
		}
		logger.info(query+" **********************************************"+loginId);
		
		if(groupName!=null && !groupName.equals("undefined") && !groupName.equals("")) {
			query=query+" and contactGroupName Like '%"+groupName+"%'";
		}
		List<ContactGroup> groupList=groupService.fetch(new ContactGroup(),pageNumber, size,query);
		System.out.println("schedule value "+groupList);
		paginationDataProvider.setGroupsList(groupList);
		paginationDataProvider.setPageNumber(pageNumber);
		paginationDataProvider.setPageSize(size);
		paginationDataProvider.setTotalNumberOfPage(Constants.TOTAL_NUMBER_OF_PAGE);
		paginationDataProvider.setTotalNumberOfData(Constants.TOTAL_NUMBER_OF_DATA);
		
		return new ResponseEntity<PaginationDataProvider>(paginationDataProvider,HttpStatus.OK);
	}

}
