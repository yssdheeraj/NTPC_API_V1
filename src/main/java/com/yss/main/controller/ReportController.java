package com.yss.main.controller;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.PhoneDirectory;
import com.yss.main.entity.ReportEntity;
import com.yss.main.entity.ScheduleMessageEntity;
import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;



@RestController
@CrossOrigin
@RequestMapping("/report")
public class ReportController {

	
	private static final Logger logger = LoggerFactory.getLogger(ReportController.class);
	
	@Autowired
	private IGenericService<ReportEntity> reportEntitySerivce;
	
	@Autowired
	private IGenericService<UserEntity> userService;

	
	
	@RequestMapping(value="/all/Report")
	public  ResponseEntity<List<ReportEntity>> getSMSData(@RequestParam String loginId) {
		
		String query="";
		
		logger.info("working.........................................................");
		UserEntity user=userService.find(new UserEntity(),"where loginId ='"+loginId+"'");
		List<ReportEntity> reportList=new ArrayList<ReportEntity>();
		
		
		if(user.getUserType().equals("superadmin")) {
			 reportList=reportEntitySerivce.fetch(new ReportEntity());
		}
		if(user.getUserType().equals("admin")) {
			List<UserEntity> userList=userService.fetch(new UserEntity(),"where createdBy ='"+loginId+"'");
			query="WHERE loginId ='"+loginId+"'";
			for(UserEntity user1:userList){
				query=query+" OR loginId = '"+user1.getLoginId()+"'";
			}
			 reportList=reportEntitySerivce.fetch(new ReportEntity(),query);
		}
		if(user.getUserType().equals("user")) {
			query="WHERE loginId ='"+loginId+"'";
			 reportList=reportEntitySerivce.fetch(new ReportEntity(),query);
		}
		
		
		return new ResponseEntity<List<ReportEntity>>(reportList,HttpStatus.OK);
		
	}
	
	
	
	
	
	
	@RequestMapping(value="/report/filter" ,produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<ReportEntity>> filterReport(@RequestBody ReportEntity reportEntity){
	
		logger.info("Return Report Entity " +getFilterQuery(reportEntity));
	List<ReportEntity> filterList = reportEntitySerivce.findAllByCondition(new ReportEntity(),getFilterQuery(reportEntity));
	
	logger.info("####Fetch List####" + filterList);
	
	if (filterList == null) {
		logger.info("####Null Data in  Filter List####");
		return new ResponseEntity<List<ReportEntity>>(HttpStatus.NO_CONTENT);
	} 
	if(filterList.size() == 0) {
		logger.info("####No Data in Filter List####");
			return new ResponseEntity<List<ReportEntity>>(HttpStatus.NO_CONTENT);
		}
		logger.info("####Data in  Filter List####");
		return new ResponseEntity<List<ReportEntity>>(filterList,HttpStatus.OK);
	}


	private String getFilterQuery(ReportEntity reportEntity) {
		
		String query = "";
		
		if(!StringUtils.isEmpty(reportEntity.getMobile())) {
			query = query + "where mobile = '"+reportEntity.getMobile()+"' ";
		}
		
//		if(!StringUtils.isEmpty(reportEntity.getDepartmentName())) {
//			query = query +"where departmentName = '"+reportEntity.getDepartmentName()+"'";
//		}
		return query;
	}
	
	@RequestMapping(value="report/bydate")
	public ResponseEntity<List<ReportEntity>> getReportByDate(@RequestParam(value="dateFrom") String dateFrom,@RequestParam(value="dateTo") String dateTo ){
		logger.info(dateFrom+"####  1  ####"+dateTo);
		//dateFrom=dateFrom+" 12:00:00";
		//dateTo=dateTo+" 12:00:00";
	logger.info(dateFrom+"   3 "+dateTo);
	//String query="where scheduleDate>='2017-09-20 12:23:43' AND scheduleDate <='2017-09-20 12:24:13'";
		String query="where scheduleDate>='"+dateFrom+"' AND scheduleDate <='"+dateTo+"'";
		logger.info("query "+query);
		List<ReportEntity> reportList =reportEntitySerivce.fetch(new ReportEntity(),query);
		logger.info("#### ReportList DateWise ####"+reportList);
		
		
		return new ResponseEntity<>(reportList,HttpStatus.OK);
	}
	
	@GetMapping(value="total/report" ,  produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ReportEntity>> getTotalReport(){
		List<ReportEntity> reportList=new ArrayList<>();
		try {
			 reportList =reportEntitySerivce.fetch(new ReportEntity());
			logger.info("list "+reportList);
			return new ResponseEntity<>(reportList,HttpStatus.OK);
		}catch(Exception e) {
			logger.info("Exception in totals report =>"+e.getMessage());
			return new ResponseEntity<>(reportList,HttpStatus.EXPECTATION_FAILED);
		}
		
	}
	
	
}


