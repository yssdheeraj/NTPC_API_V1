package com.yss.main.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.AllContactGroup;
import com.yss.main.entity.GroupWithContact;
import com.yss.main.entity.PhoneDirectory;
import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;
import com.yss.main.sms.service.DataProvider;

@RestController
@CrossOrigin
@RequestMapping("/allcontact")
public class AllContactGroupController {

	
private static final Logger logger = LoggerFactory.getLogger(AllContactGroupController.class);

@Autowired
private IGenericService<GroupWithContact> iDataGenericService;

@Autowired
private IGenericService<PhoneDirectory> iDataPhoneDirectoryGenericService;

@Autowired
private IGenericService<UserEntity> userService;



@PostMapping(value="/addContactGrp")
public  ResponseEntity<?> saveGroup(@RequestBody AllContactGroup objAllContactGroup) {
	
	logger.info("########Contact Group Saved successfully########");
	logger.info("########Group"+objAllContactGroup.getContactGroupName());
	logger.info("########Group"+objAllContactGroup.getPhoneDirectoryId());
	logger.info("########Group"+objAllContactGroup);	
	
	for(PhoneDirectory phone:objAllContactGroup.getPhoneDirectoryId())
	{
		GroupWithContact group=new GroupWithContact();
		group.setContactGroup(objAllContactGroup.getContactGroupName());
		group.setLoginId(objAllContactGroup.getLoginId());
		group.setCreateDate(new Date().toString());
		group.setPhoneDirectory(phone);
		iDataGenericService.update(group);
	}
		logger.info("########Data saved successfully########"+objAllContactGroup.toString());
		return new ResponseEntity(HttpStatus.OK);
}


@RequestMapping(value="/allgrouplist")
public  ResponseEntity<List<GroupWithContact>> getAllContactGroupData() {

	
	 List<GroupWithContact> allgroupList=iDataGenericService.fetch(new GroupWithContact());
	 if (allgroupList != null) {
		 logger.info("########Fetch Group List Successfully########");
		 return new ResponseEntity<List<GroupWithContact>>(allgroupList,HttpStatus.OK);
	} else {
		 logger.info("########Error in Fetch Group List########");
		 return new ResponseEntity<List<GroupWithContact>>(allgroupList,HttpStatus.NOT_FOUND);
	}
	 
}


//@PostMapping("/findAllContactGroup/name")
//public ResponseEntity<?> findByAllContactGroupName(@RequestBody AllContactGroup objAllContactGroup) {
//	
//	AllContactGroup AllContactGroup=iDataGenericService.find(objAllContactGroup,"where contactGroupName = '" +objAllContactGroup.getContactGroupName()+"'");
//	logger.info("Data saved successfully !");
//	return new ResponseEntity<>(AllContactGroup,HttpStatus.CREATED);
//}

@RequestMapping(value="/removecontactfromgroup", method=RequestMethod.POST)
public  ResponseEntity<?> removeContactFromGroup(@RequestParam(value="phoneDirectoryId") long phoneDirectoryId,
										@RequestParam(value="groupId") long groupId){ 
											
	int result=0;
	
	logger.info(phoneDirectoryId+"########Data Retrive successfully########"+groupId);
		List<GroupWithContact> contactList = iDataGenericService.fetch(new GroupWithContact());
		for(GroupWithContact contact:contactList) {
			if((contact.getContactGroup().getContact_groupId()==groupId) && (contact.getPhoneDirectory().getContactId().equals(phoneDirectoryId))) {
				iDataGenericService.delete(contact);
				result=1;
				logger.info("########Data Successfully Deleted########");
				break;
			}
			else {
				result=0;
			}
		}
		if(result==1) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		 
}

@RequestMapping(value="/ongroupcontact/{id}")
public  ResponseEntity<List<PhoneDirectory>> oneGroupContact(@PathVariable ("id") long id) {
	logger.info("####One Group Data####"+id );
	 List<GroupWithContact> onegroupList=new ArrayList<GroupWithContact>();
	 GroupWithContact contact1=new GroupWithContact();
	 //for get group phone directory list 
	 List<PhoneDirectory> phoneDirectoryList=new ArrayList<PhoneDirectory>();
	 List<GroupWithContact> allgroupList=iDataGenericService.fetch(new GroupWithContact()); 
	for(GroupWithContact contact:allgroupList)
	{
		if(contact.getContactGroup().getContact_groupId()==id)
		{
			onegroupList.add(contact);
			logger.info("####Get Direcrory####"+contact.getPhoneDirectory());
			phoneDirectoryList.add(contact.getPhoneDirectory());
		}
	}
		logger.info("####Data get successfully####"+phoneDirectoryList.size());
return new ResponseEntity<List<PhoneDirectory>>(phoneDirectoryList,HttpStatus.OK);
}



@RequestMapping(value="/showallcontact/{id}")
public  ResponseEntity<List<PhoneDirectory>> showAllContactNotInGroup(@PathVariable ("id") long id,@RequestParam String loginId) {
		int temp=0;
	logger.info(loginId+"   ####One group data####"+id );
	List<GroupWithContact> onegroupList=new ArrayList<GroupWithContact>();
	 GroupWithContact contact1=new GroupWithContact();
	 //for get group phone directory list 
	// List<PhoneDirectory> groupPhoneDirectoryList=new ArrayList<PhoneDirectory>();
	 List<PhoneDirectory> notInGroupPhoneDirectoryList=new ArrayList<PhoneDirectory>();
	 List<GroupWithContact> allgroupList=iDataGenericService.fetch(new GroupWithContact(),"where contactGroup ='"+id+"'"); 
	logger.info("gggggggggggggggggggggg name "+allgroupList);

	String query="";
	UserEntity user=userService.find(new UserEntity(),"where loginId ='"+loginId+"'");
	query=DataProvider.getQueryCondition(user);
	

	List<PhoneDirectory> allPhoneDirectoryList1=iDataPhoneDirectoryGenericService.fetch(new PhoneDirectory(),query);
	
	
	for(PhoneDirectory phone:allPhoneDirectoryList1) {
		
//		for(PhoneDirectory group:groupPhoneDirectoryList) {
//			if(group.getContactId()==phone.getContactId()) {
//				temp=1;
//				break;
//			}
//		}
		
		for(GroupWithContact group:allgroupList) {
			if(group.getPhoneDirectory().getContactId()==phone.getContactId()) {
				temp=1;
				break;
			}
		}
		if(temp==1) {
		temp=0;
			logger.info("already in group"+phone);
		}
		else{
			notInGroupPhoneDirectoryList.add(phone);
		}
	}
	
logger.info("Data get successfully !"+notInGroupPhoneDirectoryList.size());
return new ResponseEntity<List<PhoneDirectory>>(notInGroupPhoneDirectoryList,HttpStatus.OK);
}



}

