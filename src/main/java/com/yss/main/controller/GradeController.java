package com.yss.main.controller;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.PhoneDirectory;
import com.yss.main.generic.service.IGenericService;


@RestController
@CrossOrigin
@RequestMapping("/grade")
public class GradeController {
	
	
	private static final Logger logger = LoggerFactory.getLogger(GradeController.class);
	
	@Autowired
	private IGenericService<PhoneDirectory> phoneService;
	
	@RequestMapping(value="/all/gradeList")
	public ResponseEntity<List<PhoneDirectory>> getGrade(){
		
	
		try {
			
			List<PhoneDirectory> gradeList = phoneService.fetch(new PhoneDirectory());
			if (gradeList.isEmpty()) {
				logger.info("#########Grade List Does not Exist########");
				return new ResponseEntity<List<PhoneDirectory>>(HttpStatus.NO_CONTENT);
			} else {
				
				logger.info(Arrays.toString(gradeList.toArray()));
				for (PhoneDirectory phoneDirectory : gradeList) {
					phoneDirectory.getGrade();
					System.out.println("Grade List : " + phoneDirectory.getGrade());
				}
				return new ResponseEntity<List<PhoneDirectory>>(gradeList,HttpStatus.OK);				
			}
			
		} catch (Exception e) {

			logger.info("########Error To Fetch Grade########" +e.getMessage());
			return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
		}
	}
	

}
