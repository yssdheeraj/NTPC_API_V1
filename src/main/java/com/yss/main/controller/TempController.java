package com.yss.main.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.LocationEntity;
import com.yss.main.entity.TempEntity;
import com.yss.main.generic.service.IGenericService;

@RestController
@CrossOrigin
@RequestMapping("/tempMessage")
public class TempController {

	
	private static final Logger logger = LoggerFactory.getLogger(TempController.class);	
	
	@Autowired
	private IGenericService<TempEntity> tempEntityService;
	
	@GetMapping("/deleteTempMessage/{id}/{loginId}")
	public ResponseEntity <List<TempEntity>> deleteTempMessage(@PathVariable("id") long id,@PathVariable("loginId") String loginId) {
		logger.info("id "+id);
		logger.info("login id "+loginId);
		TempEntity temp = tempEntityService.find(new TempEntity(),id);
		tempEntityService.delete(temp);
		logger.info("####Temp Message Deleted successfully####");
		List<TempEntity> tempList=tempEntityService.fetch(new TempEntity(), "where loginId='"+loginId+"'");
		return new ResponseEntity<List<TempEntity>>(tempList,HttpStatus.OK);
	}
		
	}
	

