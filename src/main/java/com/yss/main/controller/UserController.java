package com.yss.main.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.AddNewsEntity;
import com.yss.main.entity.NewsListEntity;
import com.yss.main.entity.UrlEntity;
import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;
import com.yss.main.sms.service.DataProvider;
import com.yss.main.util.AES;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
	

	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private IGenericService<UserEntity> iDataGenericService;

	@Autowired
	private IGenericService<UrlEntity> urlEntityService;
	
	@Autowired
	private IGenericService<AddNewsEntity> addNeweService;
	
	
	String URL_Name="";
	String LOGIN_ID="";
	
	@RequestMapping(value="/userlist")
	public  ResponseEntity<List<UserEntity>> getUserData(@RequestParam String loginId) {

		logger.info("user list "+loginId);
		String query="";
		UserEntity user=iDataGenericService.find(new UserEntity(),"where loginId ='"+loginId+"'");
		query=DataProvider.getQueryCondition(user);
		//"WHERE loginId='"+loginId+"' OR createdBy='"+loginId+"' and flag  = false"
		 List<UserEntity> userList=iDataGenericService.fetch(new UserEntity(),query);
		 if(userList==null)
		 {
			 logger.info("####User List Not Found####");
			return new ResponseEntity<List<UserEntity>>(HttpStatus.NOT_FOUND);
		 }
		 logger.info("#### Found" +userList.size()+ "User####");
		return new ResponseEntity<List<UserEntity>>(userList,HttpStatus.OK);
	}
	

	@PostMapping(value="/createuser")
	public ResponseEntity<?> saveUser(@RequestBody UserEntity objUser) throws Exception {
		
		if (objUser !=null) {
			objUser.getPassword();
			objUser.setPassword(AES.encrypt(objUser.getPassword()));
			//objUser.setPassword(PasswordEncription.generateHash(objUser.getPassword()));
			
			iDataGenericService.save(objUser);
			logger.info("####User Create Succesfully####"+objUser);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} else {
			
			logger.info("####Errro Create User####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}

	@PutMapping(value="/updateuser")
	public ResponseEntity<?> updateUser(@RequestBody UserEntity objUser) {
		
		if (objUser != null) {
			iDataGenericService.update(objUser);
			logger.info("####Update User Succcesfully####");
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			logger.info("####Error In Update User####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}

	
	@RequestMapping(value = "/deleteuser/{loginId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUser(@PathVariable ("loginId") String loginId) {
		
		logger.info("####User id ####"+loginId);
		UserEntity objUser = iDataGenericService.find(new UserEntity(),"where loginId = '"+loginId+"'");
		if (objUser == null) {
			logger.info("####User Not Found For Delete####");
			return new ResponseEntity<>(objUser, HttpStatus.NO_CONTENT);
		}
		else {
			objUser.setFlag(true);
			iDataGenericService.update(objUser);
			logger.info("####Delete User For####"+objUser);
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
	}

	@PostMapping(value="/findUser/id")
	public ResponseEntity<?> findByUserId(@RequestBody UserEntity objUser) {

		UserEntity User = iDataGenericService.find(objUser, objUser.getLoginId());
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(User, HttpStatus.CREATED);
	}

	@PostMapping(value="/findUser/name")
	public ResponseEntity<?> findByUserame(@RequestBody UserEntity objUser) {

		UserEntity User = iDataGenericService.find(objUser, objUser.getUserName());

		logger.info("Data saved successfully !");
		return new ResponseEntity<>(User, HttpStatus.CREATED);
	}

	
	@PutMapping(value="/saveUpdate/url")
	@CacheEvict(cacheNames="objUrlEntity")
	public ResponseEntity<?> saveupdateUrl(@RequestBody UrlEntity objUrlEntity){
		
//		objUrlEntity.getProtocol();
//		objUrlEntity.getDomainName();
//		objUrlEntity.getParameter1();
//		objUrlEntity.getParameter2();
//		objUrlEntity.getParameter3();
//		objUrlEntity.getParameter4();
//		objUrlEntity.getParameter5();
//		objUrlEntity.getParameter5();
//		objUrlEntity.getParameter6();
		
		if (objUrlEntity != null) {
			URL_Name = objUrlEntity.getProtocol() + "://" + objUrlEntity.getDomainName() + "?" + objUrlEntity.getParameter1() + "&" + objUrlEntity.getParameter2() + "&" + objUrlEntity.getParameter3() + "&" + objUrlEntity.getParameter4() + "&" + objUrlEntity.getParameter5() + "&" + objUrlEntity.getParameter6();
			logger.info("----------------Save URL" + URL_Name);
			objUrlEntity.setUrlName(URL_Name);
			urlEntityService.update(objUrlEntity);
			logger.info("####Update URL Succcesfully####");
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			logger.info("####Error In Update URL ####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}
	
	
	@RequestMapping(value="/url/list")
	public ResponseEntity<List<UrlEntity>> findUrl(){
		
		 List<UrlEntity> url = urlEntityService.fetch(new UrlEntity());
		logger.info("***********Enter into URL List" +url);
//		for (UrlEntity urlEntity : url) {
//			urlEntity.getProtocol();
//			urlEntity.getDomainName();
//			urlEntity.getParameter1();
//			urlEntity.getParameter2();
//			urlEntity.getParameter3();
//			urlEntity.getParameter4();
//			urlEntity.getParameter5();
//			urlEntity.getParameter5();
//			urlEntity.getParameter6();
//			URL_Name = urlEntity.getProtocol() + "://" + urlEntity.getDomainName() + "?" + urlEntity.getParameter1() + "&" + urlEntity.getParameter2() + "&" + urlEntity.getParameter3() + "&" + urlEntity.getParameter4() + "&" + urlEntity.getParameter5() + "&" + urlEntity.getParameter6();
//			logger.info("#### URL_USE_FOR_SMS #### => " + URL_Name);
//		}
		return new ResponseEntity<List<UrlEntity>>(url,HttpStatus.OK);
	}
	
	@RequestMapping(value="/usermobilenumberexist")
	public ResponseEntity<UserEntity> getUserByMobileNo(@RequestParam String mobile){
		logger.info("mobile "+mobile);
		String query="where mobile='"+mobile+"'";
		UserEntity user=iDataGenericService.find(new UserEntity(),query);
		System.out.println("ok..............................."+user);
		if(user!=null) {
			return new ResponseEntity<UserEntity>(user,HttpStatus.OK);
		}else {
			return new ResponseEntity<UserEntity>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@PostMapping(value="/save/news")
	public ResponseEntity<?> saveNews(@RequestBody List<NewsListEntity> objNewsListEntity) {
		
		//LOGIN_ID = objNewsListEntity.getLoginId().getLoginId();
		
		for (NewsListEntity newsListEntity : objNewsListEntity) {
			
			AddNewsEntity objAddNewsEntity = new AddNewsEntity();
			objAddNewsEntity.setNewsTitle(newsListEntity.getNewsTitle());
			objAddNewsEntity.setDate_time(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date()).toString());
			addNeweService.save(objAddNewsEntity);
			
		}
		logger.info(" #### News Saved Successfully #### ");
		return new ResponseEntity<>(HttpStatus.OK);
	
	}
	
	
	@GetMapping(value="/news/list")
	public ResponseEntity<?> newsList(){
	
		List<AddNewsEntity> newsList = addNeweService.fetch(new AddNewsEntity());
		return new ResponseEntity<>(newsList,HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/delete/news/{newsId}" ,method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteNews(@PathVariable("newsId") String newsId){
		
		AddNewsEntity objAddNewsEntity = addNeweService.find(new AddNewsEntity(), "Where newsId='"+newsId+"'");
		if (objAddNewsEntity !=null && !objAddNewsEntity.equals("")) {
			addNeweService.delete(objAddNewsEntity);
			logger.info(" #### News Deleted Successfully For newsId #### " + newsId);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			logger.info(" #### News Not Deleted For newsId #### " + newsId);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}
	
	
	@PutMapping(value="/update/news")
	@CacheEvict(cacheNames="objAddNewsEntity")
	public ResponseEntity<?> updateNews(@RequestBody AddNewsEntity objAddNewsEntity){
		
		if (objAddNewsEntity !=null && !objAddNewsEntity.equals("")) {
			logger.info(" #### News Update Successfully #### ");
			addNeweService.update(objAddNewsEntity);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			logger.info(" #### Error in News Update #### ");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
	}
	
}
