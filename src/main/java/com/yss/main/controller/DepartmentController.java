package com.yss.main.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.DepartmentEntity;
import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;
import com.yss.main.sms.service.DataProvider;

@RestController
@CrossOrigin
@RequestMapping("/department")
public class DepartmentController {
	
private static final Logger logger = LoggerFactory.getLogger(DepartmentController.class);
	
	@Autowired
	private IGenericService<DepartmentEntity> iDataGenericService;

	@Autowired
	private IGenericService<UserEntity> userService;
	
//	@RequestMapping("/all/department")
//	public ResponseEntity<List<DepartmentEntity>> getDepartmentData() {
//	
//		List<DepartmentEntity> departmentList=iDataGenericService.fetch(new DepartmentEntity(),"WHERE flag  = false");
//		if (departmentList  == null) {
//			logger.info("####No Department List Found####");
//			return new ResponseEntity<List<DepartmentEntity>>(HttpStatus.NOT_FOUND);
//		} else {
//			logger.info("####" + departmentList.size() + "Department Found####");
//			return new ResponseEntity<List<DepartmentEntity>>(departmentList, HttpStatus.OK);
//		}
//		
//	}
	
	
	@RequestMapping(value="/all/department")
	public ResponseEntity<List<DepartmentEntity>> getAllDepartment(@RequestParam String loginId){
		
		logger.info("login user "+loginId);
		try {
			
			//String query=DataProvider.getQueryCondition(loginId, userService);
			List<DepartmentEntity> departmentList = iDataGenericService.fetch(new DepartmentEntity());
			if (departmentList.isEmpty()) {
				logger.info("#########Department List Does not Exist########");
				return new ResponseEntity<List<DepartmentEntity>>(HttpStatus.NO_CONTENT);
			} else {
				//logger.info("#########Found " +departmentList.size()+ " Department########");
				logger.info(Arrays.toString(departmentList.toArray()));
				return new ResponseEntity<List<DepartmentEntity>>(departmentList,HttpStatus.OK);				
			}
			
		} catch (Exception e) {

			logger.info("########Error To Fetch Department########" +e.getMessage());
			return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@PostMapping(value="/department/create")
	public ResponseEntity<?> saveDepartment(@RequestBody DepartmentEntity objDepartment) {
		
		if (objDepartment !=null) {
			
			objDepartment.setCreatedDate(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date()).toString());
			iDataGenericService.save(objDepartment);
			logger.info("########Department saved successfully########");
			return new ResponseEntity<>(HttpStatus.CREATED);
		} else {
			logger.info("########Sorry Department Not Saved########");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}	
	
	}

	@PutMapping(value="/department/update")
	public ResponseEntity<?> updateDepartment(@RequestBody DepartmentEntity objDepartment) {
		
		if (objDepartment != null) {
			iDataGenericService.update(objDepartment);
			logger.info("########Dept update successfully########");
			return new ResponseEntity<>(HttpStatus.CREATED);

		} else {
			logger.info("########Sorry Department Not Found For Update########");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

//	@DeleteMapping("/deletedepartment")
//	public ResponseEntity<?> deleteDepartment(@RequestParam(value = "departmentId", required = true) long departmentId) {
//		DepartmentEntity objDepartment = iDataGenericService.find(new DepartmentEntity(), "where departmentId = " + departmentId);
//		if (objDepartment == null) {
//			return new ResponseEntity<>(objDepartment, HttpStatus.NO_CONTENT);
//		}
//		iDataGenericService.delete(objDepartment);
//		logger.info("Dept deleted successfully !");
//		return new ResponseEntity<>(HttpStatus.GONE);
//	}
	
	
	@RequestMapping(value="/department/{departmentId}" , method=RequestMethod.DELETE)
	public ResponseEntity<?> deleteDepartment(@PathVariable ("departmentId") long departmentId){
		
		DepartmentEntity dEntity = iDataGenericService.find(new DepartmentEntity(), departmentId);
		
		if (dEntity == null) {
			logger.info("########Department with Id " +departmentId + " Does Not Exist########");
			return new ResponseEntity<>(HttpStatus.FOUND);
		} else {
			dEntity.setFlag(true);
			iDataGenericService.update(dEntity);
			logger.info("########Department with Id " +departmentId + " Successfully Deleted########");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
	}

	@PostMapping(value="/finddepartment/id")
	public ResponseEntity<?> findByDepartmentId(@RequestBody DepartmentEntity objDepartment) {
		
		DepartmentEntity Department=iDataGenericService.find(objDepartment,objDepartment.getDepartmentId());
		logger.info("########Find Dept by Id########");
		return new ResponseEntity<>(Department,HttpStatus.CREATED);
	}
	
	@PostMapping("/finddepartment/name")
	public ResponseEntity<?> findByDepartmentame(@RequestBody DepartmentEntity objDepartment) {
		
		DepartmentEntity Department=iDataGenericService.find(objDepartment,"where dptName = '"+objDepartment.getDepartmentName()+"'");
		logger.info("########Data find successfully########");
		return new ResponseEntity<>(Department,HttpStatus.CREATED);
	}
	

}
