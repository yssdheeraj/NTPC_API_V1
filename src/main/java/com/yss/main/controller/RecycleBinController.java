package com.yss.main.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.LocationEntity;
import com.yss.main.generic.service.IGenericService;

@RestController
@CrossOrigin
@RequestMapping("/recyclebin")
public class RecycleBinController {

	private static final Logger logger = LoggerFactory.getLogger(RecycleBinController.class);

	
	@Autowired
	private IGenericService<LocationEntity> locationService;
	
	
	@RequestMapping(value="/get/delete/location")
	public ResponseEntity<List<LocationEntity>> getDeleteLocation() {
		List<LocationEntity> deletLocationList=locationService.findAllByCondition(new LocationEntity(), "where flag = true");
		logger.info("delete location "+deletLocationList);
		return new ResponseEntity<List<LocationEntity>>(deletLocationList,HttpStatus.OK);
	}
}
