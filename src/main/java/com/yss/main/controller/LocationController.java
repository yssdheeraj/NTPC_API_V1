package com.yss.main.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.LocationEntity;
import com.yss.main.generic.service.IGenericService;

@RestController
@CrossOrigin
@RequestMapping("/location")
public class LocationController {

	private static final Logger logger = LoggerFactory.getLogger(LocationController.class);

	@Autowired
	private IGenericService<LocationEntity> iDataGenericService;

	@RequestMapping(value="/locationlist")
	public ResponseEntity<List<LocationEntity>> getLocationData() {

		List<LocationEntity> locationList = iDataGenericService.fetch(new LocationEntity(),"WHERE flag  = false");
		if (locationList != null) {
			logger.info("####Location List Found####" +locationList);
			logger.info("####Total " +locationList.size()+ "Location Found####");
			
			return new ResponseEntity<List<LocationEntity>>(locationList, HttpStatus.OK);
		} else {
			logger.info("####Location List Not Found####");
			return new ResponseEntity<List<LocationEntity>>(HttpStatus.NOT_FOUND);
		}
		
	}

	@PostMapping(value="/createlocation")
	public ResponseEntity<?> saveLocation(@RequestBody LocationEntity objLocation) {

		if (objLocation != null) {
			logger.info("####Location Create Successfully####" + objLocation);
			objLocation.setCreateDate(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date()).toString());
			iDataGenericService.save(objLocation);
			return new ResponseEntity<>(iDataGenericService.fetch(objLocation), HttpStatus.CREATED);
		} else {
			logger.info("####Error in Create Location####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}

	@PutMapping(value="/updatelocation")
	public ResponseEntity<?> updateLocation(@RequestBody LocationEntity objLocation) {
		if (objLocation != null) {
			iDataGenericService.update(objLocation);
			logger.info("####Location Update Successfully####" + objLocation);
			return new ResponseEntity<>(iDataGenericService.fetch(objLocation),HttpStatus.CREATED);
		} else {
			logger.info("####Error in Update Location ####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}

	@RequestMapping(value="/deletelocation/{locationId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteLocation(@PathVariable("locationId") long locationId) {
		LocationEntity objLocation = iDataGenericService.find(new LocationEntity(),locationId);
		if (objLocation == null) {
			logger.info("####No Location Found For Delete####");
			return new ResponseEntity<>(objLocation, HttpStatus.NO_CONTENT);
		} else {
			objLocation.setFlag(true);
			iDataGenericService.update(objLocation);
			logger.info("####Location Deleted successfully####");
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}

	@PostMapping(value="/findlocation/id")
	public ResponseEntity<?> findByLocationId(@RequestBody LocationEntity objLocation) {

		LocationEntity location = iDataGenericService.find(objLocation, objLocation.getLocationId());
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(location, HttpStatus.CREATED);
	}

	@PostMapping(value="/findlocation/name")
	public ResponseEntity<?> findByLocationame(@RequestBody LocationEntity objLocation) {

		LocationEntity location = iDataGenericService.find(objLocation, objLocation.getLocationName());
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(location, HttpStatus.CREATED);
	}

}
