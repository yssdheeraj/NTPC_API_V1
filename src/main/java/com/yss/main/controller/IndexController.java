package com.yss.main.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.ChangePasswordEntity;
import com.yss.main.entity.ConnectionKey;
import com.yss.main.entity.LoginDetailsEntity;
import com.yss.main.entity.LoginFailureEntity;
import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;
import com.yss.main.generic.service.ISmsSender;
import com.yss.main.sms.service.DataProvider;
import com.yss.main.sms.service.PasswordEncription;
import com.yss.main.util.AES;


@RestController
@CrossOrigin
@RequestMapping("/index")
public class IndexController {

	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);
	
	
	@Autowired
	private IGenericService<UserEntity> userService;
	
	@Autowired
	private ISmsSender ismsService;
	
	@Value(value="${master.flag}")
	boolean masterflag;
	
	@Value(value= "${connection.data.key}")
	String connectiondatakey;
	
	@PersistenceContext
	EntityManager em;
	
	public static List<UserEntity> isUserLogin=new ArrayList();
	
	public static List<LoginDetailsEntity> userLoginDetails=new ArrayList();

	@Autowired
	private IGenericService<LoginDetailsEntity> logService;
	
	@Autowired
	private IGenericService<LoginFailureEntity> logfailService;
	
	@Autowired
	private DataProvider dataProvider;


	String URL_NAME="";
	int ERROR_LOGIN_COUNT=0;
	boolean checkCredential;
	
	@RequestMapping(value="/check/credential")
	public ResponseEntity<ConnectionKey> checkCredential(){
		checkCredential = dataProvider.checkCredentialStatus();
			ConnectionKey key=new ConnectionKey(1,checkCredential);
		return new ResponseEntity<>(key,HttpStatus.OK);
	}
	

	@PostMapping(value="/user/login")
	public ResponseEntity<?> doLogin(@RequestBody UserEntity objUser) throws Exception{
		
		logger.info("####Do Login####" +objUser);
		//objUser.setPassword(PasswordEncription.generateHash(objUser.getPassword()));\
		
		checkCredential = dataProvider.checkCredentialStatus();
		  
		if (checkCredential) {
			//logger.info(" #### Master Flag #### " +masterflag);
			objUser.setPassword(AES.encrypt(objUser.getPassword()));
			String query = "where loginId ='"+objUser.getLoginId()+"' and password ='"+objUser.getPassword()+"' and flag  = false";
		
			UserEntity dentity = userService.find(new UserEntity() ,query);
				
				logger.info("Do Login : " +dentity);
				if (dentity !=null  ) {
			 	LoginDetailsEntity log = new LoginDetailsEntity();
			 	log.setLoginId(dentity.getLoginId());
			 	log.setPassword(dentity.getPassword());
				log.setLogin_datetime(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date()).toString());
			//	log.setLocationname(dentity.getLocation().getLocationName());
				logService.save(log);
				crearteSessionUser(log);	
				return new ResponseEntity<>(dentity,HttpStatus.OK);
			}
			
			else {
				logger.info(" #### Error in Login #### ");
				LoginFailureEntity logfail = new LoginFailureEntity();
				logfail.setLoginId(objUser.getLoginId());
				logfail.setPassword(objUser.getPassword());
				logfail.setDateTime(new SimpleDateFormat("dd-MM-yyy hh:mm:ss").format(new Date()).toString());
				logfailService.save(logfail);
				System.out.println("## Data Saved in LoginFailure Table ##");
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			 
		} else {
			
			logger.info(" #### System Error !! Application has Failed to be start.code ox804f05 #### ");
			return new ResponseEntity<>(HttpStatus.LOCKED);
		}

	}
	
	
	
	
		 @PostMapping(value="/user/logout")
		 public ResponseEntity<?> logOut(@RequestBody UserEntity objUser) throws Exception{
			
				logger.info("Do Logout : ");
				for(LoginDetailsEntity user:userLoginDetails) {
					if(user.getLoginId().equals(objUser.getLoginId()))
					{
						user.setLogout_datetime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()).toString());
						userLoginDetails.remove(user);
						logService.update(user);
						logger.info("----------- log session  delete --------------"+userLoginDetails.size());
						
						logger.info("####Succesfully logout####");
						break;
						
						
					}
				}
				return new ResponseEntity<>(HttpStatus.OK);					
		}
				
//		logger.info("LocationName :" +dentity.getLocation().getLocationName());
//		
//		logger.info("User LoinId :" +uentity.getLoginId());
//		logger.info("DB LoginId :" +dentity.getLoginId());
//	
//		logger.info("DB Password :" +dentity.getPassword());
//		logger.info("Entered Password " +uentity.getPassword());
//	
//		
//		
//		if (!dentity.getLoginId().equals(uentity.getLoginId())) {
//			logger.info("#########You Have Entered Wrong LoginId######### " +uentity.getLoginId());
//			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//		}
//		
//		else {
//		
//			if (!dentity.getPassword().equals(uentity.getPassword())) {
//				
//				logger.info("#########You Have Entered Wrong Password######### " +uentity.getPassword());
//				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//				
//			} else {
//			
//				logger.info("#########You Have Entered Right Password######### " +uentity.getPassword());
//				LoginDetailsEntity log = new LoginDetailsEntity();
//				log.setLoginid(dentity.getLoginId());
//				log.setPassword(dentity.getPassword());
//				log.setLogin_datetime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()).toString());
//				log.setLocationname(dentity.getLocation().getLocationName());
//				logService.save(log);
//				return new ResponseEntity<>(HttpStatus.OK);
//			}
//			
//		}
	
	
	
	 
	 @PostMapping(value="/password/reset")
	 public ResponseEntity<?> updatePassword(@RequestBody ChangePasswordEntity pentity) throws Exception{
		 
		 if (pentity != null) {
			 logger.info("####Password Reset Successfully For.########## " + pentity.getLoginId());
			 UserEntity userEntity = userService.find(new UserEntity(),"where loginId= '"+pentity.getLoginId()+"'");
			 logger.info("user "+userEntity);
			 
			 //userEntity.setPassword(pentity.getPassword());
			 userEntity.setPassword(AES.encrypt(pentity.getPassword()));
			 //userEntity.setPassword(PasswordEncription.generateHash(pentity.getPassword()));
			  userService.update(userEntity);
		
			 return new ResponseEntity<>(HttpStatus.OK);
		} else {
			logger.info("####Error in Password Resest####");			
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		 
		 
	 }
	 
	 
	 
	 @PostMapping(value="/password/change")
	 public ResponseEntity<?> changePassword(@RequestBody ChangePasswordEntity pentity){
		 
		boolean result=false;
	 logger.info("####Password Reset Successfully For.##########"+ pentity);
	 String oldPwd=PasswordEncription.generateHash(pentity.getOldpassword());
	 
	 UserEntity userEntity = userService.find(new UserEntity(),"where loginId= '"+pentity.getLoginId()+"'");
		 logger.info("####User####"+userEntity);
	 if(userEntity.getPassword().equals(oldPwd)) {
		 logger.info("####Password Match####");
	 
		// userEntity.setPassword(pentity.getPassword());
		 userEntity.setPassword(PasswordEncription.generateHash(pentity.getPassword()));
		  userService.update(userEntity);
		  result=true;
		  return new ResponseEntity<>(result,HttpStatus.OK); 
	 }
	 else {
		 logger.info("########Sorry Password Not Matched####");
		 
		 
		 result=false;
		 return new ResponseEntity<>(result,HttpStatus.NO_CONTENT);   
	 }
	
	 }
	 
	 
	 
	 
	 
	 //create session 
	 public List<UserEntity> crearteSessionUser(LoginDetailsEntity loginUser) {
		 
		 String query = "where loginid ='"+loginUser.getLoginId()+"' and login_datetime ='"+loginUser.getLogin_datetime()+"'";
			loginUser = logService.find(new LoginDetailsEntity() ,query);
			userLoginDetails.add(loginUser);
			
		 UserEntity userEntity = userService.find(new UserEntity(),"where loginId= '"+loginUser.getLoginId()+"'");
		 isUserLogin.add(userEntity);
		 logger.info("####SESSION CREATE####"+isUserLogin.get(0).getLoginId());
		return  isUserLogin;
	 }
	 
	 
	 //destroy session 
	 public  List<UserEntity> destroySessionUser(UserEntity objUser) {
				 
		 isUserLogin.remove(0);
		 logger.info(objUser.getLoginId()+"#### SESSION DESTROY####"+isUserLogin);
		return  isUserLogin;
	 }
	 
	 @RequestMapping(value="/loginstatus")
	 public ResponseEntity<?> getLoginStatus() {
		 logger.info("####Check Is User Login####"+isUserLogin);
		 try {
			 UserEntity userEntity=isUserLogin.get(0);
			 return new ResponseEntity<>(userEntity,HttpStatus.OK);
		 }catch(Exception e) {
			 UserEntity userEntity=null;
			logger.info("####Error in Login Status####" +e.getMessage());
			 return new ResponseEntity<>( userEntity,HttpStatus.BAD_REQUEST);
		 }
		 
		 }
		
	
	 
	 @GetMapping(value="/all/loginDetails")
	 public ResponseEntity<?> getLoginDetails(){
		 
		 try {
			 List<LoginDetailsEntity> logList = logService.fetch(new LoginDetailsEntity());
			 
			 if (logList.isEmpty()) {
				 logger.info("####Error in Found Login Details####");
				 return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				int loginNumber = logList.size();
				logger.info("#########Total Visit " +logList.size()+ " User########");
				return new ResponseEntity<>(loginNumber,HttpStatus.OK);
			}
			
		 } catch (Exception e) {
			// TODO: handle exception
			 System.out.println(e.getMessage());
			 return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
		}
		 
	 }
	 
	 
	@SuppressWarnings("unlikely-arg-type")
	@PostMapping(value="/forget/password")
	 public ResponseEntity<?> forgetPassword(@RequestBody UserEntity objUserEntity) throws Exception{
		 
			 logger.info("\n ------------------------------------------\n  LOGIN ID : "+objUserEntity.getLoginId()+" \n MOBILE NO.: "+objUserEntity.getMobile()+" \n ------------------------------------------\n");
			 
			
			 String query="Where loginId= '"+objUserEntity.getLoginId()+"'";
			
			 UserEntity user = userService.find(new UserEntity(), query);
			
			 if (user!=null) {
				
					String mobile = user.getMobile();
					if(objUserEntity.getMobile().equals(mobile)) {
						String decodePassword =AES.decrypt(user.getPassword());
					
						ismsService.send(mobile, decodePassword);
						logger.info("\n ------------------------------------------\n YOUR PASSWORD SEND TO YOUR REGISTERED MOBILE NUMBER  "+mobile+" \n ------------------------------------------\n");
					}else {
						logger.info("\n ------------------------------------------\n MOBILE NUMBER NOT MATCHED  \n ------------------------------------------\n");
						return new ResponseEntity<>(HttpStatus.NOT_FOUND);
					}
								  
				return new ResponseEntity<>(HttpStatus.OK);
			} else {
				logger.info("\n ------------------------------------------\n LOGINID NOT FOUND  \n ------------------------------------------\n");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}

		 
	 }
	
	@GetMapping(value="/login/failure/{loginId}")
	public ResponseEntity<?> loginFailure(@PathVariable ("loginId") String loginId){
		
		String query = "Where loginId='"+loginId+"'";
		List<LoginFailureEntity> logfailList = logfailService.fetch(new LoginFailureEntity(),query);
		int totalfail = logfailList.size();
		System.out.println("aaaaaaaaaaaaaaaaaa " + totalfail);
		return new ResponseEntity<>(totalfail,HttpStatus.OK);
	}
	
	@GetMapping(value="/login/success/{loginId}")
	public ResponseEntity<?> loginSuccess(@PathVariable ("loginId") String loginId){
		
		String query = "Where loginId='"+loginId+"'";
		List<LoginDetailsEntity> logsuccessList = logService.fetch(new LoginDetailsEntity(),query);
		int totalsuccess = logsuccessList.size();
		System.out.println("qqqqqqqqqqqqqqqq " + totalsuccess);
		return new ResponseEntity<>(totalsuccess,HttpStatus.OK);
	}
	
	@GetMapping(value="last/login/success/{loginId}")
	public ResponseEntity<?> lastloginSuccess(@PathVariable ("loginId") String loginId){
		
		
		Object ddd = em.createQuery("SELECT MAX(lde.id) FROM LoginDetailsEntity lde WHERE lde.loginId='"+loginId+"'").getSingleResult();
		//String query = "Where loginId='"+loginId+"'";
		System.out.println("llllllllllll " + ddd);
		LoginDetailsEntity ldd = logService.find(new LoginDetailsEntity(), "where id='"+ddd+"'");
		System.out.println("ddddddddddddddddd " + ldd);
		return new ResponseEntity<>(ldd,HttpStatus.OK);
	}
	
	
	@GetMapping(value="last/login/fail/{loginId}")
	public ResponseEntity<?> lastloginFail(@PathVariable ("loginId") String loginId){
		
		
		Object ddd = em.createQuery("SELECT MAX(lfe.id) FROM LoginFailureEntity lfe WHERE lfe.loginId='"+loginId+"'").getSingleResult();
		//String query = "Where loginId='"+loginId+"'";
		System.out.println("llllllllllll " + ddd);
		LoginFailureEntity ldd = logfailService.find(new LoginFailureEntity(), "where id='"+ddd+"'");
		System.out.println("ddddddddddddddddd " + ldd);
		return new ResponseEntity<>(ldd,HttpStatus.OK);
	}
}
