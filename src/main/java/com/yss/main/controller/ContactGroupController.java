package com.yss.main.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.ContactGroup;
import com.yss.main.entity.GroupWithContact;
import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;
import com.yss.main.sms.service.DataProvider;

@RestController
@CrossOrigin
@RequestMapping("/contactgroup")
public class ContactGroupController {

	private static final Logger logger = LoggerFactory.getLogger(ContactGroupController.class);
	
	@PersistenceContext
	private EntityManager entityManager; 
	
	@Autowired
	private IGenericService<ContactGroup> contactGroupService;
	@Autowired
	private IGenericService<GroupWithContact> contactWithGroupService;
	
	
	
	@Autowired 
	private IGenericService<UserEntity> userService;
	
	
	@RequestMapping(value="/grouplist")
	public  ResponseEntity<List<ContactGroup>> getContactGroupData(@RequestParam String loginId) {

		String query="";
		UserEntity user=userService.find(new UserEntity(),"where loginId ='"+loginId+"'");
		 query=DataProvider.getQueryCondition(user, userService);
//		if(user.getUserType().equals("superadmin")){
//			query="Where flag=false";
//		}else if(user.getUserType().equals("admin")){
//			List<UserEntity> userList=userService.fetch(new UserEntity(),"where createdBy ='"+loginId+"'");
//			query="WHERE loginId ='"+loginId+"'";
//			int i=1;
//			for(UserEntity user1:userList){
//				System.out.println(userList.size()+"  i "+i);				
//				
////				if(userList.size()==i){
////					query=query+"loginId = '"+user1.getLoginId()+"' and flag  = false";
////				}else{
//					query=query+" OR loginId = '"+user1.getLoginId()+"'";
//			//	}
//				i++;
//			}
//			query=query+" and flag = false";
//			
//		}else if(user.getUserType().equals("user")){
//			query="WHERE loginId='"+loginId+"' and flag  = false";
//		}
//		
		System.out.println("query  "+query);
		 List<ContactGroup> groupList=contactGroupService.fetch(new ContactGroup(),query);
		 if (groupList != null) {
			 logger.info("####Group List Found####  "+groupList.size());
			 return new ResponseEntity<List<ContactGroup>>(groupList,HttpStatus.OK);
		} else {
			logger.info("####Group List Not Found####");
			 return new ResponseEntity<List<ContactGroup>>(groupList,HttpStatus.NOT_FOUND);
		}
		
	}
	
	@PostMapping(value="/createGroup")
	public ResponseEntity<?> saveGroup(@RequestBody ContactGroup objContactGroup) {
		
		if (objContactGroup != null) {
			contactGroupService.save(objContactGroup);
			logger.info("####Group saved successfully####"+objContactGroup);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} else {	
			logger.info("####Group Not Saved####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}
	
	@PostMapping(value="/findContactGroup/name")
	public ResponseEntity<?> findByContactGroupName(@RequestBody ContactGroup objContactGroup) {
		
		if (objContactGroup != null) {
			ContactGroup ContactGroup=contactGroupService.find(objContactGroup,"where contactGroupName = '" +objContactGroup.getContactGroupName()+"'");
			if (ContactGroup != null) {
				logger.info("####Group Name Find####" +objContactGroup.getContact_groupId());
				return new ResponseEntity<>(ContactGroup,HttpStatus.CREATED);
			} else {
				logger.info("####Group Name Not Find####" +objContactGroup.getContact_groupId());
				return new ResponseEntity<>(ContactGroup,HttpStatus.NOT_FOUND);
			}
			
		} else {
			
			logger.info("####Sorry ContactGroup Not Found####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
	}
	
	
	
	@RequestMapping(value="/contactGroup/{contact_groupId}" , method=RequestMethod.DELETE)
	public ResponseEntity<?> deleteContactGroup(@PathVariable ("contact_groupId") long contact_groupId){
		
		ContactGroup contactGroup = contactGroupService.find(new ContactGroup(), contact_groupId);
		
		if (contactGroup == null) {
			logger.info("########ContactGroup with Id " +contactGroup + " Does Not Exist########");
			return new ResponseEntity<>(HttpStatus.FOUND);
		} else {
			contactGroup.setFlag(true);
			List<GroupWithContact> contacatList=contactWithGroupService.fetch(new GroupWithContact(),"where contactGroup='"+contact_groupId+"'");
			
			for(GroupWithContact contact:contacatList) {
				contactWithGroupService.delete(contact);				
			}
			//contactWithGroupService.delete(new GroupWithContact(),"where contactGroup='"+contact_groupId+"'");
			contactGroupService.delete(contactGroup);
			logger.info("########ContactGroup with Id " +contactGroup + " Successfully Deleted########");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
	}

	
	@PutMapping(value="/updateGroup")
	public ResponseEntity<?> updatecontactGroup(@RequestBody ContactGroup objcontactGroup){
		
		if (objcontactGroup != null) {
			contactGroupService.update(objcontactGroup);
			logger.info("####Contact Group Update Successfully####");
			return new ResponseEntity<>(HttpStatus.CREATED);
		} else {
			logger.info("####Contact Group Not Update####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		}
		
		
	}
}
