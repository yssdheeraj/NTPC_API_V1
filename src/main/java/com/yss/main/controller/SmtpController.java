package com.yss.main.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.SmtpMailConfiguration;
import com.yss.main.generic.service.IGenericService;

@RestController
@CrossOrigin
@RequestMapping("/smtp")
public class SmtpController {

private static final Logger logger = LoggerFactory.getLogger(SmtpController.class);
	
	@Autowired
	private IGenericService<SmtpMailConfiguration> iDataGenericService;
	
	@RequestMapping(value="/allconfig")
	public ResponseEntity<List<SmtpMailConfiguration>> getTestData() {
		
		
		List<SmtpMailConfiguration> smtpMailConfiguration= iDataGenericService.fetch(new SmtpMailConfiguration());
		
		return new ResponseEntity<List<SmtpMailConfiguration>>(smtpMailConfiguration,HttpStatus.OK);
		
	}
	
	@PostMapping(value="/saveconfig")
	public ResponseEntity<?> saveConfiguration(@RequestBody SmtpMailConfiguration obj) {
		iDataGenericService.update(obj);
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping(value="/updateconfig")
	public ResponseEntity<?> updateTest1(@RequestBody SmtpMailConfiguration obj) {
		iDataGenericService.update(obj);
	logger.info("Data update successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	
	@RequestMapping(value="deleteconfig/{id}" , method=RequestMethod.DELETE)
	public ResponseEntity<?> deleteTest1(@PathVariable ("id") long id) {
		SmtpMailConfiguration obj = iDataGenericService.find(new SmtpMailConfiguration(),id);
		if (obj == null) {
			return new ResponseEntity<>(obj, HttpStatus.NO_CONTENT);
		}
		iDataGenericService.delete(obj);
			logger.info("Data deleted successfully !");
		return new ResponseEntity<>(HttpStatus.OK);
	}

	
}
