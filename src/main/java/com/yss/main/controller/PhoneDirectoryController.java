package com.yss.main.controller;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.GroupWithContact;
import com.yss.main.entity.PaginationDataProvider;
import com.yss.main.entity.PhoneDirectory;
import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;
import com.yss.main.repository.PhoneDirectoryRepository;
import com.yss.main.sms.service.DataProvider;
import com.yss.main.util.Constants;

@RestController
@CrossOrigin
@RequestMapping("/phonedirectory")
public class PhoneDirectoryController {
	
private static final Logger logger = LoggerFactory.getLogger(PhoneDirectoryController.class);
	
	@Value("${paginatin_size}")
	private int PAGESIZE;

	@Autowired
	private IGenericService<PhoneDirectory> phoneService;
	
	@Autowired
	private IGenericService<GroupWithContact> groupWithContactService;
	
	
	
	@Autowired
	private IGenericService<UserEntity> userService;
	
	
	@Autowired
	private PhoneDirectoryRepository phoneDirectoryRepository;
	
	
	HashSet<String> filter=new HashSet<String>();
	
	
	@RequestMapping(value="/phoneDirectorylist")
	public  ResponseEntity<List<PhoneDirectory>> getPhoneDirectoryData(@RequestParam(value = "loginId", required = true) String loginId) {
		logger.info("\n ---------------------------------\nlogin id  "+loginId+"\n -----------------------------------------");
		String query="";
		UserEntity user=userService.find(new UserEntity(),"where loginId ='"+loginId+"'");
		query=DataProvider.getQueryCondition(user);
		
		List<PhoneDirectory> phoneDirectoryList=phoneService.fetch(new PhoneDirectory(),query);
		logger.info("conditionquery  "+query);
		
		if(phoneDirectoryList ==null) {
			logger.info("####No Phone Directory Found####");
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		else {
			logger.info("####Found####" +phoneDirectoryList.size() + "Phone Directory####");
		return new  ResponseEntity(phoneDirectoryList,HttpStatus.OK);
		}
	}
	
//	@RequestMapping("/phoneDirectorylist")
//	public Page<PhoneDirectory> showAll(@RequestParam(defaultValue="0") int page){
//		logger.info("####Found Phone Directory####" +page);
//		return phoneDirectoryRepository.findAll(new PageRequest(page, PAGESIZE));
//		
//	}


	
	@PostMapping(value="/createPhoneDirectory")
	public ResponseEntity<?> savePhoneDirectory(@RequestBody PhoneDirectory objPhoneDirectory) {
		String contactId="";
		objPhoneDirectory.setPersonal(true);
		if (objPhoneDirectory != null) {
			logger.info("jjjjj   "+objPhoneDirectory);
						List<PhoneDirectory> phoneDir=phoneService.fetch(new PhoneDirectory(),"where personal= true order by contactId desc");
				logger.info(""+phoneDir);
					if(phoneDir.size()>0) {
					PhoneDirectory phone1=phoneDir.get(0);
					contactId=phone1.getContactId();
					System.out.println("contact id "+contactId);
					contactId=contactId.substring(2,contactId.length());
					System.out.println("contact id "+contactId);
					int i=Integer.parseInt(contactId);
					i++;
					contactId="pr"+i;
					
				}else {
					contactId="pr1";
				}
			//PhoneDirectory phone1=phoneDir.get(0);
	
	//	System.out.println("contact id "+phoneDir);
					logger.info(" new conatact id "+contactId);
			objPhoneDirectory.setContactId(contactId);
			phoneService.save(objPhoneDirectory);
			logger.info("####Phone Directory Create Successfully####");
			return new ResponseEntity<>(HttpStatus.CREATED);
		} else {
			logger.info("####Error In Create Phone Directory ####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}

	@PutMapping(value="/updatePhoneDirectory")
	public ResponseEntity<?> updatePhoneDirectory(@RequestBody PhoneDirectory objPhoneDirectory) {
		
		if (objPhoneDirectory != null) {
			phoneService.update(objPhoneDirectory);
			logger.info("####Phone Directory Update Successfully####" +objPhoneDirectory);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} else {	
			logger.info("####Phone Directory Not Update####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	
	}

	@PostMapping(value="/findPhoneDirectory/id")
	public ResponseEntity<?> findByPhoneDirectoryId(@RequestBody PhoneDirectory objPhoneDirectory) {
		
		PhoneDirectory PhoneDirectory=phoneService.find(objPhoneDirectory,objPhoneDirectory.getContactId());
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(PhoneDirectory,HttpStatus.CREATED);
	}
	
	@PostMapping(value="/findPhoneDirectory/name")
	public ResponseEntity<?> findByPhoneDirectoryame(@RequestBody PhoneDirectory objPhoneDirectory) {
		
		PhoneDirectory PhoneDirectory=phoneService.find(objPhoneDirectory,"where name = '" +objPhoneDirectory.getName()+"'");
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(PhoneDirectory,HttpStatus.CREATED);
	}

	//delete
	@RequestMapping(value="/phoneDirectory/{contactId}" , method=RequestMethod.DELETE)
	public ResponseEntity<?> deletePhoneDirectory(@PathVariable ("contactId") String contactId){
		
		PhoneDirectory phoneDirectory = phoneService.find(new PhoneDirectory(), "where contactId ='"+contactId+"'");
		
		if (phoneDirectory == null) {
			logger.info("########PhoneDirectory with Id " +phoneDirectory + " Does Not Exist########");
			return new ResponseEntity<>(HttpStatus.FOUND);
		} else {
			
			GroupWithContact groupWithContact=groupWithContactService.find(new GroupWithContact(),"where phoneDirectory ='"+phoneDirectory.getContactId()+"'");
			logger.info("-------"+groupWithContact);
			if(groupWithContact!=null) {
				groupWithContactService.delete(groupWithContact);
				logger.info("--------------------- \n PhoneDirectory  Successfully Deleted  from group \n -------------------");
			}
		
			
		phoneService.delete(phoneDirectory);
			logger.info("######## \n PhoneDirectory with Id " +phoneDirectory.getContactId() + "  \n Successfully Deleted########");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
	}
	
//	@GetMapping(value="/fetchdepartmentbyLocation/{locationName}")
//	public ResponseEntity<?> fetchDepartemntAccordingToLocation(@PathVariable("locationName") String locationName){
//		
//		String query = "where locationName ='"+locationName+"'";
//		List<PhoneDirectory> list = phoneService.fetch(new PhoneDirectory(), query);
//		logger.info("LOCATION LIST " + list.size() + " Found For " + locationName);
//		logger.info("LOCATION LIST " + list);
//		for (PhoneDirectory phoneDirectory : list) {
//			
//			boolean filterDepartment = filter.add(phoneDirectory.getDepartmentName());
//			if (filterDepartment) {
//				System.out.println(" FilterDepartment From Phone Directory According To Location " + phoneDirectory.getDepartmentName());
//				String departmentName =  phoneDirectory.getDepartmentName();
//				System.out.println("*************** " + departmentName);
//				String query1 = "where departmentName ='"+departmentName+"'";
//				List<PhoneDirectory> listPhone = phoneService.findAllByCondition(new PhoneDirectory(), query1);
//				System.out.println("***************######### " + listPhone);
//				System.out.println("***************######### " + listPhone.size());
//			}
//		}
//	
//		return new ResponseEntity<>(HttpStatus.OK);
//	}
	
	@RequestMapping(value="/mobilenumberexist")
	public ResponseEntity<PhoneDirectory> getPhoneDirectory(@RequestParam String mobile){
		logger.info("mobile "+mobile);
		PhoneDirectory phone=phoneService.find(new PhoneDirectory(),"where mobileNo1='"+mobile+"' and flag =false");
		System.out.println("ok..............................."+phone);
		if(phone!=null) {
			return new ResponseEntity<PhoneDirectory>(phone,HttpStatus.OK);
		}else {
			return new ResponseEntity<PhoneDirectory>(HttpStatus.NO_CONTENT);
		}
		
	}
	@RequestMapping(value="/empidexist")
	public ResponseEntity<PhoneDirectory> getPhoneDirectoryEmpId(@RequestParam String empId){
		logger.info("emp id "+empId);
		PhoneDirectory phone=phoneService.find(new PhoneDirectory(),"where empId='"+empId+"' and flag =false");
		System.out.println("ok..............................."+phone);
		if(phone!=null) {
			return new ResponseEntity<PhoneDirectory>(phone,HttpStatus.OK);
		}else {
			return new ResponseEntity<PhoneDirectory>(HttpStatus.NO_CONTENT);
		}
		
	}

	
	@RequestMapping("/otherlocation")
	public ResponseEntity<List<PhoneDirectory>> findPhoneDirectoryByLocation(@RequestParam(value = "locationName", required = false) String locationName,
			@RequestParam(value = "mobile", required = false) String mobile,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value="groupId",required=true) String groupId ){
		
		boolean isLocation=false;
		boolean isName=false;
		boolean isMobile=false;
		
		String query="";
		int result=0;
		logger.info("  "+locationName+"    "+name+"   "+mobile+"   "+groupId);
		List<PhoneDirectory> list=new ArrayList<PhoneDirectory>();
		List<PhoneDirectory> list_F=new ArrayList<PhoneDirectory>();
		
		
		if(locationName!=null && !locationName.equals("undefined") && !locationName.equals("")) {
			isLocation=true;
			//query=query+"locationName='"+locationName+"'";
		}
		if(mobile!=null && !mobile.equals("undefined") && !mobile.equals("")) {
			isMobile=true;
			//query=query+"mobileNo1='"+mobile+"'";
		}
		if(name!=null && !name.equals("undefined") && !name.equals("")) {
			isName=true;
			//query=query+"Name='"+name+"'";
		}
		
		
		if(isLocation && isMobile && isName) {
			query="where locationName='"+locationName+"' and mobileNo1 LIKE'"+mobile+"%' and Name Like '"+name+"%'";
		}else if(isLocation && isMobile) {
			query="where locationName='"+locationName+"' and mobileNo1 LIKE '"+mobile+"%'";
			
		}else if(isLocation && isName) {
			query="where locationName='"+locationName+"' and Name Like '"+name+"%'";
		}else if(isMobile && isName) {
			query="where mobileNo1 Like '"+mobile+"%' and Name Like '"+name+"%'";
		}else if(isLocation) {
			query="where locationName='"+locationName+"'";
		}else if(isMobile) {
			//query="where mobileNo1='"+mobile+"'";
			query="WHERE mobileNo1 LIKE '"+mobile+"%'  ";
		}else if(isName) {
		//	query="where  Name='"+name+"'";
			query="WHERE Name LIKE '"+name+"%'  ";
		}else {
			logger.info(" not valid data here");
		}
		
		logger.info("query ******** "+query);
		if(query.equals("")) {
			result=0;
			
		}else {
			result=1;
			list=phoneService.fetch(new PhoneDirectory(),query);
		}
		logger.info(" top query "+query);
//		if(locationName!=null) {
//			if(mobile!=null && !mobile.equals("undefined") && !mobile.equals(""))
//			 {
//				query="where locationName='"+locationName+"' and mobileNo1='"+mobile+"'";
//				logger.info("  "+query);
//				result=1;
//			}else if(name!=null && !name.equals("undefined") && !name.equals("")) {
//				
//				query="where locationName='"+locationName+"' and Name='"+name+"'";
//				logger.info("  "+query);
//				result=1;
//			}else if(name!=null && mobile!=null) {
//				result=1;
//				query="where locationName='"+locationName+"' and mobileNo1='"+mobile+"' and Name='"+name+"'";
//				logger.info("  "+query);
//			}else {
//				result=0;
//				
//			}
//			list=phoneService.fetch(new PhoneDirectory(),query);
//			
//		}else {
//			result=0;
//		}
		
		
		logger.info("direcotyry............."+list.size());
		
		for(int i=0;i<list.size();i++) {
			String Query="where contactGroup='"+groupId+"' and phoneDirectory='"+list.get(i).getContactId()+"'";
		logger.info("query "+Query);
		GroupWithContact groupWithContact=groupWithContactService.find(new GroupWithContact(),Query);
			logger.info("contact group "+groupWithContact);
		if(groupWithContact==null) {
			list_F.add(list.get(i));
			logger.info(i+"  phone   "+list.size());
			}
			logger.info("int "+i);
		}
		
		
		if(result!=0) {
			logger.info(" if "+list_F);
			return new ResponseEntity<List<PhoneDirectory>>(list_F,HttpStatus.OK);
		}else {
			logger.info(" else "+list_F);
			return new ResponseEntity<List<PhoneDirectory>>(list_F,HttpStatus.BAD_REQUEST);
		}
		
	}
	
	
	@RequestMapping("/findbynameornumber/hh")
	public ResponseEntity<List<PhoneDirectory>> findPhoneDirectory(@RequestParam(value = "loginId", required = true) String loginId,
			@RequestParam(value = "value", required = false) String value,
			@RequestParam(value = "field", required = false) String field){
		String query="";
		logger.info("working......."+loginId+"****************************......"+value+"......."+field +"......................");
		
	//	UserEntity user=userService.find(new UserEntity(),"where loginId ='"+loginId+"'");
	//	query=DataProvider.getQueryCondition(user);
		
		query="where "+field+" like  '"+value+"%'";
		
		
		logger.info("query jjjjjj"+query);
		
		List<PhoneDirectory> list=phoneService.fetch(new PhoneDirectory(),query);
		logger.info("------*****************************************\n---------------------\n -------------------------\n-------------------finding data list "+list);
		if(list!=null) {
			return new ResponseEntity<List<PhoneDirectory>>(list,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<List<PhoneDirectory>>(HttpStatus.NOT_FOUND);
		}
		
//		boolean isName=false;
//		boolean isMobile=false;
//		
//		if(mobile!=null && !mobile.equals("undefined") && !mobile.equals("")) {
//			isMobile=true;
//			//query=query+"mobileNo1='"+mobile+"'";
//		}
//		if(name!=null && !name.equals("undefined") && !name.equals("")) {
//			isName=true;
//			//query=query+"Name='"+name+"'";
//		}
//		if((isName==true) && (isMobile==true)) {
//			query=query+" and mobileNo1 Like '"+mobile+"%' and Name Like '"+name+"%'";
//		}else if(isName==true) {
//			query=query+" and Name LIKE '"+name+"%' ";
//		}else if(isMobile==true) {
//			query=query+" and mobileNo1 LIKE '"+mobile+"%' ";
//		}
//		logger.info("new query "+query);
		
				
	}
	
	
	
//no use this function	

	@RequestMapping("/findbynameornumber")
	public ResponseEntity<PaginationDataProvider> findPhoneDirectory1(@RequestParam(value = "loginId", required = true) String loginId,
			@RequestParam(value = "value", required = false) String value,
			@RequestParam(value = "field", required = false) String field){
		String query="";
		logger.info("working......."+loginId+"****************************......"+value+"......."+field +"......................");
		
	//	UserEntity user=userService.find(new UserEntity(),"where loginId ='"+loginId+"'");
	//	query=DataProvider.getQueryCondition(user);
		
		query="where "+field+" like  '"+value+"%'";
		
		
		logger.info("query jjjjjj"+query);
		int pageNumber=1;
		int size=20;
		List<PhoneDirectory> phoneList=phoneService.fetch(new PhoneDirectory(),pageNumber, size,query);
		//System.out.println(pageNumber+"  "+size);
		PaginationDataProvider paginationDataProvider=new PaginationDataProvider();
		System.out.println("List value "+phoneList);
		paginationDataProvider.setNumberOfData(phoneList);
		paginationDataProvider.setPageNumber(pageNumber);
		paginationDataProvider.setPageSize(size);
		paginationDataProvider.setTotalNumberOfPage(Constants.TOTAL_NUMBER_OF_PAGE);
		paginationDataProvider.setTotalNumberOfData(Constants.TOTAL_NUMBER_OF_DATA);
		logger.info("------*****************************************\n---------------------\n -------------------------\n-------------------finding data list "+paginationDataProvider);
		if(paginationDataProvider!=null) {
			return new ResponseEntity<PaginationDataProvider>(paginationDataProvider,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<PaginationDataProvider>(HttpStatus.NOT_FOUND);
		}
		
//		boolean isName=false;
//		boolean isMobile=false;
//		
//		if(mobile!=null && !mobile.equals("undefined") && !mobile.equals("")) {
//			isMobile=true;
//			//query=query+"mobileNo1='"+mobile+"'";
//		}
//		if(name!=null && !name.equals("undefined") && !name.equals("")) {
//			isName=true;
//			//query=query+"Name='"+name+"'";
//		}
//		if((isName==true) && (isMobile==true)) {
//			query=query+" and mobileNo1 Like '"+mobile+"%' and Name Like '"+name+"%'";
//		}else if(isName==true) {
//			query=query+" and Name LIKE '"+name+"%' ";
//		}else if(isMobile==true) {
//			query=query+" and mobileNo1 LIKE '"+mobile+"%' ";
//		}
//		logger.info("new query "+query);
		
				
	}
	
	
	
	@GetMapping(value="/location/{project}")
	public ResponseEntity<List<PhoneDirectory>> getLocationList(@PathVariable("project") String project){
		logger.info("project "+project);
		
		String query=createQuery("project",project);
		logger.info("quer y  "+query);
		List<PhoneDirectory> locationList=phoneService.fetchFields("DISTINCT locationName ", new PhoneDirectory(),query);
		
		return new ResponseEntity<List<PhoneDirectory>>(locationList,HttpStatus.OK);
	}
	
	
	@GetMapping(value="/grade/{project}/{locationName}")
	public ResponseEntity<List<PhoneDirectory>> getDepartmentList(@PathVariable(value = "project") String project,@PathVariable(value = "locationName") String locationName){
		String query="";
		logger.info("project "+project);
		if(project.equals("NULL")) {
			query=createQuery("locationName",locationName);
			logger.info("Query "+query);
		}else {
		
		logger.info("locationName "+locationName);
			query=createQuery("project",project,"locationName",locationName);
		logger.info("Query "+query);
		}
	List<PhoneDirectory> gradeList=phoneService.fetchFields("DISTINCT grade", new PhoneDirectory(),query);
		
		return new ResponseEntity<List<PhoneDirectory>>(gradeList,HttpStatus.OK);
	}
	
	
	public String createQuery(String field,String value) {
		String query="where ";
		String[] names=value.split(",");
		for(int i=0;i<names.length;i++) {
			if(i==0) {
				query=query+" "+field+" = '"+names[i]+"'";
			}else {
				query=query+"  OR  "+field+" = '"+names[i]+"'";
			}
		}
		return query;
	}
	
	public String createQuery(String field1,String projects,String field2, String locations) {
		String query="where ";
		
		String[] project=projects.split(",");
		String[] location=locations.split(",");
		int count=0;
		for(int i=0;i<project.length;i++) {
			
			for(int j=0;j<location.length;j++) {
				
				if(count==0) {
					query=query+" "+field1+" = '"+project[i]+"' AND "+field2+" = '"+location[j]+"'";
					count++;
				}else {
					query=query+"  OR  "+field1+" = '"+project[i]+"' AND "+field2+" = '"+location[j]+"'";
				}
			}
			
		}
		return query;
	}

	
	
 	}
