package com.yss.main.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.InstanceMessageDetails;
import com.yss.main.entity.InstanceMsgEntity;
import com.yss.main.entity.PhoneDirectory;
import com.yss.main.generic.service.IGenericService;
import com.yss.main.sms.service.InstanceMessageService;

@RestController
@CrossOrigin
@RequestMapping("/intancemessage")
public class InstanceMsgController {

	private static final Logger logger = LoggerFactory.getLogger(InstanceMsgController.class);
	@Autowired
	private IGenericService<InstanceMessageDetails> iDataGenericService;
	@Autowired
	private IGenericService<PhoneDirectory> phoneService;

	@Autowired
	private InstanceMessageService instanceMessageService;
	
 String [] names= {"locationName","project","grade","substgrade","departmentName"};
	
	
	@RequestMapping("/instant/value")
	public ResponseEntity<InstanceMsgEntity> getInstantValue(){
		
		
		ArrayList<String> nameList=new ArrayList<String>();
		InstanceMsgEntity instance=new InstanceMsgEntity();
		//locationlist
//		List<PhoneDirectory> phoneList=phoneService.fetchFields("distinct grade ",new PhoneDirectory());
//		logger.info("   "+phoneList);
		for(String name:names) {
			List<PhoneDirectory> phoneList=phoneService.fetchFields("distinct "+name+" ",new PhoneDirectory());
			 nameList=getValueInStringArray(phoneList);
			 if(name.equals("locationName")) { instance.setLocationName(nameList); }
			 if(name.equals("project")) { instance.setProjectName(nameList); }
			 if(name.equals("grade")) { instance.setGrade(nameList); }
			 if(name.equals("substgrade")) { instance.setSubstGradeName(nameList); }
			 if(name.equals("departmentName")) { instance.setDepartmentName(nameList); }
			
		}
		
		
		
		
		return new ResponseEntity<InstanceMsgEntity>(instance,HttpStatus.OK);
	}
	
	
	public ArrayList<String> getValueInStringArray(List<PhoneDirectory> phoneList){
		
		ArrayList<String> nameList=new ArrayList<String>();
		for(int i=0;i<phoneList.size();i++) {
			
			if(phoneList.get(i)!=null) {
				nameList.add(phoneList.get(i)+"");
			}
			
		//	logger.info("------------------------------------------------------------ \n  "+nameList);
		}
		
		logger.info("-----------------------------"+nameList+"------------------------------ \n  ");
		return nameList;
	}
	
	
	
	
	@RequestMapping(value="/instancemsglist")
	public  ResponseEntity<List<InstanceMessageDetails>> getInstanceMsgData() {

		List<InstanceMessageDetails> InstanceMsgList=iDataGenericService.fetch(new InstanceMessageDetails(),"WHERE flag  = false");
		if (InstanceMsgList != null) {
			logger.info("####Found Instance Message List####");
			return new ResponseEntity<List<InstanceMessageDetails>>(InstanceMsgList,HttpStatus.OK);
		} else {
			logger.info("####Error in Found Instance Message List####");
			return new ResponseEntity<List<InstanceMessageDetails>>(InstanceMsgList,HttpStatus.NOT_FOUND);
		}
		
	}
	
	
	
	
	@PostMapping(value="/savemessageforsms")
	public ResponseEntity<?> saveInstacesMesssage(@RequestBody  InstanceMsgEntity instanceMsgEntity){
		
		if (instanceMsgEntity != null) {
			
			logger.info("####Save Message For SMS####"+instanceMsgEntity);
			//instanceMessageService.saveInstaceMessage(instanceMsgEntity);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			logger.info("####Error in Save Message For SMS####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
		//RunBatchFile();
		
	}
	
	
//	@PostMapping("/saveschedulemessage")
//	public ResponseEntity<?> saveschedulemessage(@RequestBody  InstanceMsgEntity instanceMsgEntity){
//		
//		logger.info("groups........................."+instanceMsgEntity);
//		instanceMessageService.saveScheduleMessage(instanceMsgEntity);
//		return new ResponseEntity<>(HttpStatus.OK);
//	}

//	@PostMapping("/createinstancemsg")
//	public ResponseEntity<?> saveInstanceMsg(@RequestBody InstanceMsgEntity objInstanceMsg) {
//	
//	iDataGenericService.save(objInstanceMsg);
//		logger.info("Data saved successfully !"+objInstanceMsg);
//		return new ResponseEntity<>(HttpStatus.CREATED);
//	}
//
//	@PutMapping("/updateinstancemsg")
//	public ResponseEntity<?> updateInstanceMsg(@RequestBody InstanceMsgEntity objInstanceMsg) {
//		iDataGenericService.update(objInstanceMsg);
//		logger.info("Data update successfully !");
//		return new ResponseEntity<>(HttpStatus.CREATED);
//	}
//
//	@DeleteMapping("/deleteinstancemsg")
//	public ResponseEntity<?> deleteInstanceMsg(@RequestParam(value = "InstanceMsgId", required = true) long InstanceMsgId) {
//		
//		logger.info("InstanceMsg id "+InstanceMsgId);
//		InstanceMsgEntity objInstanceMsg = iDataGenericService.find(new InstanceMsgEntity(), "where InstanceMsgId = " +  Long.valueOf(InstanceMsgId));
//		if (objInstanceMsg == null) {
//			return new ResponseEntity<>(objInstanceMsg, HttpStatus.NO_CONTENT);
//		}
//		
//		iDataGenericService.delete(objInstanceMsg);
//		logger.info("Data deleted successfully !");
//		return new ResponseEntity<>(HttpStatus.GONE);
//	}

//	@PostMapping("/findinstancemsg/id")
//	public ResponseEntity<?> findByInstanceMsgId(@RequestBody InstanceMsgEntity objInstanceMsg) {
//
//		InstanceMsgEntity InstanceMsg = iDataGenericService.find(objInstanceMsg, objInstanceMsg.getId());
//		logger.info("Data saved successfully !");
//		return new ResponseEntity<>(InstanceMsg, HttpStatus.CREATED);
//	}

	
	public String RunBatchFile() {
		
		logger.info("Message Sending Start");
		Runtime runtime = Runtime.getRuntime();
		try {
		    Process p1 = runtime.exec("cmd /c start D:\\NTPC_ALERT_SERVICES\\ntpc_api\\sms.bat");
		    InputStream is = p1.getInputStream();
		    int i = 0;
		    while( (i = is.read() ) != -1) {
		        System.out.print((char)i);
		    }
		} catch(IOException ioException) {
		    System.out.println(ioException.getMessage());
		}
	
		return "Success";
	}
	
}
