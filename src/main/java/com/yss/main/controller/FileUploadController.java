package com.yss.main.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.yss.main.entity.DepartmentEntity;
import com.yss.main.entity.FileUploadEntity;
import com.yss.main.entity.PhoneDirectory;
import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;

@RestController
@CrossOrigin
@RequestMapping("/fileupload")
public class FileUploadController {

	
	@Value("${filepath.uploadpath}")
	//private String UPLOADED_FOLDER ="D://NTPC_ALERT_SERVICES//";
	private String UPLOADED_FOLDER;
	

	
	String prefix = "";
	String LOGIN_ID="";
	BufferedWriter writer=null;
	
	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);
	@Autowired
	private IGenericService<FileUploadEntity> iDataGenericService;
	
	@Autowired
	private IGenericService<PhoneDirectory> iPhoneDirectoryService;
	
	@Autowired
	private IGenericService<UserEntity> iUserEntityService;
	
	@Autowired
	private IGenericService<DepartmentEntity> iDepartmentService;
	
	HashSet<String> filter=new HashSet<String>();
	 private int RIGHT_NUMBER_COUNT=0;
	 private int WROUNG_NUMBER_COUNT=0;
	 private int DUPLICATE_NUMBE_COUNT=0;
	 private String RIGHT_NUMBER_DATA="";
	 private String WROUND_NUMBER_DATA="";
	 private String DUPLICATE_NUMBER_DATA="";
	private UserEntity user;
	@RequestMapping(value="/FileUploadlist")
	public  ResponseEntity<List<FileUploadEntity>> getFileUploadData() {

		logger.info("####File Upload List####");
		 List<FileUploadEntity> FileUploadList=iDataGenericService.fetch(new FileUploadEntity(),"WHERE flag  = false");
		return new ResponseEntity<List<FileUploadEntity>>(FileUploadList,HttpStatus.OK);
	}
	
	
	@GetMapping()
	public ResponseEntity<?> fetchdata(UserEntity objUserEntity){
		
	
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping(value="upload/textFileDocument")
	public ResponseEntity<?> uploadtextFileDocument(@RequestParam(value="loginId") String loginId ,
			@RequestParam(value="messageType") String messageType ,
			@RequestParam(value="uploadTextDocument" ,required=false) MultipartFile uploadTextDocument){
		
				logger.info("####Upload Mobile Text Document For ####: " +loginId);
				uploadMobileDocument(uploadTextDocument,loginId);
				return new ResponseEntity<>(HttpStatus.OK);
			
	}
	
	@PostMapping(value="/createFileUpload")
	public ResponseEntity<?> saveFileUpload(@RequestParam(value = "txtFile", required = false) MultipartFile txtFile){
		
//		if(!txtFile.isEmpty()) {
//			String ext=txtFile.getOriginalFilename();
//			String ext2=txtFile.getContentType();
//			String ext3=txtFile.getContentType();
//			File f=new File(ext);
//			//FileUploadService.Read(ext);
//			logger.info(f.getName());
//			logger.info("file save"+ext);
//			try {
//				uploadDocuments(txtFile, "contact");
//			}catch(Exception e) {
//				
//				logger.info("success file uploate"+txtFile);
//			}
//			
//			
//			
//		}
	
		
		logger.info("####Successfully File Upload####"+txtFile);
		//iDataGenericService.save(objFileUpload);
		//logger.info("Data saved successfully !"+objFileUpload);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping(value="/updateFileUpload")
	public ResponseEntity<?> updateFileUpload(@RequestBody FileUploadEntity objFileUpload) {
		
		if (objFileUpload != null) {
			iDataGenericService.update(objFileUpload);
			logger.info("####File Update Successfully####");
			return new ResponseEntity<>(HttpStatus.CREATED);
		} else {	
			logger.info("####File Not Update####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}

	@DeleteMapping(value="/deleteFileUpload")
	public ResponseEntity<?> deleteFileUpload(@RequestParam(value = "FileUploadId", required = true) long FileUploadId) {
		
		logger.info("####FileUpload id ####"+FileUploadId);
		FileUploadEntity objFileUpload = iDataGenericService.find(new FileUploadEntity(), "where FileUploadId = " +  Long.valueOf(FileUploadId));
		if (objFileUpload == null) {
			logger.info("####File Not Found For Delete####");
			return new ResponseEntity<>(objFileUpload, HttpStatus.NO_CONTENT);
		}
		
		objFileUpload.setFlag(true);
		iDataGenericService.update(objFileUpload);
		logger.info("####File deleted successfully####");
		return new ResponseEntity<>(HttpStatus.GONE);
	}

	@PostMapping(value="/findFileUpload/id")
	public ResponseEntity<?> findByFileUploadId(@RequestBody FileUploadEntity objFileUpload) {

		if (objFileUpload != null) {
			FileUploadEntity FileUpload = iDataGenericService.find(objFileUpload, objFileUpload.getId());
			logger.info("####Find Upload File####");
			return new ResponseEntity<>(FileUpload, HttpStatus.CREATED);
		} else {
			logger.info("####Upload File Not Found####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}	
		

	}

//	@PostMapping("/findFileUpload/name")
//	public ResponseEntity<?> findByFileUploadame(@RequestBody FileUploadEntity objFileUpload) {
//
//		FileUploadEntity FileUpload = iDataGenericService.find(objFileUpload, objFileUpload.getFileName());
//
//		logger.info("Data saved successfully !");
//		return new ResponseEntity<>(FileUpload, HttpStatus.CREATED);
//	}
	
	
	/****************************************************************************************************************************/
	public void uploadMobileDocument(MultipartFile uploadTextDocument , String loginId) {
	
		FileUploadEntity objfileUpload = new FileUploadEntity();
		
		try {
		
			String MOBILE_DOCUMENT = uploadDocuments(loginId,  uploadTextDocument);
			objfileUpload.setUploadTextDocument(MOBILE_DOCUMENT !=null ? MOBILE_DOCUMENT : null);
			objfileUpload.setLoginId(loginId);
			iDataGenericService.update(objfileUpload);
			
			
		} catch (Exception e) {
			// TODO: handle exception
			ReportOfUploadFile(RIGHT_NUMBER_DATA,WROUND_NUMBER_DATA,DUPLICATE_NUMBER_DATA);
			System.out.println("e  "+e);
			logger.info("######## Error in File Upload ########" + e.getMessage());
		}
	}
	
	
	
	private String uploadDocuments(String loginId ,MultipartFile multipartFile) throws Exception {
		
		String currentDate="\n-----------------------------------------------------\n "+new Date()+"  \n-----------------------------------------------------\n ";
		RIGHT_NUMBER_DATA=currentDate;
		WROUND_NUMBER_DATA=currentDate;
		DUPLICATE_NUMBER_DATA=currentDate;
		 RIGHT_NUMBER_COUNT=0;
		 WROUNG_NUMBER_COUNT=0;
		 DUPLICATE_NUMBE_COUNT=0;

		String folder_name = "";
		String fileName = "";
		String fileNameO="";
		String newDocName = "";
		String newPath = "";
		try {
			if (!multipartFile.isEmpty()) {
				File doc = new File(UPLOADED_FOLDER  + "/");
				
				if (!doc.exists()) {
					logger.info(" Dir " + loginId + " NOT Exists");
					doc.mkdirs();
				}
				

				String s = multipartFile.getOriginalFilename();
				String[] words = s.split("\\s+");
				System.out.println("word "+words);
				for (int i = 0; i < words.length; i++) {
					fileName = fileName + words[i].replaceAll(" ", " ");
				
					/*mobile.txt*/
					logger.info("-----Trim file name----- " + fileName);
					
				}
				
				folder_name = UPLOADED_FOLDER  + loginId + "/" + fileName;
				
				/* D:/NTPC_ALERT_SERVICES/219/mobile.txt */
				
				logger.info("----- Orignal folder_name :----- " + folder_name);
			
				File file = new File(fileName);
				File filePath = file.getAbsoluteFile();
				logger.info("------ Get Real Path Where Doc from Hit.. -------- " + filePath); 
				
//				if (fileNamePrefix != null) {
//					newDocName = fileNamePrefix + fileName;
//					logger.info("New Document nmae " + newDocName);
//				} else {
//					newDocName = fileName;
//					logger.info("New Document nmae " + newDocName);
//
//				}
				
				newDocName = fileName;
				logger.info("-----New Document name ---- " + newDocName);
				
				/*  D:/NTPC_ALERT_SERVICES/219/mobile.txt  */
				
				newPath = UPLOADED_FOLDER  + loginId + "/" + newDocName;
				logger.info("-----rename file -----: " + newPath);
				File newFile = new File(newPath);
				logger.info("New File ----- " + newFile);

				if (!file.isDirectory()) {
					file.renameTo(newFile);
				}
				
				  try {
					// Get the file and save it somewhere
					byte[] bytes = multipartFile.getBytes();
					logger.info("----Bytes----" + bytes);
					
					/* D:\NTPC_ALERT_SERVICES\219\mobile.txt */
					
					//Path path = Paths.get(newPath);
					
					Path path = Paths.get(UPLOADED_FOLDER  + loginId + "_" +fileName);
					logger.info("-----path -----" + path);
				
					 Files.write(path, bytes);
					 
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.err.println(e);
				}
				
				
				//newDocName=path.toString();
				//logger.info("newDocName After To String -------" + newDocName );
			}
		} catch (Exception e) {
			logger.info("----- IOException ----- : " + e.getMessage());
		}
		
		
		//new FileUploadService().Read();
//		new FileUploadService().Read(fileName);
		logger.info("----- ----- "+ fileName);
		boolean txtfile=fileName.endsWith(".txt");
		boolean xlsfile=fileName.endsWith(".xls");
		boolean xlsxfile=fileName.endsWith(".xlsx");
		//logger.info(fileName.endsWith(".txt"));
		//logger.info(fileName.endsWith(".xls"));
		//logger.info(fileName.endsWith(".xlsx"));
		if(txtfile) {
			readFileAndStoreInDataBase(UPLOADED_FOLDER  + loginId + "_" +fileName,loginId);
		}else if(xlsfile || xlsxfile){
			readExcelFile(UPLOADED_FOLDER  + loginId + "_" +fileName,loginId);
		}
		
		return fileName;
	}
	
	public void readFileAndStoreInDataBase(String path,String loginId) {
		
		
		String txt="";
		int count=0;
		logger.info("read path here "+path);
		
		List<String> list=new ArrayList<String>();
		 try {
			    FileReader fileReader = new FileReader(path);
			 
			    int i;
			   
					while ((i=fileReader.read()) != -1) {
				//	  System.out.print((char) i);
					  txt=txt+((char)i);
					  
					}
					
				logger.info(" #### Text File data #### " +txt);	
				
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 
		 	String[] words = txt.split(",");
			for (int i = 0; i < words.length; i++) {
			    // You may want to check for a non-word character before blindly
			    // performing a replacement
			    // It may also be necessary to adjust the character class
			//    words[i] = words[i].replaceAll("[^\\w]", " ");
			  //  logger.info(words[i]);
			    list.add(words[i]);
			}
			
			logger.info(" #### List #### "+list);
			
			for(int i=0;i<list.size();i++) {
				PhoneDirectory phoneDirectory=new PhoneDirectory();
				logger.info("#### Index #### " + i);
			
				/*--   NAME  --*/
				String name=list.get(i);
					phoneDirectory.setName(name);
					i++;
					/*--   MOBILE  --*/	
					String mobileNo=list.get(i);
					i++;
					/*--   EMAIL  --*/
					String email=list.get(i);
					phoneDirectory.setEmailId(email);
					i++;
					/*--   DEPARTMENT NAME  --*/
					String deptName=list.get(i);
					phoneDirectory.setDepartmentName(deptName);
					i++;
					/*--   EMPLOYEE ID  --*/
					String empId=list.get(i);
					phoneDirectory.setEmpId(empId);
					i++;
					/*--   LOCATION NAME  --*/
					String locationName=list.get(i);
					phoneDirectory.setLocationName(locationName);
					i++;
					/*--   DOB  --*/
					String dob=list.get(i);
					System.out.println("length "+dob.length()+"   "+dob);
					//dob=dob.replace(" ", "-");
					if(dob.length()==10) {
					//	phoneDirectory.setDateofbirth(dob);
						boolean dobres=Pattern.matches("[0-9]{8}",dob.replace("-",""));
						logger.info("----dob status---"+dobres);
						if(dobres) {
												
							phoneDirectory.setDateofbirth(dob);
						}else {
							logger.info("\n--------------------\n  WRONG DOB : \n -------------------- ");
						}
						
					}else {
						logger.info("\n--------------------\n WRONG DOB : \n -------------------- ");
					}
					
//					i++;
//					/*--   DOB MESSAGE  --*/
//					String dobMessage=list.get(i);
//					phoneDirectory.setBdaySms(dobMessage);
//					i++;
//					/*--   DOA  --*/
//					String doa=list.get(i);
//					System.out.println("length "+doa.length());
//					doa=doa.replace(" ", "-");
//					if(doa.length()==10) {
//					
//						boolean dobres=Pattern.matches("[0-9]{8}",doa.replace("-",""));
//						logger.info("----doa status---"+dobres);
//						if(dobres) {
//							phoneDirectory.setDateofanniversary(doa);
//						}else {
//							logger.info("\n--------------------\n  WRONG DOA : \n -------------------- ");
//						}
//						
//					}else {
//						logger.info("\n--------------------\n WRONG DOA : \n -------------------- ");
//					}
//					
//					i++;
//					/*--   DOA MESSAGE  --*/
//					String doaMessage=list.get(i);
//					phoneDirectory.setAnniversarySms(doaMessage);
						
					UserEntity user=iUserEntityService.find(new UserEntity(),"Where loginId ='"+loginId+"'");
									
					String loginId1 = user.getLoginId();
					
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
					phoneDirectory.setCreateDate(sdf.format(new Date()));
					//phoneDirectory.setLocationName(user.getLocationName());
					
				
					phoneDirectory.setLoginId(user);
					if(mobileNo.length()==10) {
						boolean res=true;
						res=Pattern.matches("[6789]{1}[0-9]{9}",mobileNo);
						logger.info("-----------    "+res);
						
						
						
						if(res) {
						//	boolean result=filter.add(mobileNo);
							PhoneDirectory phoneNumber=iPhoneDirectoryService.find(phoneDirectory,"where mobileNo1='"+mobileNo+"'");
							
							if(phoneNumber==null){
								RIGHT_NUMBER_DATA=RIGHT_NUMBER_DATA+"\n "+name+"   "+mobileNo;
								RIGHT_NUMBER_COUNT++;
								phoneDirectory.setMobileNo1(mobileNo);
								iPhoneDirectoryService.save(phoneDirectory);
							}else{
								DUPLICATE_NUMBER_DATA=DUPLICATE_NUMBER_DATA+"\n "+name+"   "+mobileNo;
								DUPLICATE_NUMBE_COUNT++;
								logger.info("\n-------------------------\n DUPLICATTE MOBILE NUMBER NOT ALLOW: "+mobileNo+" \n -------------------------- ");
							}
							
						}else {
							WROUND_NUMBER_DATA=WROUND_NUMBER_DATA+"\n "+name+"   "+mobileNo;
							WROUNG_NUMBER_COUNT++;
							logger.info("\n--------------------\n WRONG MOBILE NUMBER : "+mobileNo+" \n -------------------- ");
						}
						
					}
					
				
			}
			
			
			ReportOfUploadFile(RIGHT_NUMBER_DATA,WROUND_NUMBER_DATA,DUPLICATE_NUMBER_DATA);
		
	}

	public void readExcelFile(String path,String loginId) {
		logger.info("excel file"+path);
		logger.info("excel file"+loginId);
	
		try {//new File(path)
			Workbook workbook = WorkbookFactory.create(new File(path));
			DataFormatter dataFormatter = new DataFormatter();
			 Sheet sheet = workbook.getSheetAt(0);
			 for (Row row: sheet) {
				 PhoneDirectory phone=new PhoneDirectory();
				int i=0;
				/*--   NAME  --*/
				 Cell cell1 = row.getCell(i);
				 String name=dataFormatter.formatCellValue(cell1);
				 phone.setName(name);
				 i++;
				 /*--   MOBILE  --*/
				 Cell cell2 = row.getCell(i);
				 String mobile=dataFormatter.formatCellValue(cell2);
				// phone.setMobileNo1(mobile);
				 i++;
				 /*--   EMAIL ID  --*/
				 Cell cell3 = row.getCell(i);
				 String emailId=dataFormatter.formatCellValue(cell3);
				 phone.setEmailId(emailId);
				 i++;
				 /*--   DEPARTMENT NAME  --*/
				 Cell cell4 = row.getCell(i);
				 String dept=dataFormatter.formatCellValue(cell4);
				 phone.setDepartmentName(dept);
				 i++;
				 /*--   EMPLOYEEE ID  --*/
				 Cell cell5 = row.getCell(i);
				 String empid=dataFormatter.formatCellValue(cell5);
				 phone.setEmpId(empid);
				 i++;
				 /*--   LOCATION NAME  --*/
				 Cell cell6 = row.getCell(i);
				 String locationName=dataFormatter.formatCellValue(cell6);
				 phone.setLocationName(locationName);
				 i++;
				 
				 /*--   DOB  --*/
				 Cell cell7 = row.getCell(i);
				 String dob=dataFormatter.formatCellValue(cell7);
				 phone.setDateofbirth(dob);
//				 i++;
//				 
//				 /*--   DOB MESSAGE  --*/
//				 Cell cell8 = row.getCell(i);
//				 String dobmessage=dataFormatter.formatCellValue(cell8);
//				 phone.setBdaySms(dobmessage);
//				 i++;
//				 /*--   DOA  --*/
//				 Cell cell9 = row.getCell(i);
//				 String doa=dataFormatter.formatCellValue(cell9);
//				 phone.setDateofanniversary(doa);
//				 i++;
//				 /*--   DOA MESSAGE  --*/
//				 Cell cell10 = row.getCell(i);
//				 String doamessage=dataFormatter.formatCellValue(cell10);
//				 phone.setAnniversarySms(doamessage);
				 
				 UserEntity user=iUserEntityService.find(new UserEntity(),"where loginId='"+loginId+"'");
					//phoneDirectory.setLocationName(user.getLocationName());
					phone.setLoginId(user);
				 
				 
							
				 
				 if(mobile.length()==10) {
						boolean res=true;
						res=Pattern.matches("[6789]{1}[0-9]{9}",mobile);
						logger.info("-----------    "+res);
						if(res) {
							boolean result=filter.add(mobile);
							PhoneDirectory phoneNumber=iPhoneDirectoryService.find(new PhoneDirectory(),"where mobileNo1='"+mobile+"'");
							if(phoneNumber==null){
								phone.setMobileNo1(mobile);
								logger.info("\n-------------------------\n MOBILE NUMBER  : "+mobile+" OK  \n -------------------------- ");
								SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
								RIGHT_NUMBER_DATA=RIGHT_NUMBER_DATA+"\n "+name+"   "+mobile;
								RIGHT_NUMBER_COUNT++;
								phone.setCreateDate(sdf.format(new Date()));
								iPhoneDirectoryService.save(phone);
							}else{
								DUPLICATE_NUMBE_COUNT++;
								DUPLICATE_NUMBER_DATA=DUPLICATE_NUMBER_DATA+"\n "+name+"   "+mobile;
								logger.info("\n-------------------------\n DUPLICATTE MOBILE NUMBER NOT ALLOW: "+mobile+" \n -------------------------- ");
							}
						}else {
							WROUND_NUMBER_DATA=WROUND_NUMBER_DATA+"\n "+name+"   "+mobile;
							WROUNG_NUMBER_COUNT++;
							logger.info("\n--------------------\n WRONG PATTERN MOBILE NUMBER : "+mobile+" \n -------------------- ");
						}
						
					}else {
						WROUND_NUMBER_DATA=WROUND_NUMBER_DATA+"\n "+name+"   "+mobile;
						WROUNG_NUMBER_COUNT++;
						logger.info("\n--------------------\n WRONG  MOBILE NUMBER : "+mobile+" \n -------------------- ");
					}
				 
		           
				//RIGHT_NUMBER_DATA="";
				 //WROUND_NUMBER_DATA="";
				 //DUPLICATE_NUMBER_DATA="";
//				 for(Cell cell: row) {
//		                String cellValue = dataFormatter.formatCellValue(cell);
//		                 System.out.print(cellValue + "\t");
//		               
//		                 // phone.setNamecellValue);
//		            }
				
				 
			 }
			
		} catch (EncryptedDocumentException e) {
			logger.info(" #### EncryptedDocumentException #### " +e.getMessage());
		} catch (InvalidFormatException e) {
			logger.info(" #### InvalidFormatException #### " +e.getMessage());
		} catch (IOException e) {
			
			logger.info(" #### IOException #### " +e.getMessage());
		}
		
		
		ReportOfUploadFile(RIGHT_NUMBER_DATA,WROUND_NUMBER_DATA,DUPLICATE_NUMBER_DATA);
	}
	
	public void ReportOfUploadFile(String RIGHT_NUMBER_DATA,String WROUND_NUMBER_DATA,String DUPLICATE_NUMBER_DATA) {
		
		String rightFile=UPLOADED_FOLDER+"/right_Number.txt";
		String wroungFile=UPLOADED_FOLDER+"/wroung_Number.txt";
		String duplicateFile=UPLOADED_FOLDER+"/duplicate_Number.txt";
		try {
			logger.info("\n--------------------\n "+RIGHT_NUMBER_DATA+"  \n -------------------- ");
			writer = new BufferedWriter(new FileWriter(rightFile));
			writer.write(RIGHT_NUMBER_DATA);
			writer.close();
			logger.info("\n--------------------\n "+WROUND_NUMBER_DATA+"  \n -------------------- ");
			writer = new BufferedWriter(new FileWriter(wroungFile));
			writer.write(WROUND_NUMBER_DATA);
			writer.close();
			logger.info("\n--------------------\n "+DUPLICATE_NUMBER_DATA+"  \n -------------------- ");
			writer = new BufferedWriter(new FileWriter(duplicateFile));
			writer.write(DUPLICATE_NUMBER_DATA);
		writer.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info("\n--------------------\n content file Exeption  \n -------------------- ");
			e.printStackTrace();
		}
		
	}
	
	@RequestMapping(value="/file/report")
	public ResponseEntity<List<String>> getFileReposts(){
		List<String> report=new ArrayList<String>();
		report.add(RIGHT_NUMBER_COUNT+"");
		report.add(WROUNG_NUMBER_COUNT+"");
		report.add(DUPLICATE_NUMBE_COUNT+"");
		
		return new ResponseEntity<List<String>>(report,HttpStatus.OK);
	}
}

