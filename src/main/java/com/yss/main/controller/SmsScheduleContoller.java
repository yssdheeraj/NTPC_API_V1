package com.yss.main.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.InstanceMessageDetails;
import com.yss.main.entity.ReportEntity;
import com.yss.main.entity.ScheduleMessageEntity;
import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;
import com.yss.main.sms.service.DataProvider;

@RestController
@CrossOrigin
@RequestMapping("/smsschedule")
public class SmsScheduleContoller {

	
	private static final Logger logger = LoggerFactory.getLogger(SmsController.class);
	
	@Autowired
	private IGenericService<ScheduleMessageEntity> scheduleMessageEntityService;
	
	@Autowired 
	IGenericService<InstanceMessageDetails> outtableService;
	
	@Autowired
	private IGenericService<UserEntity> userService;
	
	
	@RequestMapping(value="/delete/scheduled/sms{id}" , method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteScheduleSms(@PathVariable("id") long id){
		
		ScheduleMessageEntity objschdeuledMessageEntity = scheduleMessageEntityService.find(new ScheduleMessageEntity(), id);
		InstanceMessageDetails outObj=outtableService.find(new InstanceMessageDetails(), id);
		if(outObj !=null) {
			logger.info("------------------ \n message delete form out table \n --------------------------");
			outtableService.delete(outObj);
		}
		if (objschdeuledMessageEntity != null) {
			
			scheduleMessageEntityService.delete(objschdeuledMessageEntity);
			logger.info("####Scheduled Sms Deleted Successfully####" +objschdeuledMessageEntity.getId());
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			logger.info("####Scheduled Sms Not Delete####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}
	
	
	@RequestMapping(value="/smsschedulelist")
	public  ResponseEntity<List<ScheduleMessageEntity>> getSMSData(@RequestParam String loginId) {
		
		String query="";
		
		UserEntity user=userService.find(new UserEntity(),"where loginId ='"+loginId+"'");
		
		query=DataProvider.getQueryCondition(user,userService);
		
		//List<ReportEntity> reportList=reportEntitySerivce.fetch(new ReportEntity(),query);
		logger.info("conditionquery  "+query);
		query=query.replace("and flag  = false", "");
		logger.info("conditionquery  "+query);
		List<ScheduleMessageEntity> scheduleMessage=scheduleMessageEntityService.fetch(new ScheduleMessageEntity(),query);
		if (scheduleMessage != null) {
			logger.info("####Schedule Sms Fetch Successfully####");
			return new ResponseEntity<List<ScheduleMessageEntity>>(scheduleMessage,HttpStatus.OK);
		} else {
			logger.info("####Error in Schedule Sms Fetch####");
			return new ResponseEntity<List<ScheduleMessageEntity>>(scheduleMessage,HttpStatus.NOT_FOUND);
		}
		
	}
	

	
	@PutMapping(value="/updatesmsschedule")
	public ResponseEntity<?> updateSMS(@RequestBody ScheduleMessageEntity objTextSMS) {
		scheduleMessageEntityService.update(objTextSMS);
		logger.info("Data update successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}


	@RequestMapping(value="/deletesmsscheduless/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteSMS(@PathVariable("id") long id) {
		
		logger.info("TextSMS id "+id);
		ScheduleMessageEntity objSMS = scheduleMessageEntityService.find(new ScheduleMessageEntity(),id);
		if (objSMS == null) {
			logger.info("no data found !");
			return new ResponseEntity<>(objSMS, HttpStatus.NO_CONTENT);
		}
		else
		{
			objSMS.setFlag(true);
			scheduleMessageEntityService.delete(objSMS);
			logger.info("Data deleted successfully !");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		
	}

	
}
