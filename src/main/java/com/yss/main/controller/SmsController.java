package com.yss.main.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.yss.main.entity.ContactGroup;
import com.yss.main.entity.InstanceMessageDetails;
import com.yss.main.entity.InstanceMsgEntity;
import com.yss.main.entity.PhoneDirectory;
import com.yss.main.entity.ReportEntity;
import com.yss.main.entity.SMSEntity;
import com.yss.main.entity.ScheduleMessageEntity;
import com.yss.main.entity.TempEntity;
import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;
import com.yss.main.sms.service.DataProvider;
import com.yss.main.sms.service.InstanceMessageService;
import com.yss.main.sms.service.SendScheduledMessageSerivce;
import com.yss.main.util.AES;

@RestController
@CrossOrigin
@RequestMapping("/sms")
public class SmsController {

	private static final Logger logger = LoggerFactory.getLogger(SmsController.class);
	@Autowired
	private IGenericService<SMSEntity> iDataGenericService;

	@Autowired
	private IGenericService<UserEntity> iDataGenericuserService;

	@Autowired
	private IGenericService<InstanceMessageDetails> instanceMessageDetailsService;

	@Autowired
	private IGenericService<ScheduleMessageEntity> scheduleMessageEntityService;

	@Autowired
	private InstanceMessageService instanceMessageService;

	@Autowired
	private SendScheduledMessageSerivce sendScheduledMessageSerivce;

	@Autowired
	private DataProvider dataProvider;
	
	@Autowired
	private IGenericService<TempEntity> tempEntityService; 

	@Autowired
	private IGenericService<ContactGroup> contactGroupService;

	@Value("${filepath.uploadpath}")
	// private String UPLOADED_FOLDER ="D://NTPC_ALERT_SERVICES//";
	private String UPLOADED_FOLDER;

	@Value(value = "${connection.data.key}")
	String connectiondatakey;

	private List<String> contactList = new ArrayList<String>();

	// @GetMapping("/setDemo")
	// public ResponseEntity<?> ddddd(){
	//
	// sendScheduledMessageSerivce.findAllSmsByDate();
	// return new ResponseEntity<>(HttpStatus.OK);
	// }

	@PostMapping("/readcontacatfile")
	public ResponseEntity<List<String>> readContactFile(
			@RequestParam(value = "contactFile", required = true) MultipartFile contactFile,
			@RequestParam(value = "loginId", required = true) String loginId) {
		contactList.clear();
		logger.info("");
		try {
			String fileName = uploadDocuments(loginId, contactFile);
			contactList = readExcelFile(UPLOADED_FOLDER + loginId + "_" + fileName);
			System.out.println("cccccccccc" + contactList);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<String>>(contactList, HttpStatus.OK);
		}

		return new ResponseEntity<List<String>>(contactList, HttpStatus.OK);
	}

//save message in temp table
	@PostMapping(value = "/savetempmessage")
	public ResponseEntity<List<TempEntity>> setScheduleMessage(@RequestBody InstanceMsgEntity objinstanceMsgEntity)
			throws ParseException {

		
	logger.info("---------------------------------------------- \n "+objinstanceMsgEntity);
		if (objinstanceMsgEntity != null) {
			logger.info("####Came Message For Scheduled####" + objinstanceMsgEntity);
			objinstanceMsgEntity.setFlag(true);
			instanceMessageService.saveScheduledMessage(objinstanceMsgEntity);
			List<PhoneDirectory> duplicate = instanceMessageService.getDuplicatePhoneDirectory();
			logger.info("''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''\n Duplicate  "+duplicate+" \n '''''''''''''''''''''''''''''''''''''''''''''''''''''''");
			sendScheduledMessageSerivce.findAllSmsByDate();
			
			logger.info("----------------------- \n "+objinstanceMsgEntity.getLoginId()+"\n ------------------------------");
			List<TempEntity> tempList=tempEntityService.fetch(new TempEntity(),"where loginId='"+objinstanceMsgEntity.getLoginId().getLoginId()+"' ORDER BY phoneDirectoryName");
			
			return new ResponseEntity<>(tempList, HttpStatus.OK);
		} else {
			logger.info("####Error In Came Message For Scheduled####");
			return new ResponseEntity<>( HttpStatus.NOT_FOUND);
		}

	}
	
	
	@GetMapping("/saveschedule/{user}/{action}")
	public ResponseEntity<String> saveMessageInSchedule(@PathVariable(name="user") String user,
			@PathVariable(name="action") String action){
		
		
		List<TempEntity> tempList=tempEntityService.fetch(new TempEntity(),"where loginId='"+user+"'");
		List<ScheduleMessageEntity> scheduleMessageList=new ArrayList<>();
		String message="";
		for(TempEntity temp:tempList) {
			if(action.equals("1")) {
				ScheduleMessageEntity scheduleMessageEntity=new ScheduleMessageEntity();
				scheduleMessageEntity.setMobile(temp.getMobile());
				scheduleMessageEntity.setDateTime(temp.getDateTime());
				scheduleMessageEntity.setLoginId(temp.getLoginId());
				scheduleMessageEntity.setSmsText(temp.getSmsText());
				scheduleMessageEntity.setScheduleDate(temp.getScheduleDate());
				scheduleMessageEntity.setPhoneDirectoryName(temp.getPhoneDirectoryName());
				scheduleMessageEntity.setScheduleType(temp.getScheduleType());
				scheduleMessageEntity.setPriority(temp.getPriority());
				scheduleMessageEntity.setFlag(true);
				scheduleMessageEntity.setSmsName(temp.getSmsName());
				scheduleMessageEntity.setFlag(true);
				scheduleMessageEntity.setLocationName(temp.getLocationName());
				scheduleMessageEntity.setProject(temp.getProject());
				scheduleMessageEntity.setDepartmentName(temp.getDepartmentName());
				scheduleMessageEntity.setGrade(temp.getGrade());
				scheduleMessageEntityService.save(scheduleMessageEntity);
				tempEntityService.delete(temp);
				logger.info("''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''\n save in schedule and delete in temp  \n  "+scheduleMessageEntity+" \n '''''''''''''''''''''''''''''''''''''''''''''''''''''''");
				message="SUCCESS";
			}else {
				
				tempEntityService.delete(temp);	

				//logger.info("Data Deleted");
				message="CANCLE";
			}
			 
		}
				
		return new ResponseEntity<String>(message,HttpStatus.OK);
	}
	

	// send sms browser using url
	@RequestMapping(value = "/")
	public ResponseEntity<String> sendMessageByUrl(HttpServletResponse response,@RequestParam(value = "loginId", required = true) String loginId,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "mobile", required = false) String mobile,
			@RequestParam(value = "groupId", required = false) String groupId,
			@RequestParam(value = "groupName", required = false) String groupName,
			@RequestParam(value = "message", required = true) String message

	) throws Exception {
		// String
		// pass=com.yss.main.sms.service.PasswordEncription.generateHash(password);

		boolean checkCredential = dataProvider.checkCredentialStatus();

		if (checkCredential) {
			String u="";
			String pass = AES.encrypt(password);
			String query = "where loginId='" + loginId + "' and  password= '" + pass + "'";
			u="?loginId= "+loginId+"&password="+password;
			String str = "";
			try {
				UserEntity user = iDataGenericuserService.fetch(new UserEntity(), query).get(0);
				if (user != null) {
					if (mobile == null && groupId == null && groupName == null) {
						logger.info(
								"\n ----------------------------------- \n Bad request (mobie = null, groupId=null,groupName=null ) \n ----------------------------------- \n");
					} else {
						InstanceMsgEntity IME = new InstanceMsgEntity();
						IME.setLoginId(user);
						if (mobile != null) {
							IME.setMobile(mobile);

							str = str + "\n -------------------------\n loginId = " + loginId + ", \n MobileNo. = "
									+ mobile + ", \n Message = " + message + ",\n -------------------------\n";
						}
						if (groupName != null) {
							ArrayList<String> contactGroupName = new ArrayList<String>();
							contactGroupName.add(groupName);
							str = str + "\n -------------------------\n loginId " + loginId + ",    groupName = "
									+ groupName + ",  Message =" + message + ",\n -------------------------\n";
							IME.setContactGroupName(contactGroupName);
						}
						if (groupId != null) {
							str = str + "\n -------------------------\n loginId " + loginId + ",    groupId = "
									+ groupId + ",  Message =" + message + ",\n -------------------------\n";

							ContactGroup group = contactGroupService.find(new ContactGroup(),
									"where contact_groupId='" + groupId + "'");
							ArrayList<String> contactGroupName = new ArrayList<String>();
							contactGroupName.add(group.getContactGroupName());
							IME.setContactGroupName(contactGroupName);

						}
						IME.setSmsText(message);
						Date date = new Date();
						String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
						System.out.println("Date " + currentDate);
						IME.setScheduleDate(currentDate);
						IME.setPriority("HIGH");
						IME.setScheduleType("ONETIME");
						instanceMessageService.saveScheduledMessage(IME);
						List<PhoneDirectory> dupicate = instanceMessageService.getDuplicatePhoneDirectory();
						sendScheduledMessageSerivce.findAllSmsByDate();
						logger.info(" info  l= " + loginId + "  p= " + password + "   m= " + mobile + "  ms= " + message
								+ "  gi = " + groupId + "    gn= " + groupName + " ");
						logger.info(
								"\n ----------------------------------- \n SUCCESS \n ----------------------------------- \n");
					}

				}
				logger.info("user id " + user);
			} catch (IndexOutOfBoundsException e) {
				str = "CREDENTIALS NOT MATCHING";
				logger.info(
						"\n ----------------------------------- \n CREDENTIALS NOT MATCHING \n ----------------------------------- \n");
			} catch (NullPointerException e) {
				str = "CREDENTIALS NOT MATCHING";
				logger.info(
						"\n ----------------------------------- \n DATA NOT FOUND \n ----------------------------------- \n");
			} catch (Exception e) {
				str = "CREDENTIALS NOT MATCHING";
				logger.info("\n ----------------------------------- \n " + e.getMessage()
						+ " \n ----------------------------------- \n");
			}

			return new ResponseEntity<String>(str, HttpStatus.OK);
		} else {
			logger.info("Mapped url {[/logfile || /logfile.json],methods=[GET || HEAD]} Whitelabel Error Page ");
			
			HttpServletRequest request = (HttpServletRequest)org.apache.catalina.core.ApplicationFilterChain.getLastServicedRequest();
			System.out.println("-------------------------------------------------"+new SmsController().getCurrentUrl(request) );

		//String url="?loginId=loginid&password=password&mobile=NUM&message=your message";
		//	response.sendRedirect(url);
			return new ResponseEntity<>(HttpStatus.LOCKED);
		}

	}

	@RequestMapping(value = "/smslist")
	public ResponseEntity<List<SMSEntity>> getTextSMSData(@RequestParam String loginId) {
		logger.info("loginid in sms " + loginId);
		UserEntity user = iDataGenericuserService.find(new UserEntity(), "where loginId ='" + loginId + "'");
		String query = DataProvider.getQueryCondition(user, iDataGenericuserService);

		List<SMSEntity> TextSMSList = iDataGenericService.fetch(new SMSEntity(), query);
		if (TextSMSList != null) {
			logger.info("####Found####" + TextSMSList.size() + "Text SMS");
			return new ResponseEntity<List<SMSEntity>>(TextSMSList, HttpStatus.OK);
		} else {
			logger.info("####Error in Found Text SMS####");
			return new ResponseEntity<List<SMSEntity>>(HttpStatus.NOT_FOUND);
		}

	}

	@RequestMapping(value = "/voicesmslist")
	public ResponseEntity<List<SMSEntity>> getVoiceSMSData() {

		List<SMSEntity> sMSEntity = iDataGenericService.fetch(new SMSEntity(),
				"WHERE audioSms!= null AND flag  = false");
		if (sMSEntity != null) {
			logger.info("####Found####" + sMSEntity.size() + "Voice Sms");
			return new ResponseEntity<List<SMSEntity>>(sMSEntity, HttpStatus.OK);
		} else {
			logger.info("####Error in Found Voice Sms####");
			return new ResponseEntity<List<SMSEntity>>(HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping(value = "/createsms")
	public ResponseEntity<?> saveTextSMS(@RequestParam(value = "smsName") String smsName,
			@RequestParam(value = "smsDesc") String smsDesc, @RequestParam(value = "loginId") String loginId,
			@RequestParam(value = "uploadFile", required = false) MultipartFile uploadFile) {
		System.out.println("****************************************************************");
		logger.info("desc " + smsDesc);
		logger.info("id " + loginId);
		logger.info("sms " + smsName);

		SMSEntity sms = new SMSEntity();
		if (uploadFile != null) {
			if (!uploadFile.isEmpty()) {
				logger.info("#### File ####" + uploadFile.getOriginalFilename());
				String s = uploadFile.getOriginalFilename();
				File file = new File(s);
				sms.setFileName(uploadFile.getOriginalFilename());
				sms.setAudioSms(file);
			}
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
		sms.setCreateDate(sdf.format(new Date()));

		sms.setSmsName(smsName);
		sms.setSmsDesc(smsDesc);

		UserEntity user = iDataGenericuserService.find(new UserEntity(), "where loginId='" + loginId + "'");
		logger.info(loginId + " ----- user => " + user);
		sms.setLoginId(user);
		iDataGenericService.save(sms);
		logger.info("####Data saved successfully !" + sms);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping(value = "/updatesms")
	public ResponseEntity<?> updateTextSMS(@RequestBody SMSEntity objTextSMS) {
		iDataGenericService.update(objTextSMS);
		logger.info("Data update successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/deletesms/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteTextSMS(@PathVariable("id") long id) {

		logger.info("TextSMS id " + id);
		SMSEntity objTextSMS = iDataGenericService.find(new SMSEntity(), id);
		if (objTextSMS == null) {
			logger.info("no data found !");
			return new ResponseEntity<>(objTextSMS, HttpStatus.NO_CONTENT);
		} else {
			// objTextSMS.setFlag(true);
			iDataGenericService.delete(objTextSMS);
			logger.info("Data deleted successfully !");
			return new ResponseEntity<>(HttpStatus.OK);
		}

	}

	@PostMapping(value = "/findsms/id")
	public ResponseEntity<?> findByTextSMSId(@RequestBody SMSEntity objSMS) {

		SMSEntity TextSMS = iDataGenericService.find(objSMS, objSMS.getId());
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(TextSMS, HttpStatus.CREATED);
	}

	@PostMapping(value = "/findsms/name")
	public ResponseEntity<?> findByTextSMSame(@RequestBody SMSEntity objTextSMS) {

		SMSEntity TextSMS = iDataGenericService.find(objTextSMS, objTextSMS.getSmsName());

		logger.info("Data saved successfully !");
		return new ResponseEntity<>(TextSMS, HttpStatus.CREATED);
	}

	private String uploadDocuments(String loginId, MultipartFile multipartFile) throws Exception {

		String currentDate = "\n-----------------------------------------------------\n " + new Date()
				+ "  \n-----------------------------------------------------\n ";
		String RIGHT_NUMBER_DATA = currentDate;
		String WROUND_NUMBER_DATA = currentDate;
		String DUPLICATE_NUMBER_DATA = currentDate;
		int RIGHT_NUMBER_COUNT = 0;
		int WROUNG_NUMBER_COUNT = 0;
		int DUPLICATE_NUMBE_COUNT = 0;

		String folder_name = "";
		String fileName = "";
		String fileNameO = "";
		String newDocName = "";
		String newPath = "";
		try {
			if (!multipartFile.isEmpty()) {
				File doc = new File(UPLOADED_FOLDER + "/");

				if (!doc.exists()) {
					logger.info(" Dir " + loginId + " NOT Exists");
					doc.mkdirs();
				}

				String s = multipartFile.getOriginalFilename();
				String[] words = s.split("\\s+");
				System.out.println("word " + words);
				for (int i = 0; i < words.length; i++) {
					fileName = fileName + words[i].replaceAll(" ", " ");

					/* mobile.txt */
					logger.info("-----Trim file name----- " + fileName);

				}

				folder_name = UPLOADED_FOLDER + loginId + "/" + fileName;

				/* D:/NTPC_ALERT_SERVICES/219/mobile.txt */

				logger.info("----- Orignal folder_name :----- " + folder_name);

				File file = new File(fileName);
				File filePath = file.getAbsoluteFile();
				logger.info("------ Get Real Path Where Doc from Hit.. -------- " + filePath);

				// if (fileNamePrefix != null) {
				// newDocName = fileNamePrefix + fileName;
				// logger.info("New Document nmae " + newDocName);
				// } else {
				// newDocName = fileName;
				// logger.info("New Document nmae " + newDocName);
				//
				// }

				newDocName = fileName;
				logger.info("-----New Document name ---- " + newDocName);

				/* D:/NTPC_ALERT_SERVICES/219/mobile.txt */

				newPath = UPLOADED_FOLDER + loginId + "/" + newDocName;
				logger.info("-----rename file -----: " + newPath);
				File newFile = new File(newPath);
				logger.info("New File ----- " + newFile);

				if (!file.isDirectory()) {
					file.renameTo(newFile);
				}

				try {
					// Get the file and save it somewhere
					byte[] bytes = multipartFile.getBytes();
					logger.info("----Bytes----" + bytes);

					/* D:\NTPC_ALERT_SERVICES\219\mobile.txt */

					// Path path = Paths.get(newPath);

					Path path = Paths.get(UPLOADED_FOLDER + loginId + "_" + fileName);
					logger.info("-----path -----" + path);

					Files.write(path, bytes);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.err.println(e);
				}

				// newDocName=path.toString();
				// logger.info("newDocName After To String -------" + newDocName );
			}
		} catch (Exception e) {
			logger.info("----- IOException ----- : " + e.getMessage());
		}

		// new FileUploadService().Read();
		// new FileUploadService().Read(fileName);
		logger.info("----- ----- " + fileName);
		boolean txtfile = fileName.endsWith(".txt");
		boolean xlsfile = fileName.endsWith(".xls");
		boolean xlsxfile = fileName.endsWith(".xlsx");
		// logger.info(fileName.endsWith(".txt"));
		// logger.info(fileName.endsWith(".xls"));
		// logger.info(fileName.endsWith(".xlsx"));

		logger.info(" file path " + fileName);
		return fileName;
	}

	public List<String> readExcelFile(String path) {
		logger.info("excel file" + path);

		try {// new File(path)
			Workbook workbook = WorkbookFactory.create(new File(path));
			DataFormatter dataFormatter = new DataFormatter();
			Sheet sheet = workbook.getSheetAt(0);
			for (Row row : sheet) {
				PhoneDirectory phone = new PhoneDirectory();
				int i = 0;

				for (Cell cell : row) {
					String cellValue = dataFormatter.formatCellValue(cell);
					System.out.print(cellValue + "\t");
					contactList.add(cellValue);
				}
				System.out.println();

			}

		} catch (EncryptedDocumentException e) {
			logger.info(" #### EncryptedDocumentException #### " + e.getMessage());
		} catch (InvalidFormatException e) {
			logger.info(" #### InvalidFormatException #### " + e.getMessage());
		} catch (IOException e) {

			logger.info(" #### IOException #### " + e.getMessage());
		}

		return contactList;
	}

	
	public  String getCurrentUrl(HttpServletRequest request) throws URISyntaxException, MalformedURLException{

	    URL url = new URL(request.getRequestURL().toString());
System.out.println("---------------------------------------------going well");
	    String host  = url.getHost();
	    String userInfo = url.getUserInfo();
	    String scheme = url.getProtocol();
	    int port = url.getPort();
	    String path = (String) request.getAttribute("javax.servlet.forward.request_uri");
	    String query = (String) request.getAttribute("javax.servlet.forward.query_string");
	    URI uri = new URI(scheme,userInfo,host,port,path,query,null);
	    return uri.toString();
	}
}
