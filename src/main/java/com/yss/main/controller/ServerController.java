package com.yss.main.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yss.main.entity.DepartmentEntity;
import com.yss.main.entity.GroupWithContact;
import com.yss.main.entity.LocationEntity;
import com.yss.main.entity.PhoneDirectory;
import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;

@RestController
@CrossOrigin
public class ServerController {
	private static final Logger logger = LoggerFactory.getLogger(ServerController.class);
	@Value("${second.driver}")
	private String driver;
	@Value("${second.datasource.url}")
	String url;
	@Value("${second.datasource.username}")
	String user;
	@Value("${second.datasource.password}")
	String password;
	@Value("${second.datasource.query}")
	String query;
	
	@Value("${second.data.update.time}")
	String udateTime;
	
	private Connection con;
	
	private Map<String,PhoneDirectory> serverData = new HashMap<String,PhoneDirectory>();
	
	@Autowired
	private IGenericService<PhoneDirectory> phoneService;
	@Autowired
	private IGenericService<LocationEntity> locationService;
	@Autowired
	private IGenericService<DepartmentEntity> departmentService;
	
	@Autowired
	private IGenericService<GroupWithContact> groupWithContactService;
	@Autowired
	private IGenericService<UserEntity> userService;
	
	UserEntity loginId=new UserEntity();
	
	@RequestMapping("/server/updatedata")
	public int  updatePhoneDirectory() throws Exception {
		int data=0;
		if(getConnection() !=null) {
			  
			serverData=getdata(phoneService,userService);
			data=serverData.size();
			updatePhoneDirectoryBYServer(serverData);
			logger.info("------------------------------------ \n "+data+" PHONEDIRECTORY SUCCESSFULLY UPDATED \n ----------------------------------------");

		
			
		}
	return data;
	}
	
	
	@RequestMapping("/location/updatedata")
	public int  locationUpdate() throws Exception {
		int data=0;
		data=updateLoation();
		logger.info("------------------------------------ \n "+data+" LOCATION SUCCESSFULLY UPDATED \n ----------------------------------------");
	return data;
	}
	
	@RequestMapping("/department/updatedata")
	public int  departmentUpdate() throws Exception {
	int data=0;
	data=updateDepartment();
	logger.info("------------------------------------ \n "+data+" DEPARTMENT SUCCESSFULLY UPDATED \n ----------------------------------------");
	return data;
	}
	
	
	
	
	public Connection getConnection() throws Exception {
		Class.forName(driver);
		logger.info("--------------------------------------------- DRIVER LOADED ------------------------------------------------------");
		con=DriverManager.getConnection(url, user, password);
		logger.info("--------------------------------------------- CONNECTION ESTABLISED ------------------------------------------------------");
		return con;
	}

	//DATABASE UPDATE FROM SERVER DATABASE 
public Map<String,PhoneDirectory>  getdata(IGenericService<PhoneDirectory> phoneService,IGenericService<UserEntity> userService) throws Exception {
		
		int count=0;
		PreparedStatement ps=con.prepareStatement(query);
		logger.info("--------------------------------------------- STATEMENT CREATED ------------------------------------------------------");
		ResultSet rs=ps.executeQuery();
		while(rs.next()) {
			
			String contactId=rs.getString(1);
			String name=rs.getString(2)+" "+rs.getString(3);
			String grade=rs.getString(4); 	String departmentName=rs.getString(5); 	String project=rs.getString(6); 
			String locationName=rs.getString(7);	String mobileNo1=rs.getString(8);	String emailId=rs.getString(9);
			String substgrade=rs.getString(10);
			String createDate=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a").format(new Date());
			String empId="";
			String dateofbirth="";
			boolean personal=false;
			loginId=userService.find(new UserEntity(),"Where loginId='superUser'");
			PhoneDirectory phone=new PhoneDirectory(contactId,name,mobileNo1,grade,project,substgrade,departmentName,locationName,personal,empId,emailId,dateofbirth,createDate,loginId,false);
		
			serverData.put(contactId, phone);
			phoneService.update(phone);
			logger.info("--------------------------------------------- CUSTOMER DATA UPDATED "+contactId+"------------------------------------------------------");
			count++;
		}
		return serverData;
	}

// CONTACT FIND IN DATABASE 
public String updatePhoneDirectoryBYServer(Map<String,PhoneDirectory> serverData) {
	
	List<PhoneDirectory> phoneList=phoneService.fetch(new PhoneDirectory(),"Where personal=false");
	for(PhoneDirectory phone:phoneList) {
		
		PhoneDirectory phone1=serverData.get(phone.getContactId());
		if(phone1==null) {
			logger.info("--------------------- \n DATA NOT FOUND IN SERVER DATABASE \n -------------------");
			deleteDataFromDataBase(phone);
		}
	//	System.out.println("hhhh   "+phone1);
	}
	
	return "";
}



//FOR DELETE FROM DATABASE IF NOT FOUND DATA IN SERVER DATABASE
public void deleteDataFromDataBase(PhoneDirectory phoneDirectory) {
	GroupWithContact groupWithContact=groupWithContactService.find(new GroupWithContact(),"where phoneDirectory ='"+phoneDirectory.getContactId()+"'");
	logger.info("-------"+groupWithContact);
	if(groupWithContact!=null) {
		groupWithContactService.delete(groupWithContact);
		logger.info("--------------------- \n CONTACT  SUCCESSFULLY DELETED  FROM GROUP \n -------------------");
	}

	
	phoneService.delete(phoneDirectory);
	logger.info("------------------------ \n CONTACT ID  " +phoneDirectory.getContactId() + " IS SUCCESSFULLY DELETED \n ------------------------");
}



//FOR UPDATE LOCATION locationName

	public int  updateLoation() {
	int count=0;
		List<PhoneDirectory> phoneList=phoneService.fetchFields("distinct locationName ",new PhoneDirectory(),"order by locationName");
		 
		System.out.println("tttttttttttttttttttttttttttttttttttttttttttttt   \n "+phoneList);
		for(int i=0;i<phoneList.size();i++) {
			System.out.println("tttttttttttttttttttttttttttttttttttttttttttttt   \n "+phoneList.get(i));
			
			String locationName=phoneList.get(i)+"";
			
			if(phoneList.get(i)!=null) {
				LocationEntity location=new LocationEntity();
				location.setLocationId(i+1);
				location.setLocationName(locationName);
				location.setCreateDate(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a").format(new Date()));
				locationService.update(location);
				count++;
			}
			
		}
		return count;
	}
	
	//FOR UPDATE DEPARTMENT departmentName
	
	public int  updateDepartment() {
		int count=0;
		List<PhoneDirectory> phoneList=phoneService.fetchFields("distinct departmentName ",new PhoneDirectory(),"order by departmentName");
		logger.info(" bbbbbbbbbbbbbbbbbbbb       "+phoneList);
		loginId=userService.find(new UserEntity(),"Where loginId='superUser'");
		for(int i=0;i<phoneList.size();i++) {
			
			String departmentName=phoneList.get(i)+"";
			logger.info(" bbbbbbbbbbbbbbbbbbbb       "+phoneList);
			if(phoneList.get(i) != null) {
			DepartmentEntity department = new DepartmentEntity();
			department.setDepartmentId(i+1);
			department.setDepartmentName(departmentName);
			
			department.setLoginId(loginId);
			department.setCreatedDate(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a").format(new Date()));
			departmentService.update(department);
			count++;
			}
		}
		return count;
	}
}
