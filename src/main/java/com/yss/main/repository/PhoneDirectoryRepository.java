package com.yss.main.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.yss.main.entity.PhoneDirectory;

public interface PhoneDirectoryRepository extends PagingAndSortingRepository<PhoneDirectory,Long>{
}
