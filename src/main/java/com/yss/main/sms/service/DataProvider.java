package com.yss.main.sms.service;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yss.main.entity.UserEntity;
import com.yss.main.generic.service.IGenericService;

@Service
@Transactional
public class DataProvider {
	
	
	private static final Logger logger = LoggerFactory.getLogger(InstanceMessageService.class);
	
	@Value(value= "${connection.data.key}")
	String connectiondatakey;
	
	boolean CREDENTIAL_STATUS;
	
	//user and phone directory
	public static String getQueryCondition(UserEntity user){
		String query="";
		logger.info("user type--------------------"+user.getUserType());
		if(user.getUserType().equals("superadmin")){
			query="where flag =false";
		}else if(user.getUserType().equals("admin"))
		{
			logger.info("admin.....................");
			 query="WHERE locationName='"+user.getLocationName()+"' and flag  = false";
		}else if(user.getUserType().equals("user")){
			logger.info("usssr.....................");
			query="WHERE  locationName='"+user.getLocationName()+"' and departmentName='"+user.getDepartmentName()+"' and flag  = false";
		}
		logger.info("query  "+query);
		return query;
	}
	
	
	//sms and contact group
	public static String getQueryCondition(UserEntity user,IGenericService<UserEntity> userService){
		String query="";
		String loginId=user.getLoginId();
		if(user.getUserType().equals("superadmin")){
			query="Where flag=false";
		}else if(user.getUserType().equals("admin")){
			List<UserEntity> userList=userService.fetch(new UserEntity(),"where createdBy ='"+loginId+"'");
			query="WHERE loginId ='"+loginId+"'";
			for(UserEntity user1:userList){
				query=query+" OR loginId = '"+user1.getLoginId()+"'";
	
			}
			query=query+" and flag = false";
			
		}else if(user.getUserType().equals("user")){
			query="WHERE loginId='"+loginId+"' and flag  = false";
		}
		logger.info("query  "+query);
		return query;
	}
	
	
	//department 
	public static String getQueryCondition(String loginId,IGenericService<UserEntity> userService){
		UserEntity user=userService.find(new UserEntity(),"where loginId ='"+loginId+"'");
		String query="";
		if(user.getUserType().equals("superadmin")){
			query="where flag=false";
		}else if(user.getUserType().equals("admin")){
			query="where loginId='"+loginId+"' and flag=false";
		}else if(user.getUserType().equals("user")){
			query="where loginId='"+loginId+"' and flag=false";
		}
	
		return query;
	}
	
	
/*	*********************** License Check ***********************************/
	public boolean checkCredentialStatus() {
		String xdate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String d1 = connectiondatakey.substring(2,4);

		String d2 = connectiondatakey.substring(4,8);

		String d3 = connectiondatakey.substring(10,12);

		String dt = d1+d3+d2;
		System.out.println("dt : "+dt);
		System.out.println("xdate : "+xdate);
		long sd1 = Long.parseLong(dt);
		long sd2 = Long.parseLong(xdate);
		
		if (sd2 <= sd1) {
		CREDENTIAL_STATUS = true;
		return CREDENTIAL_STATUS;
		}
		else {
			
			CREDENTIAL_STATUS = false;
			return CREDENTIAL_STATUS;
		}
	}
	

	//pagination 
	public static String getQueryConditionPagination(UserEntity user,IGenericService<UserEntity> userService){
		String query="";
		String loginId=user.getLoginId();
		if(user.getUserType().equals("superadmin")){
		query="WHERE smsText!=''";
		}else if(user.getUserType().equals("admin")){
			List<UserEntity> userList=userService.fetch(new UserEntity(),"where createdBy ='"+loginId+"'");
			query="WHERE loginId ='"+loginId+"'";
			for(UserEntity user1:userList){
				query=query+" OR loginId = '"+user1.getLoginId()+"'";
	
			}
			
			
		}else {
			query="where loginId='"+loginId+"'";
		}
		logger.info("query  "+query);
		return query;
	}
	
}
