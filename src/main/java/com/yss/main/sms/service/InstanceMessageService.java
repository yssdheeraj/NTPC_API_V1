package com.yss.main.sms.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yss.main.entity.ContactGroup;
import com.yss.main.entity.DepartmentEntity;
import com.yss.main.entity.GroupWithContact;
import com.yss.main.entity.InstanceMessageDetails;
import com.yss.main.entity.InstanceMsgEntity;
import com.yss.main.entity.PhoneDirectory;
import com.yss.main.entity.ReportEntity;
import com.yss.main.entity.SMSEntity;
import com.yss.main.entity.ScheduleMessageEntity;
import com.yss.main.entity.TempEntity;
import com.yss.main.generic.service.IGenericService;


@Service
@Transactional
public class InstanceMessageService {
	
	private static final Logger logger = LoggerFactory.getLogger(InstanceMessageService.class);
	@Autowired
	private IGenericService<DepartmentEntity> departmentDataGenericService;
	
	@Autowired
	private IGenericService<GroupWithContact> allContactGroupGenericService;
	
	@Autowired
	private IGenericService<ReportEntity> reportEntityService;
	
	@Autowired
	private IGenericService<ContactGroup> contactGroupService;
	
	@Autowired
	private IGenericService<PhoneDirectory> phoneDirectoryService;
	
	@Autowired
	private IGenericService<SMSEntity> ismsService;

	@Autowired
	private IGenericService<InstanceMessageDetails> instanceMessageDetailsService;
	
	@Autowired
	private IGenericService<ScheduleMessageEntity> scheduleMessageEntityService; 
	@Autowired
	private IGenericService<TempEntity> tempEntityService; 
	
	private String MESSAGE="";
	private String SCHEDULE_DATE;
	private String SCHEDULETIME;
	private String PRIORITY="";
	private String SCHEDULE_TYPE=""; 
	private String SMS_NAME="";
	private String GROUP_NAME="";
	private String USER_ID="";
	private String MOBILE_NUMBERS="";
	Date TODAY_DATE = new Date();
	HashSet<String> filter=new HashSet<String>();
	List<PhoneDirectory> duplicateList=new ArrayList<PhoneDirectory>();
	List<String> uploadContactList=new ArrayList<String>();
	List<String> projectList=new ArrayList<String>();
	List<String> locationList=new ArrayList<String>();
	List<String> departmentList=new ArrayList<String>();
	List<String> gradeList=new ArrayList<String>();
	
	
	
	/******************************** FOR SCHEDULE MESSAGE **************************************************************/
	public void saveScheduledMessage(InstanceMsgEntity instanceMsgEntity) {
		
		filter.clear();
		
		SCHEDULE_DATE=instanceMsgEntity.getScheduleDate();
		
		logger.info("\n ----Schedule Time \n"+SCHEDULE_DATE+"\n ..............................");
		
		PRIORITY = "HIGH";
		SCHEDULE_TYPE = instanceMsgEntity.getScheduleType();
		SMS_NAME = instanceMsgEntity.getSmsName();
		USER_ID = instanceMsgEntity.getLoginId().getLoginId();
		MOBILE_NUMBERS=instanceMsgEntity.getMobile();
		
		
		logger.info("-------------------upload list----------------------\n "+instanceMsgEntity.getUploadContactList());
		
		if(instanceMsgEntity.getSmsName() !=null ||instanceMsgEntity.getSmsText() !=null ) {
			MESSAGE=getRealMessage(instanceMsgEntity.getSmsName(),instanceMsgEntity.getSmsText());
			logger.info("message created.............................");
		
			if (instanceMsgEntity.getPhoneDirectoryName() !=null) {
				getPhoneDirectoryByMobileList(instanceMsgEntity.getPhoneDirectoryName());
				logger.info("----Phonedirectory List Fetched..............................");
			}
		
			if (instanceMsgEntity.getContactGroupName() !=null ) {
				getPhoneDirectoryListByGroups(instanceMsgEntity.getContactGroupName());
				logger.info("-----Contact Group List Fetched..............................");
			}
			
			//save data according phone number comma separated mobile number list 
			if(instanceMsgEntity.getMobile() !=null ) {
				//convertMobileNumber(MOBILE_NUMBERS);
				getPhoneDirectoryByMobileNumber(convertMobileNumber(MOBILE_NUMBERS));
				logger.info("------Enter Mobile Number Fetched..............................");
			}
			
			//upload file contact list
			if(instanceMsgEntity.getUploadContactList()!=null) {
				uploadContactList=instanceMsgEntity.getUploadContactList();
				getPhoneDirectoryByMobileNumber(uploadContactList);
			}
			
			
			if(instanceMsgEntity.getLocationName()!=null || instanceMsgEntity.getDepartmentName()!=null ) {
				
				
				getPhoneDirectoryListByDepartmentNameList(instanceMsgEntity.getProjectName(),instanceMsgEntity.getLocationName(),instanceMsgEntity.getDepartmentName());
				logger.info("-----Department List Fetched ..............................");
			}
			
			
           if(instanceMsgEntity.getLocationName()!=null || instanceMsgEntity.getGrade()!=null ) {
				
				
				getPhoneDirectoryListByGradeNameList(instanceMsgEntity.getProjectName(),instanceMsgEntity.getLocationName(),instanceMsgEntity.getGrade());
				logger.info("-----Grade List Fetched ..............................");
			}
			
			
			//save data according to project 
			if(instanceMsgEntity.getProjectName()!=null){
				logger.info("project \n "+instanceMsgEntity.getProjectName());
				getPhoneDirectoryListByProject(instanceMsgEntity.getProjectName());
			}
		}
		
		else {
			logger.info("----Please Enter Sms-----");
		}
		
		
	}
	
	
	

	

	//create real sms 
	public String  getRealMessage(String smsName,String smstxt) {
		String message="";
		if(smsName !=null && !smsName.equals("")){
			SMSEntity sMSEntity=ismsService.find(new SMSEntity(),"where smsName = '"+smsName+"'");
			message=sMSEntity.getSmsDesc()+" \n ";
		}
		if(smstxt !=null){
			message=message+smstxt;
		}
//		else
//		{
//			message=smstxt;
//		}
		logger.info("This is real sms created  "+message);	
		return message;
		}
	
	
	//get phone directory by mobile number list
	public void getPhoneDirectoryByMobileList(ArrayList<String> phoneDirectoryName){
		if(phoneDirectoryName !=null)
		{
			for(String mobile1 :phoneDirectoryName ) {
				
				
				ScheduleMessageEntity  scheduleMessageEntity = new ScheduleMessageEntity(); 
				//InstanceMessageDetails instanceMessageDetails=new InstanceMessageDetails();
				PhoneDirectory phoneDirectory=phoneDirectoryService.find(new PhoneDirectory(), "where mobileNo1 = '"+mobile1+"'");
				if(phoneDirectory !=null) {
				
					
						savedataInInstanceSMSDB(phoneDirectory);
				
					
				}
				
								
			}
		}
		
	}
	
	
	//get phone directory by mobile number  comma separated mobile number
	public void getPhoneDirectoryByMobileNumber(List<String> mobileList) {
		
		/*InstanceMessageDetails instanceMessageDetails=new InstanceMessageDetails();
		instanceMessageDetails.setMobile(mobile1);
		instanceMessageDetails.setDateAndTime(new Date().toString());
		instanceMessageDetails.setSendBy("Admin");
		instanceMessageDetails.setSmsText(MESSAGE);
		instanceMessageDetails.setPriority(PRIORITY);
		instanceMessageDetails.setScheduleDate(SCHEDULE_DATE);
		instanceMessageDetails.setScheduleType(SCHEDULE_TYPE);
		instanceMessageDetails.setFlag(true);*/
	
		
		logger.info("\n--------------------------------------------------\n data sava in schedule  table \n--------------------------------------------------\n");
		for(String mobile1:mobileList) {
			String mobile=mobileFilter(mobile1);
				
			
			if(mobile!= null){
				
			//	ScheduleMessageEntity  scheduleMessageEntity = new ScheduleMessageEntity();
				TempEntity  scheduleMessageEntity = new TempEntity();
				
				String number=mobile;
				logger.info(" ****************** Your mobile number "+number);
				boolean result=filter.add(number);
				logger.info(" ****************** Not Duplicate "+result);
					scheduleMessageEntity.setMobile(mobile1);
					scheduleMessageEntity.setDateTime(new Date().toString());
					scheduleMessageEntity.setLoginId(USER_ID);
					scheduleMessageEntity.setSmsText(MESSAGE);
					scheduleMessageEntity.setScheduleDate(SCHEDULE_DATE);
					scheduleMessageEntity.setPhoneDirectoryName("Unknown");
					scheduleMessageEntity.setScheduleType(SCHEDULE_TYPE);
					scheduleMessageEntity.setPriority(PRIORITY);
					scheduleMessageEntity.setFlag(true);
					scheduleMessageEntity.setSmsName(SMS_NAME);
					
					logger.info("\n--------------------------------------------------\n "+scheduleMessageEntity+" \n--------------------------------------------------\n");
					if(result) {	
						tempEntityService.save(scheduleMessageEntity);
					
					}else {
					PhoneDirectory phoneDirectory=new PhoneDirectory();
					phoneDirectory.setMobileNo1(number);
					duplicateList.add(phoneDirectory);
					logger.info("**********************************************************");
					logger.info("dupicate value   "+phoneDirectory);
					logger.info("**********************************************************");
				}
				
			}else {
				logger.info("\n ------------------------------------\n  wroung number "+mobile1+"\n ------------------------------\n");
			}
		
		
		}
		
	
		
	}
// mobile number filter	
public	String mobileFilter(String mobileNo){
		if(mobileNo.length()==10) {
			boolean res=true;
			res=Pattern.matches("[6789]{1}[0-9]{9}",mobileNo);
			logger.info("----------- Number Formate is correct   "+res);
			if(res) {
				return mobileNo;
			}else {
				mobileNo=null;
			}
			
		}
		return mobileNo;
	}
	//get phone directory list by contact group
	public void getPhoneDirectoryListByGroups(ArrayList<String> contactGroupName) {
		
		if(contactGroupName !=null)
		{
			GROUP_NAME="";
			for(String groupName :contactGroupName ) {
				GROUP_NAME = groupName;
				ContactGroup contactGroup=contactGroupService.find(new ContactGroup(), "where contactGroupName = '"+groupName+"'");
				
				List<GroupWithContact> contactList = allContactGroupGenericService.findAllByCondition(new GroupWithContact(), "where contactGroup='"+contactGroup.getContact_groupId()+"'");
				System.out.println("####################################" +contactList);
				logger.info("data save according ot phone directory----------------------------"+contactGroup);
				logger.info("constact list "+contactList);
				
				

				for (GroupWithContact groupWithContact : contactList) {
					
					PhoneDirectory phoneDirectory=groupWithContact.getPhoneDirectory();
					
					if(phoneDirectory !=null) {
							savedataInInstanceSMSDB(phoneDirectory);
					}
					
				}
				

			}
		}
		
	}
	
	
	//get phone directory by project 
public void getPhoneDirectoryListByProject(ArrayList<String> projectNameList) {
		
	logger.info("------Data  Fetched By project ..............................");
		if(projectNameList !=null)
		{
			for(String project :projectNameList ) {
					
				String query="where project = '"+project+"'";
				List<PhoneDirectory> phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
					
				for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
					if(phoneDirectory !=null) {
						savedataInInstanceSMSDB(phoneDirectory);
				}
		
				}
				
				

			}
		}
		
	}
	
	
	
	
	
	
	
	//get phone directory list by department list
	public void getPhoneDirectoryListByDepartmentNameList(ArrayList<String> projectList,ArrayList<String> locationNameList,ArrayList<String> departmentNameList)
	{
		
		logger.info("-----------------------Project list --------------- "+projectList);
		logger.info("-----------------------Locatino list --------------- "+locationNameList);
		logger.info("------------------------Department list-------------- "+departmentNameList);
		List<PhoneDirectory> phoneDirectoryList=new ArrayList<PhoneDirectory>();
		
		
		if(projectList!=null && locationNameList !=null && departmentNameList!=null ) {
			
			
			for(String project:projectList) {
				
				for(String location:locationNameList) {
					
					for(String department:departmentNameList) {
						
						String query ="where project ='"+project+"' AND locationName='"+location+"' and departmentName = '"+department+"'";
						phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
						
						logger.info(" data by project and location and department");
						for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
							
							if(phoneDirectory !=null) {
									
									savedataInInstanceSMSDB(phoneDirectory);
							
							}
						}

						
					}
				}
			}
		}else if(projectList!=null && locationNameList !=null) {
			
	for(String project:projectList) {
				
				for(String location:locationNameList) {
					
											
						String query ="where project ='"+project+"' AND locationName='"+location+"'";
						phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
						
						logger.info(" data by project and location");
						for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
							
							if(phoneDirectory !=null) {
									
									savedataInInstanceSMSDB(phoneDirectory);
							
							}
						}

						
					}
				}
			
			
		}else if(projectList!=null && departmentNameList !=null) {
			
			for(String project:projectList) {
				
					
					for(String department:departmentNameList) {
						
						String query ="where project ='"+project+"' AND departmentName = '"+department+"'";
						phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
						
						logger.info(" data by project  and department");
						for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
							
							if(phoneDirectory !=null) {
									
									savedataInInstanceSMSDB(phoneDirectory);
							
							}
						}

						
				
				}
			}

		}else if(locationNameList !=null && departmentNameList!=null) {
			
			for(String location:locationNameList) {
				
				for(String department:departmentNameList) {
					
					String query="where locationName = '"+location+"' AND departmentName = '"+department+"'";
					phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
					
					logger.info(" data by location and department");
					for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
						
						if(phoneDirectory !=null) {
								
								savedataInInstanceSMSDB(phoneDirectory);
						
						}
					}
				}
			}
			
		}else if(projectList!=null) {
			for(String project:projectList) {
				
					String query ="where project ='"+project+"'";
						phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
						
						logger.info(" data by project ");
						for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
							
							if(phoneDirectory !=null) {
									
									savedataInInstanceSMSDB(phoneDirectory);
							
							}
						}

					}

			
		}else if(locationNameList !=null) {
				for(String location:locationNameList) {
				
						
					String query="where locationName = '"+location+"'";
					phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
					
					logger.info(" data by location");
					for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
						
						if(phoneDirectory !=null) {
								
								savedataInInstanceSMSDB(phoneDirectory);
						
						}
					}
				}
			}else if(departmentNameList !=null) {
				
				for(String department:departmentNameList) {
					
					String query="where departmentName = '"+department+"'";
					phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
					
					logger.info(" data by location and department");
					for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
						
						if(phoneDirectory !=null) {
								
								savedataInInstanceSMSDB(phoneDirectory);
						
						}
					}
				}
			}
			
		}
		

	
	
	//get phone directory list by Grade list
	public void getPhoneDirectoryListByGradeNameList(ArrayList<String> projectList,ArrayList<String> locationNameList,ArrayList<String> gradeList)
	{
		
		logger.info("-----------------------Project list --------------- "+projectList);
		logger.info("-----------------------Locatino list --------------- "+locationNameList);
		logger.info("------------------------Department list-------------- "+gradeList);
		List<PhoneDirectory> phoneDirectoryList=new ArrayList<PhoneDirectory>();
		
		
		if(projectList!=null && locationNameList !=null && gradeList!=null ) {
			
			
			for(String project:projectList) {
				
				for(String location:locationNameList) {
					
					for(String grade:gradeList) {
						
						String query ="where project ='"+project+"' AND locationName='"+location+"' and grade = '"+grade+"'";
						phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
						
						logger.info(" data by project and location and grade");
						for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
							
							if(phoneDirectory !=null) {
									
									savedataInInstanceSMSDB(phoneDirectory);
							
							}
						}

						
					}
				}
			}
		}else if(projectList!=null && locationNameList !=null) {
			
	for(String project:projectList) {
				
				for(String location:locationNameList) {
					
											
						String query ="where project ='"+project+"' AND locationName='"+location+"'";
						phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
						
						logger.info(" data by project and location");
						for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
							
							if(phoneDirectory !=null) {
									
									savedataInInstanceSMSDB(phoneDirectory);
							
							}
						}

						
					}
				}
			
			
		}else if(projectList!=null && gradeList !=null) {
			
			for(String project:projectList) {
				
					
					for(String grade:gradeList) {
						
						String query ="where project ='"+project+"' AND grade = '"+grade+"'";
						phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
						
						logger.info(" data by project  and department");
						for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
							
							if(phoneDirectory !=null) {
									
									savedataInInstanceSMSDB(phoneDirectory);
							
							}
						}

						
				
				}
			}

		}else if(locationNameList !=null && gradeList!=null) {
			
			for(String location:locationNameList) {
				
				for(String grade:gradeList) {
					
					String query="where locationName = '"+location+"' AND grade = '"+grade+"'";
					phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
					
					logger.info(" data by location and grade");
					for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
						
						if(phoneDirectory !=null) {
								
								savedataInInstanceSMSDB(phoneDirectory);
						
						}
					}
				}
			}
			
		}else if(projectList!=null) {
			for(String project:projectList) {
				
					String query ="where project ='"+project+"'";
						phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
						
						logger.info(" data by project ");
						for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
							
							if(phoneDirectory !=null) {
									
									savedataInInstanceSMSDB(phoneDirectory);
							
							}
						}

					}

			
		}else if(locationNameList !=null) {
				for(String location:locationNameList) {
				
						
					String query="where locationName = '"+location+"'";
					phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
					
					logger.info(" data by location");
					for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
						
						if(phoneDirectory !=null) {
								
								savedataInInstanceSMSDB(phoneDirectory);
						
						}
					}
				}
			}else if(gradeList !=null) {
				
				for(String grade:gradeList) {
					
					String query="where grade = '"+grade+"'";
					phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),query);
					
					logger.info(" data by location and grade");
					for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
						
						if(phoneDirectory !=null) {
								
								savedataInInstanceSMSDB(phoneDirectory);
						
						}
					}
				}
			}
			
		}
	
	
	
	
	
	
	
	
	//save data using phone directory
	public void savedataInInstanceSMSDB(PhoneDirectory phoneDirectory){
		
	//	ScheduleMessageEntity  scheduleMessageEntity = new ScheduleMessageEntity(); 
		TempEntity  scheduleMessageEntity = new TempEntity(); 
		
		if(phoneDirectory !=null) {
			
//			instanceMessageDetails.setSmsName(phoneDirectory.getName());
//			instanceMessageDetails.setMobile(phoneDirectory.getMobileNo1());
//			instanceMessageDetails.setDateAndTime(new Date().toString());
//			instanceMessageDetails.setSendBy("Admin");
//			instanceMessageDetails.setSmsText(MESSAGE);
//			instanceMessageDetails.setPriority(PRIORITY);
//			instanceMessageDetails.setScheduleDate(SCHEDULE_DATE);
//			instanceMessageDetails.setScheduleType(SCHEDULE_TYPE);
//			instanceMessageDetails.setFlag(true);
			
			System.out.println(phoneDirectory);
			String number=phoneDirectory.getMobileNo1();
			logger.info(" ****************** Your mobile number "+number);
			boolean result=filter.add(phoneDirectory.getMobileNo1());
			logger.info(" number "+result);
			if(result) {
				//========================================================================================================			
				//scheduleMessageEntity.setDepartmentName(phoneDirectory.getDepartment().getDepartmentName());
				scheduleMessageEntity.setContactGroupName(GROUP_NAME);
				scheduleMessageEntity.setSmsName(SMS_NAME);
				scheduleMessageEntity.setPhoneDirectoryName(phoneDirectory.getName());
				scheduleMessageEntity.setMobile(phoneDirectory.getMobileNo1());
				//scheduleMessageEntity.setDateTime(new Date().toString());
				scheduleMessageEntity.setDateTime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date()).toString());
				scheduleMessageEntity.setLoginId(USER_ID);
				scheduleMessageEntity.setSmsText(MESSAGE);
				scheduleMessageEntity.setPriority(PRIORITY);
				scheduleMessageEntity.setScheduleDate(SCHEDULE_DATE);
				scheduleMessageEntity.setScheduleType(SCHEDULE_TYPE);
				scheduleMessageEntity.setFlag(true);
				scheduleMessageEntity.setLocationName(phoneDirectory.getLocationName());
				scheduleMessageEntity.setProject(phoneDirectory.getProject());
				scheduleMessageEntity.setDepartmentName(phoneDirectory.getDepartmentName());
				scheduleMessageEntity.setGrade(phoneDirectory.getGrade());
				logger.info("data save according ot phone directory----------------------------"+scheduleMessageEntity);
				tempEntityService.save(scheduleMessageEntity);
				
				
	//==================================================================================================
				
			}else {
				duplicateList.add(phoneDirectory);
				logger.info("**********************************************************");
				logger.info("dupicate value   "+phoneDirectory);
				logger.info("**********************************************************");
			}
			
			
//========================================================================================================			
			/*scheduleMessageEntity.setDepartmentName(phoneDirectory.getDepartment().getDepartmentName());
			scheduleMessageEntity.setContactGroupName(GROUP_NAME);
			scheduleMessageEntity.setSmsName(SMS_NAME);
			scheduleMessageEntity.setPhoneDirectoryName(phoneDirectory.getName());
			scheduleMessageEntity.setMobile(phoneDirectory.getMobileNo1());
			scheduleMessageEntity.setDateTime(new Date().toString());
			scheduleMessageEntity.setSendBy(USER_ID);
			scheduleMessageEntity.setSmsText(MESSAGE);
			scheduleMessageEntity.setPriority(PRIORITY);
			scheduleMessageEntity.setScheduleDate(SCHEDULE_DATE);
			scheduleMessageEntity.setScheduleType(SCHEDULE_TYPE);
			scheduleMessageEntity.setFlag(true);
			
			logger.info("data save according ot phone directory----------------------------"+scheduleMessageEntity);
			scheduleMessageEntityService.save(scheduleMessageEntity);
			*/
			
//==================================================================================================
			
//			reportEntity.setContactGroupName(instanceMessageDetails.getContactGroupName());
//			reportEntity.setMobile(instanceMessageDetails.getMobile());
//			reportEntity.setDateAndTime(instanceMessageDetails.getDateAndTime());
//			reportEntity.setSendBy(instanceMessageDetails.getSendBy());
//			reportEntity.setSmsText(instanceMessageDetails.getSmsText());
//			reportEntity.setPriority(instanceMessageDetails.getPriority());
//			reportEntity.setScheduleDate(instanceMessageDetails.getScheduleDate());
//			reportEntity.setScheduleType(instanceMessageDetails.getScheduleType());
			
			//logger.info("data save according ot phone directory in ReportEntity----------------------------"+reportEntity);
			//reportEntityService.save(reportEntity);
			
	
		}
	}
		
	public List<PhoneDirectory> getDuplicatePhoneDirectory(){
		
		logger.info("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		return duplicateList;
	}
	
	// convert  string mobileNumber in String array
	public List<String> convertMobileNumber(String MOBILE_NUMBERS){
		List<String> list=new ArrayList<>();
		String number[]=MOBILE_NUMBERS.split(",");
		
		for (int i = 0; i < number.length; i++) {
		    // You may want to check for a non-word character before blindly
		    // performing a replacement
		    // It may also be necessary to adjust the character class
			number[i] = number[i].replaceAll("[^\\w]", "");
		  //  System.out.println(words[i]);
			
			if(number[i].trim().length()==10) {
				logger.info("length of number .."+number[i].trim().length()+" OK");
				 list.add(number[i]);
			}else {
				logger.info("length of number .."+number[i].trim().length()+" ERROR ");
				logger.info(number[i].trim()+"  NOT A VALID NUMBER");
			}
		   
		}
		
		logger.info("list of comma separated mobile number  "+list);
		return list;
	}
	
	//fetch a smslist
	

	
	
}
