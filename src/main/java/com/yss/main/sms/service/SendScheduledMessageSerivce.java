package com.yss.main.sms.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.yss.main.entity.InstanceMessageDetails;
import com.yss.main.entity.ReportEntity;
import com.yss.main.entity.ScheduleMessageEntity;
import com.yss.main.generic.service.IGenericService;

@Service
@Transactional
@Component
public class SendScheduledMessageSerivce {

	private static final Logger logger = LoggerFactory.getLogger(SendScheduledMessageSerivce.class);

	@Autowired
	private IGenericService<ScheduleMessageEntity> scheduleMessageEntityService;

	@Autowired
	private IGenericService<InstanceMessageDetails> instanceMessageDetailsSerivce;

	@Autowired
	private IGenericService<ReportEntity> reportEntityService;

	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

	String PRIORITY = "";

	String SCHEDULE_TYPE = "";
	String SCHEDULE_DATE = "";
	int LENGTH = 0;

	public void findAllSmsByDate() throws ParseException {

		Date todayDate = new Date();
		logger.info("Default Date *********************" + todayDate);
		SimpleDateFormat sm = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String strDate = sm.format(todayDate);
		logger.info("str Date *********************" + strDate);
		String query = "WHERE scheduleDate <= '"+strDate+"' and flag  = true";
		logger.info(query);
		List<ScheduleMessageEntity> sendSmslists = scheduleMessageEntityService
				.findAllByCondition(new ScheduleMessageEntity(), query);
		logger.info("--------Today Sms List--------" + sendSmslists);
		logger.info("-------- No date found of current date--------" + sendSmslists.size());

		for (ScheduleMessageEntity scheduleMessageEntity : sendSmslists) {

			if (scheduleMessageEntity != null) {
				logger.info("-------Save Sms For Mobile-------- " + scheduleMessageEntity.getMobile()
						+ "\n --------And Date-------- " + scheduleMessageEntity.getScheduleDate());
				saveOutGoingSms(scheduleMessageEntity);
			}

		}
	}

	public void saveOutGoingSms(ScheduleMessageEntity ddd) throws ParseException {
		InstanceMessageDetails imd = new InstanceMessageDetails();
		imd.setId(ddd.getId());
		imd.setMobile(ddd.getMobile());
		imd.setSmsText(ddd.getSmsText());
		imd.setMessageDate(new Date().toString());
		imd.setFlag(true);
	//	imd.setFld_TrunkNumber("12345");
		logger.info("--------------------Saved Successfully in Smsq--------------------------");
		instanceMessageDetailsSerivce.save(imd);
		ddd.setFlag(false);
		
		if(ddd.getScheduleType().equalsIgnoreCase("ONETIME")) {
			ddd.setFlag(false);
			scheduleMessageEntityService.update(ddd);
			logger.info(" data delete from table ==> onetime");
		}else {
			
			scheduleMessageEntityService.save(setScheduleDateAgain(ddd));
		}
		

	}

	public ScheduleMessageEntity setScheduleDateAgain(ScheduleMessageEntity scheduleMessageEntity)
			throws ParseException {
		String newScheduleDate = "";
		String SCHEDULE_TYPE = scheduleMessageEntity.getScheduleType();
		logger.info("---------------------- \n " + SCHEDULE_TYPE + "\n ---------------------- \n ");
		switch (SCHEDULE_TYPE) {
		case "YEARLY":

			newScheduleDate = getNextYear(scheduleMessageEntity.getScheduleDate());
			scheduleMessageEntity.setScheduleDate(newScheduleDate);
			scheduleMessageEntity.setFlag(true);
			logger.info("----YEARLY----");

			break;

		case "MONTHLY":
			newScheduleDate = getNextMonth(scheduleMessageEntity.getScheduleDate());
			scheduleMessageEntity.setScheduleDate(newScheduleDate);
			scheduleMessageEntity.setFlag(true);
			logger.info("----MONTHLY----");
			break;

		case "WEEKLY":
			logger.info("----WEEKLY----");

			newScheduleDate = getNextWeak(scheduleMessageEntity.getScheduleDate());
			scheduleMessageEntity.setScheduleDate(newScheduleDate);
			scheduleMessageEntity.setFlag(true);
			break;

		case "DAILY":
			logger.info("----DAILY----");

			newScheduleDate = getNextDate(scheduleMessageEntity.getScheduleDate());
			logger.info("-----nnnnnnnnnnnnnnnnnnnn---" + newScheduleDate + "----------");
			scheduleMessageEntity.setScheduleDate(newScheduleDate);
			scheduleMessageEntity.setFlag(true);

			break;

		case "ONETIME":
			logger.info("----ONETIME----");
			scheduleMessageEntity = scheduleMessageEntityService.find(new ScheduleMessageEntity(),
					scheduleMessageEntity.getId());
			scheduleMessageEntityService.delete(scheduleMessageEntity);
			logger.info("data successfully delete form schedule table...." + scheduleMessageEntity);

			break;

		default:
			logger.info("------------INVALID SCHEDULE TYPE---------");
		}

		logger.info(SCHEDULE_TYPE + " updated entity  " + scheduleMessageEntity);

		return scheduleMessageEntity;
	}

	public static String getNextDate(String scheduleDate) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = format.parse(scheduleDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		return format.format(calendar.getTime());
	}

	public static String getNextMonth(String scheduleDate) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = format.parse(scheduleDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, 1);
		return format.format(calendar.getTime());
	}

	public static String getNextWeak(String scheduleDate) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = format.parse(scheduleDate + "");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 7);
		return format.format(calendar.getTime());
	}

	public static String getNextYear(String scheduleDate) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = format.parse(scheduleDate + "");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, 1);
		return format.format(calendar.getTime());
	}

	/**********************************************************************************************************************************************************/
	// @Scheduled(fixedDelay = 5000)
	// @Scheduled(fixedRate = 5000)
	// public void scheduleTaskWithFixedDelay() {
	// logger.info("Fixed Delay Task :: Execution Tasks - {} " ,
	// dateTimeFormatter.format(LocalDateTime.now()));
	//
	// try {
	// TimeUnit.SECONDS.sleep(5);
	// logger.info("---------------Call a method for Send Sm------------");
	// //findScheduletype();
	// findAllByDate();
	//
	//
	// } catch (InterruptedException ie) {
	//
	// logger.error("Enter into an Error-----------" + ie);
	// throw new IllegalStateException(ie);
	//
	// }
	// }
	

	/***********************************************
	 * Fires Daily AT 11Am Everyday
	 * 
	 * @throws ParseException
	 ************************************************************/

	 //@Scheduled(cron = "0 0 11 1/1 * ?")
	 @Scheduled(cron = "0/10 * * * * ?" ,zone="Asia/Calcutta")
//	 @Scheduled(cron = "0 0 0/1 1/1 * ?")         //one hour
//	 @Scheduled(cron="10 */1 * * * ?")  //10 second
	 public void scheduleTaskWithDaily() throws ParseException {
	 logger.info("Cron Taks :: Excecution Time - {}",
	 dateTimeFormatter.format(LocalDateTime.now()));
	 logger.info("Fire Every 10 Seconds------------------------------");
	 findAllSmsByDate();
	 logger.info("Message Will be Fired According To Date------------------------------");
	
	 }

	/***********************************************
	 * Fires At 11Am First Day Of Every Month
	 ************************************************************/
//	@Scheduled(cron = "0 0 11 1 1/1 ?" , zone="Asia/Calcutta")
//	public void scheduleTaskWithMonthly() {
//		logger.info("Cron Taks :: Excecution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));
//		logger.info("Fire EveryDay At 11Am ------------------------------");
//		try {
//			findAllSmsByDate();
//		} catch (ParseException pe) {
//			logger.info("Error in Find All SMS " +pe.getMessage());
//		}
//	}

	/***********************************************
	 * Fires Every Year At 11Am In 1 January
	 ************************************************************/
	// @Scheduled(cron = "0 0 11 1 1 ?")
	// public void scheduleTaskWithYearly() {
	// logger.info("Cron Taks :: Excecution Time - {}",
	// dateTimeFormatter.format(LocalDateTime.now()));
	// logger.info("Fires Every Year At 11Am In 1
	// January------------------------------");
	// }

	/***********************************************
	 * Fires Weekly At Every Monday
	 ************************************************************/
	// @Scheduled(cron = "0 0 11 ? * MON")
	// public void scheduleTaskWithWeekly() {
	// logger.info("Cron Taks :: Excecution Time - {}",
	// dateTimeFormatter.format(LocalDateTime.now()));
	// logger.info("Fire Every 20 Seconds------------------------------");
	// logger.info("Fires Weekly At Every Monday------------------------------");
	// }

}
