package com.yss.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.yss.main.util.AES;

@SpringBootApplication
@EnableScheduling
@EnableCaching
public class NtpcApiApplication {

	private static final Logger logger = LoggerFactory.getLogger(NtpcApiApplication.class);
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(NtpcApiApplication.class, args);
		logger.info("****-****-**** Application Started ****-****-****_****");
		logger.info("password " + AES.encrypt("superUser"));
		
	}
}


