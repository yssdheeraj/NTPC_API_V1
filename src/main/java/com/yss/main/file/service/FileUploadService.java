package com.yss.main.file.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUploadService {

	private static final Logger logger = LoggerFactory.getLogger(FileUploadService.class);
	BufferedReader br = null;
	FileReader fr = null;
	File fl = null;
	//String fileName = "";
	public void Read(String fileName) throws IOException {

		String line = null;

		 try {
		
		 fl = new File(fileName);
		 fr = new FileReader(fl);
		 br=new BufferedReader(fr);
		 String currentLine;
		 while((currentLine=br.readLine()) !=null) {
		 System.out.println(currentLine);
		 }
		
		 }catch(Exception e) {
		 e.printStackTrace();
		
		 }

//		try {
//			// FileReader reads text files in the default encoding.
//			FileReader fileReader = new FileReader(fileName);
//			logger.info("-----  File Reader ----- " +fileReader);
//			
//			// Always wrap FileReader in BufferedReader.
//			BufferedReader bufferedReader = new BufferedReader(fileReader);
//			logger.info("-----  Buffered Reader ----- " +bufferedReader);
//			
//			while ((line = bufferedReader.readLine()) != null) {
//				System.out.println(line);
//			}
//
//			// Always close files.
//			bufferedReader.close();
//		} catch (FileNotFoundException ex) {
//			System.out.println("Unable to open file '" + fileName + "'");
//		} catch (IOException ex) {
//			System.out.println("Error reading file '" + fileName + "'");
//			// Or we could just do this:
//			// ex.printStackTrace();
//		}
//
		fr.close();
	}

}
